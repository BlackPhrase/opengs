#pragma once

#include "public/interface.h"

struct IFileSystem;

class CLauncher final
{
public:
	CLauncher() = default;
	~CLauncher() = default;

	void Run();

private:
	bool PreInit();
	bool Init();
	void Shutdown();

	void SetEngineDLL(const char **ppEngineDLL);

	bool OnVideoModeFailed();

	CSysModule /*CreateInterfaceFn*/ *LoadFileSystemModule(const char *sModuleName);

	IFileSystem *m_pFileSystem{ nullptr };
};