#include "Sys.hpp"
#include "winheaders.hpp"

bool Sys::GetExecutableName(const char *sFileName, size_t nSize)
{
	auto hThisModule{ GetModuleHandleA(nullptr) };

	// TODO: won't work properly on WinXP, see https://msdn.microsoft.com/en-us/library/windows/desktop/ms683197(v=vs.85).aspx - Solokiller
	return GetModuleFileNameA(hThisModule, sFileName, nSize) != 0;
};

const char *Sys::GetBaseDir()
{
	static char szLongPath[MAX_PATH]{};

	char FileName[MAX_PATH]{};

	if(GetModuleFileNameA(nullptr, FileName, sizeof(FileName)))
	{
		GetLongPathNameA(FileName, szLongPath, sizeof(szLongPath));

		char *pszLastSlash = strrchr(szLongPath, '\\');

		if(*pszLastSlash)
			pszLastSlash[1] = '\0';

		const size_t uiLength = strlen(szLongPath);

		if(uiLength > 0)
		{
			char *pszEnd = &szLongPath[uiLength - 1];

			if(*pszEnd == '\\' || *pszEnd == '/')
				*pszEnd = '\0';
		};
	};

	return szLongPath;
};