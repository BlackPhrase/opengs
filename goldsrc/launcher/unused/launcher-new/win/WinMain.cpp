#include <cstdlib>
#include "winheaders.h"

extern int LauncherMain();

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	auto hMutex{ CreateMutexA(nullptr, FALSE, "ValveHalfLifeLauncherMutex") };

	if(hMutex != nullptr)
		GetLastError();

	auto dwMutexResult{ WaitForSingleObject(hMutex, 0) };

	if(dwMutexResult != WAIT_OBJECT_0 && dwMutexResult != WAIT_ABANDONED)
	{
		MessageBoxA(nullptr, "Could not launch game.\nOnly one instance of this game can be run at a time.", "Fatal Error", MB_OK | MB_ICONERROR);
		return EXIT_SUCCESS;
	};

	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 0), &WSAData);

	cmdline->CreateCmdLine(GetCommandLineA());

	// Remove old libraries distributed with older Half-Life versions
	_unlink("mssv29.asi");
	_unlink("mssv12.asi");
	_unlink("mp3dec.asi");
	_unlink("opengl32.dll");

	int nRetVal{ LauncherMain() };

	ReleaseMutex(hMutex);
	CloseHandle(hMutex);
	WSACleanup();

	return nRetVal;
};