#include "Launcher.hpp"
#include "public/IRegistry.h"

bool CLauncher::PreInit()
{
	registry->Init();

	//If the game crashed during video mode initialization, reset video mode to default.
	if(registry->ReadInt("CrashInitializingVideoMode", 0))
	{
		registry->WriteInt("CrashInitializingVideoMode", 0);

		const char *pszEngineDLL = registry->ReadString("EngineDLL", "hw.dll");

		if(!_stricmp(pszEngineDLL, "hw.dll"))
		{
			const char *pszCaption{ "Video mode change failure" };
			const char *pszMessage{ "" };

			if(registry->ReadInt("EngineD3D", 0))
			{
				registry->WriteInt("EngineD3D", 0);

				pszMessage =
				"The game has detected that the previous attempt to start in D3D video mode failed.\n"
				"The game will now run attempt to run in openGL mode.";
			}
			else
			{
				//TODO: Shouldn't this be sw.dll? - Solokiller
				registry->WriteString("EngineDLL", "hw.dll");

				pszMessage =
				"The game has detected that the previous attempt to start in openGL video mode failed.\n"
				"The game will now run in software mode.";
			};

			//Ask the user if they want to continue.
			if(MessageBoxA(nullptr, pszMessage, pszCaption, MB_OKCANCEL | MB_ICONERROR | MB_ICONQUESTION) != IDOK)
				return EXIT_SUCCESS;

			registry->WriteInt("ScreenBPP", 16);
			registry->WriteInt("ScreenHeight", 640);
			registry->WriteInt("ScreenWidth", 480);
		};
	};

	return true;
};

void CLauncher::Shutdown()
{
	registry->Shutdown();
};

bool CLauncher::OnVideoModeFailed()
{
	registry->WriteInt("ScreenBPP", 16);
	registry->WriteInt("ScreenHeight", 640);
	registry->WriteInt("ScreenWidth", 480);

	registry->WriteString("EngineDLL", "hw.dll");

	return MessageBoxA(
	       nullptr,
	       "The specified video mode is not supported.", "Video mode change failure",
	       MB_OKCANCEL | MB_ICONERROR | MB_ICONQUESTION) == IDOK;
};