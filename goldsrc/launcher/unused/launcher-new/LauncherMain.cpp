/// @file
/// @brief platform-independent entry point

#include <cstdlib>
#include "Launcher.hpp"

int LauncherMain()
{
	CLauncher Launcher();

	Launcher.Run();

	return EXIT_SUCCESS;
};