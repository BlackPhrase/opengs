#include <sys/stat.h>
#include "Sys.hpp"

bool Sys::FileExists(const char *sFileName)
{
	struct stat buf;

	return stat(sFileName, &buf) == 0;
};