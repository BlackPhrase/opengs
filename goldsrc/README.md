# Contents:

* **common** - common code;
* **cstrike** - cstrike-specific code;
* **dedicated** - dedicated server launcher;
* **engine** - engine module implementation;
* **filesystem** - custom filesystem(_stdio) module implementation;
* **GameUI** - Game User Interface module implementation;
* **HLTV** - Half-Life TV sources;
* **hookers** - engine hookers;
* **launcher** - engine launcher code;
* **optional** - engine optional components;
* **particleman** - particle manager module implementation;
* **public** - public headers;
* **rehlds** - rehlds api implementation;
* **render** - render module sources;
* **testsuite** - testing suite;
* **tier0** - tier0 library custom implementation;
* **tier1** - tier1 library custom implementation;
* **unittests** - set of code base unit tests;
* **vgui2** - vgui2 library implementation and related sources;
* **vstdlib** - Valve Standard Library custom implementation;