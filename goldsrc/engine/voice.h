/// @file

#pragma once

#include "common/ivoicetweak.h"

extern IVoiceTweak g_VoiceTweakAPI;

void Voice_RegisterCvars();

bool Voice_Init(const char *pCodecName, int quality);
void Voice_Deinit();

//void Voice_Idle(float frametime);

//qboolean Voice_RecordStart(const char *pUncompressedFile, const char *pDecompressedFile, const char *pMicInputFile);
//qboolean Voice_RecordStop();

//qboolean Voice_IsRecording();

//
// ivoicetweak
//
int VoiceTweak_StartVoiceTweakMode();
void VoiceTweak_EndVoiceTweakMode();

void VoiceTweak_SetControlFloat(VoiceTweakControl iControl, float value);
float VoiceTweak_GetControlFloat(VoiceTweakControl iControl);

int VoiceTweak_GetSpeakingVolume();