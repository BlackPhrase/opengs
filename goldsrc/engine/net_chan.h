/*
*
*    This program is free software; you can redistribute it and/or modify it
*    under the terms of the GNU General Public License as published by the
*    Free Software Foundation; either version 2 of the License, or (at
*    your option) any later version.
*
*    This program is distributed in the hope that it will be useful, but
*    WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software Foundation,
*    Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*    In addition, as a special exception, the author gives permission to
*    link the code of this program with the Half-Life Game Engine ("HL
*    Engine") and Modified Game Libraries ("MODs") developed by Valve,
*    L.L.C ("Valve").  You must obey the GNU General Public License in all
*    respects for all of the code used other than the HL Engine and MODs
*    from Valve.  If you modify this file, you may extend this exception
*    to your version of the file, but you are not obligated to do so.  If
*    you do not wish to do so, delete this exception statement from your
*    version.
*
*/

/// @file

#pragma once

/*
#include "maintypes.h"
#include "cvar.h"
#include "net.h"
*/
#include "common/mathlib.h"
#include "common/const.h"
#include "common/enums.h" // netsrc_t
#include "common/netadr.h"
#include "common.h"

extern char gDownloadFile[256];

extern int net_drop; // packets dropped before this one

extern cvar_t net_log;

extern cvar_t net_showpackets;
extern cvar_t net_showdrop;

extern cvar_t net_drawslider;
extern cvar_t net_chokeloopback;

extern cvar_t sv_filetransfercompression;
extern cvar_t sv_filetransfermaxsize;

typedef struct fragbuf_s fragbuf_t;
typedef struct fragbufwaiting_s fragbufwaiting_t;

// Message data
typedef struct flowstats_s
{
	// Size of message sent/received
	int size;
	// Time that message was sent/received
	double time;
} flowstats_t;

const int MAX_LATENT = 32;

typedef struct flow_s
{
	// Data for last MAX_LATENT messages
	flowstats_t stats[MAX_LATENT];
	// Current message position
	int current;
	// Time when we should recompute k/sec data
	double nextcompute;
	// Average data
	float kbytespersec;
	float avgkbytespersec;
} flow_t;

// Quake
/*

typedef struct
{
	qboolean	fatal_error;	

// the statistics are cleared at each client begin, because
// the server connecting process gives a bogus picture of the data
	float		frame_latency;		// rolling average
	float		frame_rate;

	int			drop_count;			// dropped packets, cleared each level
	int			good_count;			// cleared each level

	int			qport;			

	byte		message_buf[MAX_MSGLEN];

	byte		reliable_buf[MAX_MSGLEN];	// unacked reliable message

// time and size data to calculate bandwidth
	int			outgoing_size[MAX_LATENT];
	double		outgoing_time[MAX_LATENT];
} netchan_t;
*/

// REHLDS
/*
typedef struct netchan_s
{

	// Bandwidth choke
	// Bytes per second
	double rate;
	// If realtime > cleartime, free to send next packet
	double cleartime;

	// Sequencing variables
	//
	// Increasing count of sequence numbers
	int incoming_sequence;
	// # of last outgoing message that has been ack'd.
	int incoming_acknowledged;
	// Toggles T/F as reliable messages are received.
	int incoming_reliable_acknowledged;
	// single bit, maintained local
	int incoming_reliable_sequence;
	// Message we are sending to remote
	int outgoing_sequence;
	// Whether the message contains reliable payload, single bit
	int reliable_sequence;
	// Outgoing sequence number of last send that had reliable data
	int last_reliable_sequence;

	
	short int frag_startpos[MAX_STREAMS];
	
	short int frag_length[];

} netchan_t;
*/

// Network Connection Channel
typedef struct netchan_s
{
	// NS_SERVER or NS_CLIENT, depending on channel.
	netsrc_t sock;

	// Address this channel is talking to.
	netadr_t remote_address;

	int player_slot;

	float last_received; // for timeouts // For timeouts.  Time last message was received.
	float connect_time; // Time when channel was connected.

	// bandwidth estimator
	double rate;      // seconds / byte
	double cleartime; // if realtime > nc->cleartime, free to go

	// sequencing variables
	int incoming_sequence;
	int incoming_acknowledged;

	int incoming_reliable_acknowledged; // single bit
	int incoming_reliable_sequence;     // single bit, maintained local

	int outgoing_sequence;

	int reliable_sequence;      // single bit
	int last_reliable_sequence; // sequence number of last send

	void *connection_status;

	int (*pfnNetchan_Blocksize)(void *);

	// Staging and holding areas
	sizebuf_t message; // writing buffer to send to server
	byte message_buf[MAX_MSGLEN]; // 3990

	// Reliable message buffer. We keep adding to it until reliable is acknowledged. Then we clear it.
	int reliable_length;
	byte reliable_buf[MAX_MSGLEN]; // 3990

	fragbufwaiting_t *waitlist[MAX_STREAMS]; // Waiting list of buffered fragments to go onto queue. Multiple outgoing buffers can be queued in succession.

	int reliable_fragment[MAX_STREAMS]; // Is reliable waiting buf a fragment?
	unsigned int reliable_fragid[MAX_STREAMS]; // Buffer id for each waiting fragment

	fragbuf_t *fragbufs[MAX_STREAMS]; // The current fragment being set
	int fragbufcount[MAX_STREAMS]; // The total number of fragments in this stream

	__int16 frag_startpos[MAX_STREAMS]; // Position in outgoing buffer where frag data starts
	__int16 frag_length[MAX_STREAMS]; // Length of frag data in the buffer

	fragbuf_t *incomingbufs[MAX_STREAMS]; // Incoming fragments are stored here
	qboolean incomingready[MAX_STREAMS]; // Set to true when incoming data is ready

	// Only referenced by the FRAG_FILE_STREAM component
	// Name of file being downloaded
	char incomingfilename[MAX_PATH]; // 260

	void *tempbuffer;
	int tempbuffersize;

	// Incoming and outgoing flow metrics
	flow_t flow[MAX_FLOWS];
} netchan_t;

void Netchan_Init();

void Netchan_Setup(netsrc_t socketnumber, netchan_t *chan, netadr_t adr, int player_slot, void *connection_status, qboolean (*pfnNetchan_Blocksize)(void *));

void Netchan_UpdateFlow(netchan_t *chan);
/*NOXREF*/ void Netchan_UpdateProgress(netchan_t *chan);

void Netchan_Transmit(netchan_t *chan, int length, byte *data);

void Netchan_OutOfBand(netsrc_t sock, netadr_t adr, int length, byte *data);
void Netchan_OutOfBandPrint(netsrc_t sock, netadr_t adr, char *format, ...);

qboolean Netchan_Process(netchan_t *chan);

qboolean Netchan_CanPacket(netchan_t *chan);
qboolean Netchan_CanReliable(netchan_t *chan);

qboolean Netchan_Validate(netchan_t *chan, qboolean *frag_message, unsigned int *fragid, int *frag_offset, int *frag_length);

void Netchan_CreateFragments_(qboolean server, netchan_t *chan, sizebuf_t *msg);
void Netchan_CreateFragments(qboolean server, netchan_t *chan, sizebuf_t *msg);

void Netchan_CreateFileFragmentsFromBuffer(qboolean server, netchan_t *chan, const char *filename, unsigned char *uncompressed_pbuf, int uncompressed_size);
int Netchan_CreateFileFragments(qboolean server, netchan_t *chan, const char *filename);

#ifdef REHLDS_FIXES
int Netchan_CreateFileFragments_(qboolean server, netchan_t *chan, const char *filename);
#endif

qboolean Netchan_CopyNormalFragments(netchan_t *chan);
qboolean Netchan_CopyFileFragments(netchan_t *chan);

void Netchan_ClearFragbufs(fragbuf_t **ppbuf);
void Netchan_ClearFragments(netchan_t *chan);
void Netchan_Clear(netchan_t *chan);

void Netchan_CheckForCompletion(netchan_t *chan, int stream, int intotalbuffers);

void Netchan_UnlinkFragment(fragbuf_t *buf, fragbuf_t **list);

/*NOXREF*/ qboolean Netchan_IsSending(netchan_t *chan);
/*NOXREF*/ qboolean Netchan_IsReceiving(netchan_t *chan);

void Netchan_FlushIncoming(netchan_t *chan, int stream);
qboolean Netchan_IncomingReady(netchan_t *chan);

/*NOXREF*/ qboolean Netchan_CompressPacket(sizebuf_t *chan);
/*NOXREF*/ qboolean Netchan_DecompressPacket(sizebuf_t *chan);

void Netchan_FragSend(netchan_t *chan);

fragbuf_t *Netchan_AllocFragbuf();

void Netchan_AddBufferToList(fragbuf_t **pplist, fragbuf_t *pbuf);
void Netchan_AddFragbufToTail(fragbufwaiting_t *wait, fragbuf_t *buf);

fragbuf_t *Netchan_FindBufferById(fragbuf_t **pplist, int id, qboolean allocate);