/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/// @file

#include "quakedef.h"
#include "winquake.h"
//#include "cl_pred.h"

cvar_t cl_nopred = { "cl_nopred", "0" };           // TODO: remove; enabled by default
cvar_t cl_pushlatency = { "pushlatency", "-999" }; // TODO: remove; was removed from because various related issues

extern frame_t *view_frame;

void CL_PushPMStates()
{
	//TODO: implement - Solokiller
}

void CL_PopPMStates()
{
	//TODO: implement - Solokiller
}

/*
=================
CL_NudgePosition

If g_clmove.origin is in a solid position,
try nudging slightly on all axis to
allow for the cut precision of the net coordinates
=================
*/
void CL_NudgePosition()
{
	vec3_t base;
	int x, y;

	if(PM_HullPointContents(&cl.model_precache[1]->hulls[1], 0, g_clmove.origin) == CONTENTS_EMPTY)
		return;

	VectorCopy(g_clmove.origin, base);
	for(x = -1; x <= 1; x++)
	{
		for(y = -1; y <= 1; y++)
		{
			g_clmove.origin[0] = base[0] + x * 1.0 / 8;
			g_clmove.origin[1] = base[1] + y * 1.0 / 8;
			if(PM_HullPointContents(&cl.model_precache[1]->hulls[1], 0, g_clmove.origin) == CONTENTS_EMPTY)
				return;
		}
	}
	Con_DPrintf("CL_NudgePosition: stuck\n");
}

/*
==============
CL_PredictUsercmd
==============
*/
// NOTE: why ptr to double? (double *pfElapsed)
void CL_RunUsercmd(local_state_t *from, local_state_t *to, usercmd_t *u, qboolean runfuncs, double *pfElapsed, unsigned int random_seed)
{
	// split up very long moves
	if(u->msec > 50)
	{
		local_state_t temp;
		usercmd_t split;

		split = *u;
		split.msec /= 2;

		CL_RunUsercmd(from, &temp, &split, runfuncs, pfElapsed, random_seed);
		CL_RunUsercmd(&temp, to, &split, runfuncs, pfElapsed, random_seed);
		return;
	}

	VectorCopy(from->origin, g_clmove.origin);
	//	VectorCopy (from->viewangles, g_clmove.angles);
	VectorCopy(u->angles, g_clmove.angles);
	VectorCopy(from->velocity, g_clmove.velocity);

	g_clmove.oldbuttons = from->oldbuttons;
	g_clmove.waterjumptime = from->waterjumptime;
	g_clmove.dead = cl.stats[STAT_HEALTH] <= 0;
	g_clmove.spectator = spectator;

	g_clmove.cmd = *u;

	ClientDLL_PlayerMove(&g_clmove, 0);

	ClientDLL_WeaponsPostThink(from, to, u, *pfElapsed, random_seed); // ?

	ClientDLL_PostRunCmd(from, to, u, runfuncs, *pfElapsed, random_seed); // ?

	//for (i=0 ; i<3 ; i++)
	//g_clmove.origin[i] = ((int)(g_clmove.origin[i]*8))*0.125;

	to->waterjumptime = g_clmove.waterjumptime;
	to->oldbuttons = g_clmove.cmd.buttons;
	VectorCopy(g_clmove.origin, to->origin);
	VectorCopy(g_clmove.angles, to->viewangles);
	VectorCopy(g_clmove.velocity, to->velocity);
	to->onground = onground;

	to->weaponframe = from->weaponframe;
}

/*
==============
CL_PredictMove
==============
*/
void CL_PredictMove(qboolean repredicting)
{
	int i;
	float f;
	frame_t *from, *to = NULL;
	int oldphysent;

	if(cl_pushlatency.value > 0)
		Cvar_Set("pushlatency", "0");

	if(cl.paused)
		return;

	cl.time = realtime - cls.latency - cl_pushlatency.value * 0.001;

	if(cl.time > realtime)
		cl.time = realtime;

	if(cl.intermission)
		return;

	if(!cl.validsequence)
		return;

	if(cls.netchan.outgoing_sequence - cls.netchan.incoming_sequence >= UPDATE_BACKUP - 1)
		return;

	VectorCopy(cl.viewangles, cl.simangles);

	// this is the last frame received from the server
	from = &cl.frames[cls.netchan.incoming_sequence & UPDATE_MASK];

	// we can now render a frame
	if(cls.state == ca_onserver)
	{
		// first update is the final signon stage
		cls.state = ca_active;
	}

	if(cl_nopred.value)
	{
		VectorCopy(from->playerstate[cl.playernum].velocity, cl.simvel);
		VectorCopy(from->playerstate[cl.playernum].origin, cl.simorg);
		return;
	}

	// predict forward until cl.time <= to->senttime
	oldphysent = g_clmove.numphysent;
	CL_SetSolidPlayers(cl.playernum);

	//	to = &cl.frames[cls.netchan.incoming_sequence & UPDATE_MASK];

	for(i = 1; i < UPDATE_BACKUP - 1 && cls.netchan.incoming_sequence + i < cls.netchan.outgoing_sequence; i++)
	{
		to = &cl.frames[(cls.netchan.incoming_sequence + i) & UPDATE_MASK];
		CL_RunUsercmd(&from->playerstate[cl.playernum],
		              &to->playerstate[cl.playernum], &to->cmd, cl.spectator);
		if(to->senttime >= cl.time)
			break;
		from = to;
	}

	g_clmove.numphysent = oldphysent;

	if(i == UPDATE_BACKUP - 1 || !to)
		return; // net hasn't deliver packets in a long time...

	// now interpolate some fraction of the final frame
	if(to->senttime == from->senttime)
		f = 0;
	else
	{
		f = (cl.time - from->senttime) / (to->senttime - from->senttime);

		if(f < 0)
			f = 0;
		if(f > 1)
			f = 1;
	}

	for(i = 0; i < 3; i++)
		if(fabs(from->playerstate[cl.playernum].origin[i] - to->playerstate[cl.playernum].origin[i]) > 128)
		{ // teleported, so don't lerp
			VectorCopy(to->playerstate[cl.playernum].velocity, cl.simvel);
			VectorCopy(to->playerstate[cl.playernum].origin, cl.simorg);
			return;
		}

	for(i = 0; i < 3; i++)
	{
		cl.simorg[i] = from->playerstate[cl.playernum].origin[i] + f * (to->playerstate[cl.playernum].origin[i] - from->playerstate[cl.playernum].origin[i]);
		cl.simvel[i] = from->playerstate[cl.playernum].velocity[i] + f * (to->playerstate[cl.playernum].velocity[i] - from->playerstate[cl.playernum].velocity[i]);
	}
}

/*
==============
CL_InitPrediction
==============
*/
void CL_InitPrediction()
{
	Cvar_RegisterVariable(&cl_pushlatency);
	Cvar_RegisterVariable(&cl_nopred);
}

void CL_RedoPrediction(){
	// TODO
};