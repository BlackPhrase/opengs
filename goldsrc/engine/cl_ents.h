/// @file

#pragma once

#include "pm_shared/pm_defs.h"

void CL_SetSolidPlayers(int playernum);

void CL_SetUpPlayerPrediction(qboolean dopred, qboolean bIncludeLocalClient);

void CL_AddStateToEntlist(physent_t *pe, entity_state_t *state);