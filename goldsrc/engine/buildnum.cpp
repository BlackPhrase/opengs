/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the included (GNU.txt) GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
/*
*
*    This program is free software; you can redistribute it and/or modify it
*    under the terms of the GNU General Public License as published by the
*    Free Software Foundation; either version 2 of the License, or (at
*    your option) any later version.
*
*    This program is distributed in the hope that it will be useful, but
*    WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software Foundation,
*    Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*    In addition, as a special exception, the author gives permission to
*    link the code of this program with the Half-Life Game Engine ("HL
*    Engine") and Modified Game Libraries ("MODs") developed by Valve,
*    L.L.C ("Valve").  You must obey the GNU General Public License in all
*    respects for all of the code used other than the HL Engine and MODs
*    from Valve.  If you modify this file, you may extend this exception
*    to your version of the file, but you are not obligated to do so.  If
*    you do not wish to do so, delete this exception statement from your
*    version.
*
*/

/// @file

//#include "precompiled.h"
#include <cstdlib>
#include <cstring>
#include "quakedef.h"
//#include "strtools.h"

namespace
{
/**
*	Day on which Half-Life was released.
*/
static const int RELEASE_DAY = 34995;

static int cached_build_number = 0;

static const char *const date = __DATE__;

static const int NUM_MONTHS = 12;

static const char *const mon[NUM_MONTHS] =
{
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec"
};

static const char mond[NUM_MONTHS] =
{
  31,
  28,
  31,
  30,
  31,
  30,
  31,
  31,
  30,
  31,
  30,
  31
};

}; // namespace

int build_number()
{
	if(cached_build_number)
		return cached_build_number;

	int days = 0;

	int month;

	for(month = 0; month < NUM_MONTHS; ++month)
	{
		if(!Q_strncasecmp(date, mon[month], 3))
			break;

		days += mond[month];
	}

	//Edge case: if __DATE__ contains an unknown month string, possibly due to locale issues, then we'll return 0 instead.
	//Better than returning strange values.
	if(month >= NUM_MONTHS)
		return cached_build_number;

	const int iDaysInMonth = days + strtol(date + 4, nullptr, 10) - 1;
	const int iYear = strtol(date + 7, nullptr, 10);
	int iTotalDays = (signed int)floor((long double)(iYear - 1901) * 365.25) + iDaysInMonth;

	//Adjust for leap years.
	if(!(iYear & 3) && (month > 1))
		iTotalDays -= 1;

	cached_build_number = iTotalDays - RELEASE_DAY;

	return cached_build_number;
}