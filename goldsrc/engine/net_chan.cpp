/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
/*
*
*    This program is free software; you can redistribute it and/or modify it
*    under the terms of the GNU General Public License as published by the
*    Free Software Foundation; either version 2 of the License, or (at
*    your option) any later version.
*
*    This program is distributed in the hope that it will be useful, but
*    WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software Foundation,
*    Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*    In addition, as a special exception, the author gives permission to
*    link the code of this program with the Half-Life Game Engine ("HL
*    Engine") and Modified Game Libraries ("MODs") developed by Valve,
*    L.L.C ("Valve").  You must obey the GNU General Public License in all
*    respects for all of the code used other than the HL Engine and MODs
*    from Valve.  If you modify this file, you may extend this exception
*    to your version of the file, but you are not obligated to do so.  If
*    you do not wish to do so, delete this exception statement from your
*    version.
*
*/

/// @file

//#include "precompiled.h"
#include "quakedef.h"
//#include "net_chan.h"

#ifdef _WIN32
#include "winquake.h"
#endif

#define PACKET_HEADER 8

/*

packet header
-------------
31	sequence
1	does this message contain a reliable payload
31	acknowledge sequence
1	acknowledge receipt of even/odd message
16  qport

The remote connection never knows if it missed a reliable message, the
local side detects that it has been dropped by seeing a sequence acknowledge
higher thatn the last reliable sequence, but without the correct evon/odd
bit for the reliable set.

If the sender notices that a reliable message has been dropped, it will be
retransmitted.  It will not be retransmitted again until a message after
the retransmit has been acknowledged and the reliable still failed to get there.

if the sequence number is -1, the packet should be handled without a netcon

The reliable message can be added to at any time by doing
MSG_Write* (&netchan->message, <data>).

If the message buffer is overflowed, either by a single message, or by
multiple frames worth piling up while the last reliable transmit goes
unacknowledged, the netchan signals a fatal error.

Reliable messages are allways placed first in a packet, then the unreliable
message is included if there is sufficient room.

To the receiver, there is no distinction between the reliable and unreliable
parts of the message, they are just processed out as a single larger message.

Illogical packet sequence numbers cause the packet to be dropped, but do
not kill the connection.  This, combined with the tight window of valid
reliable acknowledgement numbers provides protection against malicious
address spoofing.

The qport field is a workaround for bad address translating routers that
sometimes remap the client's source port on a packet during gameplay.

If the base part of the net address matches and the qport matches, then the
channel matches even if the IP port differs.  The IP port should be updated
to the new value before sending out any replies.


*/

int net_drop;

// TODO: should be MAX_PATH - Solokiller
char gDownloadFile[256] = {};

cvar_t net_log = { "net_log", "0", 0, 0.0f, nullptr};
cvar_t net_showpackets = { "net_showpackets", "0", 0, 0.0f, nullptr};
cvar_t net_showdrop = { "net_showdrop", "0", 0, 0.0f, nullptr};
cvar_t net_drawslider = { "net_drawslider", "0", 0, 0.0f, nullptr};
cvar_t net_chokeloopback = { "net_chokeloop", "0", 0, 0.0f, nullptr};

cvar_t sv_filetransfercompression = { "sv_filetransfercompression", "1", 0, 0.0f, nullptr};
cvar_t sv_filetransfermaxsize = { "sv_filetransfermaxsize", "10485760", 0, 0.0f, nullptr};

void Netchan_UnlinkFragment(fragbuf_t *buf, fragbuf_t **list)
{
	fragbuf_t *search;

	if (list == nullptr)
	{
		Con_Printf("%s: Asked to unlink fragment from empty list, ignored\n", __func__);
		return;
	}

	if (*list == buf)
	{
		*list = buf->next;
		Mem_Free(buf);
		return;
	}

	search = *list;
	while (search->next)
	{
		if (search->next == buf)
		{
			search->next = buf->next;
			Mem_Free(buf);
			return;
		}
		search = search->next;
	}

	Con_Printf("%s: Couldn't find fragment\n", __func__);
}

/*
===============
Netchan_OutOfBand

Sends an out-of-band datagram
================
*/
void Netchan_OutOfBand(netsrc_t sock, netadr_t adr, int length, byte *data)
{
	sizebuf_t send;
	byte send_buf[MAX_MSGLEN + PACKET_HEADER];

	// write the packet header
	send.buffername = "Netchan_OutOfBand";
	send.data = send_buf;
	send.maxsize = sizeof(send_buf);
	send.cursize = 0;
	send.flags = SIZEBUF_ALLOW_OVERFLOW;

	MSG_WriteLong(&send, -1); // -1 sequence means out of band
	SZ_Write(&send, data, length);

	// send the datagram
	// no input in demo playback mode
#ifndef SERVERONLY
	if(!cls.demoplayback)
#endif
		NET_SendPacket(sock, send.cursize, send.data, adr);
}

/*
===============
Netchan_OutOfBandPrint

Sends a text message in an out-of-band datagram
================
*/
void Netchan_OutOfBandPrint(netsrc_t sock, netadr_t adr, char *format, ...)
{
	va_list argptr;
	static char string[8192]; // ??? why static?

	va_start(argptr, format);
	vsprintf(string, format, argptr);
	va_end(argptr);

	Netchan_OutOfBand(sock, adr, strlen(string), (byte *)string);
}

void Netchan_ClearFragbufs(fragbuf_t **ppbuf)
{
	fragbuf_t *buf, *n;

	if (!ppbuf)
	{
		return;
	}

	// Throw away any that are sitting around
	buf = *ppbuf;
	while (buf)
	{
		n = buf->next;
		Mem_Free(buf);
		buf = n;
	}
	*ppbuf = nullptr;
}

void Netchan_ClearFragments(netchan_t *chan)
{
	fragbufwaiting_t *wait, *next;
	for (int i = 0; i < MAX_STREAMS; i++)
	{
		wait = chan->waitlist[i];
		while (wait)
		{
			next = wait->next;
			Netchan_ClearFragbufs(&wait->fragbufs);
			Mem_Free(wait);
			wait = next;
		}
		chan->waitlist[i] = nullptr;

		Netchan_ClearFragbufs(&chan->fragbufs[i]);
		Netchan_FlushIncoming(chan, i);
	}
}

void Netchan_Clear(netchan_t *chan)
{
	Netchan_ClearFragments(chan);

	if (chan->reliable_length)
	{
		Con_DPrintf("%s: reliable length not 0, reliable_sequence: %d, incoming_reliable_acknowledged: %d\n", __func__, chan->reliable_length, chan->incoming_reliable_acknowledged);
		chan->reliable_sequence ^= 1;
		chan->reliable_length = 0;
	}

	chan->cleartime = 0.0;

	for (int i = 0; i < MAX_STREAMS; i++)
	{
		chan->reliable_fragid[i] = 0;
		chan->reliable_fragment[i] = 0;
		chan->fragbufcount[i] = 0;
		chan->frag_startpos[i] = 0;
		chan->frag_length[i] = 0;
		chan->incomingready[i] = FALSE;
	}

	if (chan->tempbuffer)
	{
		Mem_Free(chan->tempbuffer);
		chan->tempbuffer = nullptr;
	}
	chan->tempbuffersize = 0;
}

/*
==============
Netchan_Setup

called to open a channel to a remote system
==============
*/
void Netchan_Setup(netsrc_t socketnumber, netchan_t *chan, netadr_t adr, int player_slot, void *connection_status, qboolean(*pfnNetchan_Blocksize)(void *))
{
	Netchan_Clear(chan);

	Q_memset(chan, 0, sizeof(netchan_t));

	chan->player_slot = player_slot + 1;
	chan->sock = socketnumber;
	chan->remote_address = adr;
	chan->last_received = (float)realtime;
	chan->connect_time = (float)realtime;

	chan->message.buffername = "netchan->message";

#ifdef REHLDS_FIXES
	if (player_slot != -1)
	{
		chan->message.data = g_GameClients[player_slot]->GetExtendedMessageBuffer();
		chan->message.maxsize = NET_MAX_PAYLOAD;
	}
	else
#endif
	{
		chan->message.data = chan->message_buf;
		chan->message.maxsize = sizeof(chan->message_buf);
	}

#ifdef REHLDS_FIXES
	chan->message.cursize = 0;
#endif // REHLDS_FIXES

	chan->message.flags = SIZEBUF_ALLOW_OVERFLOW;

	chan->rate = DEFAULT_RATE;

	// Prevent the first message from getting dropped after connection is set up.
	chan->outgoing_sequence = 1;

	chan->connection_status = connection_status;
	chan->pfnNetchan_Blocksize = pfnNetchan_Blocksize;
}

/*
===============
Netchan_CanPacket

Returns true if the bandwidth choke isn't active
================
*/
#define MAX_BACKUP 200
qboolean Netchan_CanPacket(netchan_t *chan)
{
	if(chan->cleartime < realtime + MAX_BACKUP * chan->rate)
		return true;
	return false;
}

/*
===============
Netchan_CanReliable

Returns true if the bandwidth choke isn't 
================
*/
/*
qboolean Netchan_CanReliable(netchan_t *chan)
{
	if(chan->reliable_length)
		return false; // waiting for ack
	return Netchan_CanPacket(chan);
}

#ifdef SERVERONLY
qboolean ServerPaused();
#endif
*/

void Netchan_UpdateFlow(netchan_t *chan)
{
	if (!chan)
		return;

	int bytes = 0;
	float faccumulatedtime = 0.0f;

	for (int flow = 0; flow < MAX_FLOWS; flow++)
	{
		flow_t *pflow = &chan->flow[flow];
		if (realtime - pflow->nextcompute < 0.1)
			continue;

		pflow->nextcompute = realtime + 0.1;
		int start = pflow->current - 1;
		for (int i = 0; i < MAX_LATENT - 1; i++)
		{
			flowstats_t *pprev = &pflow->stats[(start - i) & 0x1F];
			flowstats_t *pstat = &pflow->stats[(start - i - 1) & 0x1F];

			faccumulatedtime += pprev->time - pstat->time;
			bytes += pstat->size;
		}

		pflow->kbytespersec = (faccumulatedtime == 0.0f) ? 0.0f : bytes / faccumulatedtime / 1024.0f;
		pflow->avgkbytespersec = pflow->avgkbytespersec * (2.0 / 3) + pflow->kbytespersec * (1.0 / 3);
	}
}

/*
===============
Netchan_Transmit

tries to send an unreliable message to a connection, and handles the
transmition / retransmition of the reliable messages.

A 0 length will still generate a packet and deal with the reliable messages.
================
*/
void Netchan_Transmit(netchan_t *chan, int length, byte *data)
{
	sizebuf_t send;
	byte send_buf[MAX_MSGLEN + PACKET_HEADER];
	qboolean send_reliable;
	unsigned w1, w2;
	int i;

	// check for message overflow
	if(chan->message.overflowed)
	{
		chan->fatal_error = true;
		Con_Printf("%s:Outgoing message overflow\n", NET_AdrToString(chan->remote_address));
		return;
	}

	// if the remote side dropped the last reliable message, resend it
	send_reliable = false;

	if(chan->incoming_acknowledged > chan->last_reliable_sequence && chan->incoming_reliable_acknowledged != chan->reliable_sequence)
		send_reliable = true;

	// if the reliable transmit buffer is empty, copy the current message out
	if(!chan->reliable_length && chan->message.cursize)
	{
		memcpy(chan->reliable_buf, chan->message_buf, chan->message.cursize);
		chan->reliable_length = chan->message.cursize;
		chan->message.cursize = 0;
		chan->reliable_sequence ^= 1;
		send_reliable = true;
	}

	// write the packet header
	send.data = send_buf;
	send.maxsize = sizeof(send_buf);
	send.cursize = 0;

	w1 = chan->outgoing_sequence | (send_reliable << 31);
	w2 = chan->incoming_sequence | (chan->incoming_reliable_sequence << 31);

	chan->outgoing_sequence++;

	MSG_WriteLong(&send, w1);
	MSG_WriteLong(&send, w2);

// send the qport if we are a client
#ifndef SERVERONLY
	MSG_WriteShort(&send, cls.qport);
#endif

	// copy the reliable message to the packet first
	if(send_reliable)
	{
		SZ_Write(&send, chan->reliable_buf, chan->reliable_length);
		chan->last_reliable_sequence = chan->outgoing_sequence;
	}

	// add the unreliable part if space is available
	if(send.maxsize - send.cursize >= length)
		SZ_Write(&send, data, length);

	// send the datagram
	i = chan->outgoing_sequence & (MAX_LATENT - 1);
	chan->outgoing_size[i] = send.cursize;
	chan->outgoing_time[i] = realtime;

//zoid, no input in demo playback mode
#ifndef SERVERONLY
	if(!cls.demoplayback)
#endif
		NET_SendPacket(send.cursize, send.data, chan->remote_address);

	if(chan->cleartime < realtime)
		chan->cleartime = realtime + send.cursize * chan->rate;
	else
		chan->cleartime += send.cursize * chan->rate;
#ifdef SERVERONLY
	if(ServerPaused())
		chan->cleartime = realtime;
#endif

	if(net_showpackets.value)
		Con_Printf("--> s=%i(%i) a=%i(%i) %i\n", chan->outgoing_sequence, send_reliable, chan->incoming_sequence, chan->incoming_reliable_sequence, send.cursize);
}

fragbuf_t *Netchan_FindBufferById(fragbuf_t **pplist, int id, qboolean allocate)
{
	fragbuf_t *list = *pplist;
	fragbuf_t *pnewbuf;

	while (list)
	{
		if (list->bufferid == id)
			return list;

		list = list->next;
	}

	if (!allocate)
		return nullptr;

	// Create new entry
	pnewbuf = Netchan_AllocFragbuf();
	pnewbuf->bufferid = id;
	Netchan_AddBufferToList(pplist, pnewbuf);

	return pnewbuf;
}

void Netchan_CheckForCompletion(netchan_t *chan, int stream, int intotalbuffers)
{
	int c;
	int size;
	int id;
	fragbuf_t *p;

	size = 0;
	c = 0;

	p = chan->incomingbufs[stream];
	if (!p)
		return;

	while (p)
	{
		size += p->frag_message.cursize;
		c++;

		id = FRAG_GETID(p->bufferid);
		if (id != c && chan == &g_pcls.netchan)
		{
			if (chan->sock == NS_MULTICAST)
			{
				char szCommand[32];
				Q_snprintf(szCommand, sizeof(szCommand), "listen %s\n", NET_AdrToString(chan->remote_address));
				Cbuf_AddText(szCommand);
				return;
			}
			Con_Printf("%s:  Lost/dropped fragment would cause stall, retrying connection\n", __func__);
			Cbuf_AddText("retry\n");
		}

		p = p->next;
	}

	// Received final message
	if (c == intotalbuffers)
	{
		chan->incomingready[stream] = true;
	}
}

qboolean Netchan_Validate(netchan_t *chan, qboolean *frag_message, unsigned int *fragid, int *frag_offset, int *frag_length)
{
	for (int i = 0; i < MAX_STREAMS; i++)
	{
		if (!frag_message[i])
			continue;

#ifndef REHLDS_FIXES
		if (FRAG_GETID(fragid[i]) > MAX_FRAGMENTS || FRAG_GETCOUNT(fragid[i]) > MAX_FRAGMENTS)
		{
			return FALSE;
		}

		if ((unsigned int)frag_length[i] > 0x800 || (unsigned int)frag_offset[i] > 0x4000)
		{
			return FALSE;
		}
#else // REHLDS_FIXES
		// total fragments should be <= MAX_FRAGMENTS and current fragment can't be > total fragments
		if (i == FRAG_NORMAL_STREAM && FRAG_GETCOUNT(fragid[i]) > MAX_NORMAL_FRAGMENTS)
			return FALSE;
		if (i == FRAG_FILE_STREAM && FRAG_GETCOUNT(fragid[i]) > MAX_FILE_FRAGMENTS)
			return FALSE;
		if (FRAG_GETID(fragid[i]) > FRAG_GETCOUNT(fragid[i]))
			return FALSE;
		if (!frag_length[i])
			return FALSE;
		if ((size_t)frag_length[i] > FRAGMENT_MAX_SIZE || (size_t)frag_offset[i] > NET_MAX_PAYLOAD - 1)
			return FALSE;

		int frag_end = frag_offset[i] + frag_length[i];

		// end of fragment is out of the packet
		if (frag_end + msg_readcount > net_message.cursize)
			return FALSE;

		// fragment overlaps next stream's fragment or placed after it
		for (int j = i + 1; j < MAX_STREAMS; j++)
		{
			if (frag_end > frag_offset[j] && frag_message[j]) // don't add msg_readcount for comparison
				return FALSE;
		}
#endif // REHLDS_FIXES
	}

	return TRUE;
}

/*
=================
Netchan_Process

called when the current net_message is from remote_address
modifies net_message so that it points to the packet payload
=================
*/
qboolean Netchan_Process(netchan_t *chan)
{
	unsigned sequence, sequence_ack;
	unsigned reliable_ack, reliable_message;
#ifdef SERVERONLY
	int qport;
#endif
	int i;

	if(
#ifndef SERVERONLY
	!cls.demoplayback &&
#endif
	!NET_CompareAdr(net_from, chan->remote_address))
		return false;

	// get sequence numbers
	MSG_BeginReading();
	sequence = MSG_ReadLong();
	sequence_ack = MSG_ReadLong();

// read the qport if we are a server
#ifdef SERVERONLY
	qport = MSG_ReadShort();
#endif

	reliable_message = sequence >> 31;
	reliable_ack = sequence_ack >> 31;

	sequence &= ~(1 << 31);
	sequence_ack &= ~(1 << 31);

	if(net_showpackets.value)
		Con_Printf("<-- s=%i(%i) a=%i(%i) %i\n", sequence, reliable_message, sequence_ack, reliable_ack, net_message.cursize);

// get a rate estimation
#if 0
	if (chan->outgoing_sequence - sequence_ack < MAX_LATENT)
	{
		int				i;
		double			time, rate;
	
		i = sequence_ack & (MAX_LATENT - 1);
		time = realtime - chan->outgoing_time[i];
		time -= 0.1;	// subtract 100 ms
		if (time <= 0)
		{	// gotta be a digital link for <100 ms ping
			if (chan->rate > 1.0/5000)
				chan->rate = 1.0/5000;
		}
		else
		{
			if (chan->outgoing_size[i] < 512)
			{	// only deal with small messages
				rate = chan->outgoing_size[i]/time;
				if (rate > 5000)
					rate = 5000;
				rate = 1.0/rate;
				if (chan->rate > rate)
					chan->rate = rate;
			}
		}
	}
#endif

	//
	// discard stale or duplicated packets
	//
	if(sequence <= (unsigned)chan->incoming_sequence)
	{
		if(net_showdrop.value)
			Con_Printf("%s:Out of order packet %i at %i\n", NET_AdrToString(chan->remote_address), sequence, chan->incoming_sequence);
		return false;
	}

	//
	// dropped packets don't keep the message from being used
	//
	net_drop = sequence - (chan->incoming_sequence + 1);
	if(net_drop > 0)
	{
		chan->drop_count += 1;

		if(net_showdrop.value)
			Con_Printf("%s:Dropped %i packets at %i\n", NET_AdrToString(chan->remote_address), sequence - (chan->incoming_sequence + 1), sequence);
	}

	//
	// if the current outgoing reliable message has been acknowledged
	// clear the buffer to make way for the next
	//
	if(reliable_ack == (unsigned)chan->reliable_sequence)
		chan->reliable_length = 0; // it has been received

	//
	// if this message contains a reliable message, bump incoming_reliable_sequence
	//
	chan->incoming_sequence = sequence;
	chan->incoming_acknowledged = sequence_ack;
	chan->incoming_reliable_acknowledged = reliable_ack;
	if(reliable_message)
		chan->incoming_reliable_sequence ^= 1;

	//
	// the message can now be read from the current message pointer
	// update statistics counters
	//
	chan->frame_latency = chan->frame_latency * OLD_AVG + (chan->outgoing_sequence - sequence_ack) * (1.0 - OLD_AVG);
	chan->frame_rate = chan->frame_rate * OLD_AVG + (realtime - chan->last_received) * (1.0 - OLD_AVG);
	chan->good_count += 1;

	chan->last_received = realtime;

	return true;
}

void Netchan_FragSend(netchan_t *chan)
{
	fragbufwaiting_t *wait;
	int i;

	if (!chan)
		return;

	for (i = 0; i < MAX_STREAMS; i++)
	{
		// Already something queued up, just leave in waitlist
		if (chan->fragbufs[i])
		{
			continue;
		}

		wait = chan->waitlist[i];

		// Nothing to queue?
		if (!wait)
		{
			continue;
		}

		chan->waitlist[i] = wait->next;

#ifdef REHLDS_FIXES
		if (wait->fragbufs->isfile && !wait->fragbufs->isbuffer && !wait->fragbufs->size)
		{
			if (!Netchan_CreateFileFragments_(true, chan, wait->fragbufs->filename))
			{
				Mem_Free(wait);

				continue;
			}

			fragbufwaiting_t *prev = nullptr;
			while (chan->waitlist[i]->next)
			{
				prev = chan->waitlist[i];
				chan->waitlist[i] = chan->waitlist[i]->next;
			}

			if (prev)
				prev->next = nullptr;

			auto oldWait = wait;
			wait = chan->waitlist[i];

			chan->waitlist[i] = oldWait->next;

			Mem_Free(oldWait);
		}
#endif // REHLDS_FIXES

		wait->next = nullptr;

		// Copy in to fragbuf
		chan->fragbufs[i] = wait->fragbufs;
		chan->fragbufcount[i] = wait->fragbufcount;

		// Throw away wait list
		Mem_Free(wait);
	}
}

void Netchan_AddBufferToList(fragbuf_t **pplist, fragbuf_t *pbuf)
{
	// Find best slot
	fragbuf_t *pprev, *n;
	int		id1, id2;

	pbuf->next = nullptr;

	if (!pplist)
		return;

	if (!*pplist)
	{
		pbuf->next = *pplist;
		*pplist = pbuf;
		return;
	}

	pprev = *pplist;
	while (pprev->next)
	{
		n = pprev->next; // Next item in list
		id1 = FRAG_GETID(n->bufferid);
		id2 = FRAG_GETID(pbuf->bufferid);

		if (id1 > id2)
		{
			// Insert here
			pbuf->next = n->next;
			pprev->next = pbuf;
			return;
		}

		pprev = pprev->next;
	}

	// Insert at end
	pprev->next = pbuf;
}

fragbuf_t *Netchan_AllocFragbuf()
{
	fragbuf_t *buf;

	buf = (fragbuf_t *)Mem_ZeroMalloc(sizeof(fragbuf_t));
	buf->bufferid = 0;
	buf->frag_message.cursize = 0;
	buf->frag_message.data = buf->frag_message_buf;
	buf->frag_message.maxsize = sizeof(buf->frag_message_buf);
	buf->frag_message.buffername = "Frag Buffer Alloc'd";
	buf->next = nullptr;

	return buf;
}

void Netchan_AddFragbufToTail(fragbufwaiting_t *wait, fragbuf_t *buf)
{
	fragbuf_t *p;

	buf->next = nullptr;
	wait->fragbufcount++;

	p = wait->fragbufs;
	if (p)
	{
		while (p->next)
		{
			p = p->next;
		}
		p->next = buf;
	}
	else
	{
		wait->fragbufs = buf;
	}
}

void Netchan_CreateFragments_(qboolean server, netchan_t *chan, sizebuf_t *msg)
{
	fragbuf_t *buf;
	int chunksize;
	int send;
	int remaining;
	int pos;
	int bufferid = 1;
	fragbufwaiting_t *wait, *p;

	if (msg->cursize == 0)
	{
		return;
	}

	// Compress if not done already
	if (*(uint32 *)msg->data != MAKEID('B', 'Z', '2', '\0'))
	{
		unsigned char compressed[65536];
		char hdr[4] = "BZ2";
		unsigned int compressedSize = msg->cursize - sizeof(hdr);	// we should fit in same data buffer minus 4 bytes for a header
		if (!BZ2_bzBuffToBuffCompress((char *)compressed, &compressedSize, (char *)msg->data, msg->cursize, 9, 0, 30))
		{
			Con_DPrintf("Compressing split packet (%d -> %d bytes)\n", msg->cursize, compressedSize);
			Q_memcpy(msg->data, hdr, sizeof(hdr));
			Q_memcpy(msg->data + sizeof(hdr), compressed, compressedSize);
			msg->cursize = compressedSize + sizeof(hdr);
		}
	}

	chunksize = chan->pfnNetchan_Blocksize(chan->connection_status);

	wait = (fragbufwaiting_t *)Mem_ZeroMalloc(sizeof(fragbufwaiting_t));

	remaining = msg->cursize;
	pos = 0;
	while (remaining > 0)
	{
		send = min(remaining, chunksize);
		remaining -= send;

		buf = Netchan_AllocFragbuf();
		if (!buf)
		{
			return;
		}

		buf->bufferid = bufferid++;

		// Copy in data
		SZ_Clear(&buf->frag_message);
		SZ_Write(&buf->frag_message, &msg->data[pos], send);
		pos += send;

		Netchan_AddFragbufToTail(wait, buf);
	}

	// Now add waiting list item to the end of buffer queue
	if (!chan->waitlist[FRAG_NORMAL_STREAM])
	{
		chan->waitlist[FRAG_NORMAL_STREAM] = wait;
	}
	else
	{
		p = chan->waitlist[FRAG_NORMAL_STREAM];
		while (p->next)
		{
			p = p->next;
		}
		p->next = wait;
	}
}

void Netchan_CreateFragments(qboolean server, netchan_t *chan, sizebuf_t *msg)
{
	// Always queue any pending reliable data ahead of the fragmentation buffer
	if (chan->message.cursize > 0)
	{
		Netchan_CreateFragments_(server, chan, &chan->message);
		chan->message.cursize = 0;
	}

	Netchan_CreateFragments_(server, chan, msg);
}

void Netchan_CreateFileFragmentsFromBuffer(qboolean server, netchan_t *chan, const char *filename, unsigned char *uncompressed_pbuf, int uncompressed_size)
{
	int chunksize;
	int send;
	fragbufwaiting_t *p;
	fragbuf_t *buf;
	unsigned char *pbuf;
	qboolean bCompressed;
	qboolean firstfragment;
	signed int bufferid;
	int remaining;
	int pos;
	unsigned int size;
	fragbufwaiting_t *wait;

	if (!uncompressed_size)
		return;

	bufferid = 1;
	firstfragment = TRUE;
	size = uncompressed_size;

	pbuf = (unsigned char *)Mem_Malloc(uncompressed_size);
	if (BZ2_bzBuffToBuffCompress((char*)pbuf, &size, (char*)uncompressed_pbuf, uncompressed_size, 9, 0, 30))
	{
		bCompressed = FALSE;
		Mem_Free(pbuf);
		pbuf = uncompressed_pbuf;
		size = uncompressed_size;
	}
	else
	{
		bCompressed = TRUE;
		Con_DPrintf("Compressed %s for transmission (%d -> %d)\n", filename, uncompressed_size, size);
	}

	chunksize = chan->pfnNetchan_Blocksize(chan->connection_status);
	send = chunksize;
	wait = (fragbufwaiting_t *)Mem_ZeroMalloc(0xCu);
	remaining = size;
	pos = 0;

	while (remaining > 0)
	{
		send = min(remaining, chunksize);
		buf = (fragbuf_t *)Netchan_AllocFragbuf();
		if (!buf)
		{
			Con_Printf("Couldn't allocate fragbuf_t\n");
			Mem_Free(wait);
			if (server)
				SV_DropClient(host_client, 0, "Malloc problem");
			else
				rehlds_syserror("%s:Reverse me: client-side code", __func__);

#ifdef REHLDS_FIXES
			if (bCompressed) {
				Mem_Free(pbuf);
			}
#endif
			return;
		}

		buf->bufferid = bufferid++;
		SZ_Clear(&buf->frag_message);
		if (firstfragment)
		{
			firstfragment = FALSE;
			MSG_WriteString(&buf->frag_message, filename);
			MSG_WriteString(&buf->frag_message, bCompressed ? "bz2" : "uncompressed");
			MSG_WriteLong(&buf->frag_message, uncompressed_size);
			send -= buf->frag_message.cursize;
		}

		buf->isbuffer = TRUE;
		buf->isfile = TRUE;
		buf->size = send;
		buf->foffset = pos;

		MSG_WriteBuf(&buf->frag_message, send, &pbuf[pos]);
		pos += send;
		remaining -= send;

		Netchan_AddFragbufToTail(wait, buf);
	}

	if (!chan->waitlist[FRAG_FILE_STREAM]) {
		chan->waitlist[FRAG_FILE_STREAM] = wait;
	}
	else
	{
		p = chan->waitlist[FRAG_FILE_STREAM];
		while (p->next)
			p = p->next;

		p->next = wait;
	}

#ifdef REHLDS_FIXES
	if (bCompressed) {
		Mem_Free(pbuf);
	}
#endif
}

int Netchan_CreateFileFragments(qboolean server, netchan_t *chan, const char *filename)
#ifdef REHLDS_FIXES
{
	if (!server)
		return Netchan_CreateFileFragments_(server, chan, filename);

	if (!FS_FileExists(filename))
		return FALSE;
	if (FS_FileSize(filename) > sv_filetransfermaxsize.value)
		return FALSE;

	auto wait = (fragbufwaiting_t *)Mem_ZeroMalloc(sizeof(fragbufwaiting_t));

	auto buf = Netchan_AllocFragbuf();
	buf->bufferid = 1;
	buf->isbuffer = false;
	buf->isfile = true;
	Q_strncpy(buf->filename, filename, sizeof(buf->filename));
	buf->filename[sizeof(buf->filename) - 1] = '\0';

	Netchan_AddFragbufToTail(wait, buf);

	if (!chan->waitlist[FRAG_FILE_STREAM])
	{
		chan->waitlist[FRAG_FILE_STREAM] = wait;
	}
	else
	{
		auto p = chan->waitlist[FRAG_FILE_STREAM];
		while (p->next)
			p = p->next;

		p->next = wait;
	}

	return TRUE;
}

int Netchan_CreateFileFragments_(qboolean server, netchan_t *chan, const char *filename)
#endif // REHLDS_FIXES
{
	int chunksize;
	int compressedFileTime;
	FileHandle_t hfile;
	signed int filesize;
	int remaining;
	fragbufwaiting_t *p;
	int send;
	fragbuf_t *buf;
	char compressedfilename[MAX_PATH];
	qboolean firstfragment;
	int bufferid;
	qboolean bCompressed;
	int pos;
	fragbufwaiting_t *wait;
	int uncompressed_size;

	bufferid = 1;
	firstfragment = TRUE;
	bCompressed = FALSE;
	chunksize = chan->pfnNetchan_Blocksize(chan->connection_status);

	Q_snprintf(compressedfilename, sizeof compressedfilename, "%s.ztmp", filename);
	compressedFileTime = FS_GetFileTime(compressedfilename);
	if (compressedFileTime >= FS_GetFileTime(filename) && (hfile = FS_Open(compressedfilename, "rb")))
	{
		filesize = FS_Size(hfile);
		FS_Close(hfile);
		bCompressed = TRUE;
		hfile = FS_Open(filename, "rb");
		if (!hfile)
		{
			Con_Printf("Warning:  Unable to open %s for transfer\n", filename);
			return 0;
		}

		uncompressed_size = FS_Size(hfile);
		if (uncompressed_size > sv_filetransfermaxsize.value)
		{
			FS_Close(hfile);
			Con_Printf("Warning:  File %s is too big to transfer from host %s\n", filename, NET_AdrToString(chan->remote_address));
			return 0;
		}
	}
	else
	{
		hfile = FS_Open(filename, "rb");
		if (!hfile)
		{
			Con_Printf("Warning:  Unable to open %s for transfer\n", filename);
			return 0;
		}
		filesize = FS_Size(hfile);
		if (filesize > sv_filetransfermaxsize.value)
		{
			FS_Close(hfile);
			Con_Printf("Warning:  File %s is too big to transfer from host %s\n", filename, NET_AdrToString(chan->remote_address));
			return 0;
		}

		uncompressed_size = filesize;
		if (sv_filetransfercompression.value != 0.0)
		{
			unsigned char* uncompressed = (unsigned char*)Mem_Malloc(filesize);
			unsigned char* compressed = (unsigned char*)Mem_Malloc(filesize);
			unsigned int compressedSize = filesize;
			FS_Read(uncompressed, filesize, 1, hfile);
			if (BZ_OK == BZ2_bzBuffToBuffCompress((char*)compressed, &compressedSize, (char*)uncompressed, filesize, 9, 0, 30))
			{
				FileHandle_t destFile = FS_Open(compressedfilename, "wb");
				if (destFile)
				{
					Con_DPrintf("Creating compressed version of file %s (%d -> %d)\n", filename, filesize, compressedSize);
					FS_Write(compressed, compressedSize, 1, destFile);
					FS_Close(destFile);
					filesize = compressedSize;
					bCompressed = TRUE;
				}
			}
			Mem_Free(uncompressed);
			Mem_Free(compressed);
		}
	}
	FS_Close(hfile);

	wait = (fragbufwaiting_t *)Mem_ZeroMalloc(0xCu);
	remaining = filesize;
	pos = 0;

	while (remaining)
	{
		send = min(chunksize, remaining);
		buf = Netchan_AllocFragbuf();
		if (!buf)
		{
			Con_Printf("Couldn't allocate fragbuf_t\n");
			Mem_Free(wait);
			if (server)
			{
#ifdef REHLDS_FIXES
				SV_DropClient(&g_psvs.clients[chan->player_slot - 1], 0, "Malloc problem");
#else // REHLDS_FIXES
				SV_DropClient(host_client, 0, "Malloc problem");
#endif // REHLDS_FIXES
				return 0;
			}
			else
			{
				rehlds_syserror("%s: Reverse clientside code", __func__);
				//return 0;
			}
		}

		buf->bufferid = bufferid++;
		SZ_Clear(&buf->frag_message);
		if (firstfragment)
		{
			firstfragment = FALSE;
			MSG_WriteString(&buf->frag_message, filename);
			MSG_WriteString(&buf->frag_message, bCompressed ? "bz2" : "uncompressed");
			MSG_WriteLong(&buf->frag_message, uncompressed_size);
			send -= buf->frag_message.cursize;
		}
		buf->isfile = TRUE;
		buf->iscompressed = bCompressed;
		buf->size = send;
		buf->foffset = pos;

		Q_strncpy(buf->filename, filename, MAX_PATH - 1);
		buf->filename[MAX_PATH - 1] = 0;

		pos += send;
		remaining -= send;

		Netchan_AddFragbufToTail(wait, buf);
	}

	if (!chan->waitlist[FRAG_FILE_STREAM])
	{
		chan->waitlist[FRAG_FILE_STREAM] = wait;
	}
	else
	{
		p = chan->waitlist[FRAG_FILE_STREAM];
		while (p->next)
			p = p->next;

		p->next = wait;
	}

	return 1;
}

void Netchan_FlushIncoming(netchan_t *chan, int stream)
{
	fragbuf_t *p, *n;

	SZ_Clear(&net_message);
	msg_readcount = 0;

	p = chan->incomingbufs[stream];
	while (p)
	{
		n = p->next;
		Mem_Free(p);
		p = n;
	}

	chan->incomingbufs[stream] = nullptr;
	chan->incomingready[stream] = FALSE;
}

qboolean Netchan_CopyNormalFragments(netchan_t *chan)
{
	fragbuf_t *p, *n;

	if (!chan->incomingready[FRAG_NORMAL_STREAM])
		return FALSE;

	if (!chan->incomingbufs[FRAG_NORMAL_STREAM])
	{
		Con_Printf("%s:  Called with no fragments readied\n", __func__);
		chan->incomingready[FRAG_NORMAL_STREAM] = FALSE;
		return FALSE;
	}

	p = chan->incomingbufs[FRAG_NORMAL_STREAM];

	SZ_Clear(&net_message);
	MSG_BeginReading();

#ifdef REHLDS_FIXES
	bool overflowed = false;
#endif // REHLDS_FIXES

	while (p)
	{
		n = p->next;

#ifdef REHLDS_FIXES
		if (net_message.cursize + p->frag_message.cursize <= net_message.maxsize)
			SZ_Write(&net_message, p->frag_message.data, p->frag_message.cursize);
		else
			overflowed = true;
#else // REHLDS_FIXES
		SZ_Write(&net_message, p->frag_message.data, p->frag_message.cursize);
#endif // REHLDS_FIXES

		Mem_Free(p);
		p = n;
	}

#ifdef REHLDS_FIXES
	if (overflowed)
	{
		if (chan->player_slot == 0)
		{
			Con_Printf("%s: Incoming overflowed\n", __func__);
		}
		else
		{
			Con_Printf("%s: Incoming overflowed from %s\n", __func__, g_psvs.clients[chan->player_slot - 1].name);
		}

		SZ_Clear(&net_message);

		chan->incomingbufs[FRAG_NORMAL_STREAM] = nullptr;
		chan->incomingready[FRAG_NORMAL_STREAM] = FALSE;

		return FALSE;
	}
#endif // REHLDS_FIXES

	if (*(uint32 *)net_message.data == MAKEID('B', 'Z', '2', '\0'))
	{
		char uncompressed[65536];
		unsigned int uncompressedSize = 65536;
		BZ2_bzBuffToBuffDecompress(uncompressed, &uncompressedSize, (char*)net_message.data + 4, net_message.cursize - 4, 1, 0);
		Q_memcpy(net_message.data, uncompressed, uncompressedSize);
		net_message.cursize = uncompressedSize;
	}

	chan->incomingbufs[FRAG_NORMAL_STREAM] = nullptr;
	chan->incomingready[FRAG_NORMAL_STREAM] = FALSE;

	return TRUE;
}

qboolean Netchan_CopyFileFragments(netchan_t *chan)
{
	fragbuf_t *p;
	int nsize;
	unsigned char *buffer;
	int pos;
	signed int cursize;
	char filename[MAX_PATH];
	char compressor[32];
	fragbuf_s *n;
	qboolean bCompressed;
	unsigned int uncompressedSize;


	if (!chan->incomingready[FRAG_FILE_STREAM])
		return FALSE;

	p = chan->incomingbufs[FRAG_FILE_STREAM];
	if (!p)
	{
		Con_Printf("%s:  Called with no fragments readied\n", __func__);
		chan->incomingready[FRAG_FILE_STREAM] = FALSE;
		return FALSE;
	}

	bCompressed = FALSE;
	SZ_Clear(&net_message);
	MSG_BeginReading();
	SZ_Write(&net_message, p->frag_message.data, p->frag_message.cursize);
	Q_strncpy(filename, MSG_ReadString(), sizeof(filename) - 1);
	filename[sizeof(filename) - 1] = 0;

	Q_strncpy(compressor, MSG_ReadString(), sizeof(compressor) - 1);
	compressor[sizeof(compressor) - 1] = 0;
	if (!Q_stricmp(compressor, "bz2"))
		bCompressed = TRUE;

	uncompressedSize = (unsigned int)MSG_ReadLong();

#ifdef REHLDS_FIXES
	// TODO: this condition is invalid for server->client
	// TODO: add console message for client
	// TODO: add client name to message
	if (uncompressedSize > 1024 * 64) {
		Con_Printf("Received too large file (size=%u)\nFlushing input queue\n", uncompressedSize);
		Netchan_FlushIncoming(chan, 1);
		return FALSE;
	}
#endif

	if (Q_strlen(filename) <= 0)
	{
		Con_Printf("File fragment received with no filename\nFlushing input queue\n");
		Netchan_FlushIncoming(chan, 1);
		return FALSE;
	}

	if (Q_strstr(filename, ".."))
	{
		Con_Printf("File fragment received with relative path, ignoring\n");
		Netchan_FlushIncoming(chan, 1);
		return FALSE;
	}

	if (filename[0] != '!' && !IsSafeFileToDownload(filename))
	{
		Con_Printf("File fragment received with bad path, ignoring\n");
		Netchan_FlushIncoming(chan, 1);
		return FALSE;
	}
	// This prohibits to write files to FS on server
	if (g_pcls.state == ca_dedicated && filename[0] != '!')
	{
		Con_Printf("File fragment received with bad path, ignoring (2)\n");
		Netchan_FlushIncoming(chan, 1);
		return FALSE;
	}

	Q_strncpy(chan->incomingfilename, filename, MAX_PATH - 1);
	chan->incomingfilename[MAX_PATH - 1] = 0;

	if (filename[0] != '!' && FS_FileExists(filename))
	{
		Con_Printf("Can't download %s, already exists\n", filename);
		Netchan_FlushIncoming(chan, 1);
		return TRUE;
	}

	nsize = 0;
	while (p)
	{
		nsize += p->frag_message.cursize;
		if (p == chan->incomingbufs[FRAG_FILE_STREAM])
			nsize -= msg_readcount;
		p = p->next;
	}

	buffer = (unsigned char*)Mem_ZeroMalloc(nsize + 1);
	if (!buffer)
	{
		Con_Printf("Buffer allocation failed on %i bytes\n", nsize + 1);
		Netchan_FlushIncoming(chan, 1);
		return FALSE;
	}

	p = chan->incomingbufs[FRAG_FILE_STREAM];
	pos = 0;
	while (p)
	{
		n = p->next;

		cursize = p->frag_message.cursize;
		// First message has the file name, don't write that into the data stream, just write the rest of the actual data
		if (p == chan->incomingbufs[FRAG_FILE_STREAM])
		{
			// Copy it in
			cursize -= msg_readcount;
			Q_memcpy(&buffer[pos], &p->frag_message.data[msg_readcount], cursize);
			p->frag_message.cursize = cursize;
		}
		else
		{
			Q_memcpy(&buffer[pos], p->frag_message.data, cursize);
		}
		pos += p->frag_message.cursize;
		Mem_Free(p);
		p = n;

	}

	if (bCompressed)
	{
		unsigned char* uncompressedBuffer = (unsigned char*)Mem_Malloc(uncompressedSize);
		Con_DPrintf("Decompressing file %s (%d -> %d)\n", filename, nsize, uncompressedSize);
		BZ2_bzBuffToBuffDecompress((char*)uncompressedBuffer, &uncompressedSize, (char*)buffer, nsize, 1, 0);
		Mem_Free(buffer);
		pos = uncompressedSize;
		buffer = uncompressedBuffer;
	}

	if (filename[0] == '!')
	{
		if (chan->tempbuffer)
		{
			Con_DPrintf("Netchan_CopyFragments:  Freeing holdover tempbuffer\n");
			Mem_Free(chan->tempbuffer);
		}
		chan->tempbuffer = buffer;
		chan->tempbuffersize = pos;
	}
	else
	{
		char filedir[MAX_PATH];
		char *pszFileName;
		FileHandle_t handle;

#ifdef REHLDS_CHECKS
		Q_strncpy(filedir, filename, sizeof(filedir) - 1);
		filedir[sizeof(filedir) - 1] = 0;
#else
		Q_strncpy(filedir, filename, sizeof(filedir));
#endif // REHLDS_CHECKS
		COM_FixSlashes(filedir);
		pszFileName = Q_strrchr(filedir, '\\');
		if (pszFileName)
		{
			*pszFileName = 0;

#ifdef REHLDS_FIXES
			FS_CreateDirHierarchy(filedir, "GAMEDOWNLOAD");
#endif
		}

#ifndef REHLDS_FIXES
		FS_CreateDirHierarchy(filedir, "GAMEDOWNLOAD");
#endif
		handle = FS_OpenPathID(filename, "wb", "GAMEDOWNLOAD");
		if (!handle)
		{
			Con_Printf("File open failed %s\n", filename);
			Netchan_FlushIncoming(chan, 1);

#ifdef REHLDS_FIXES
			Mem_Free(buffer);
#endif
			return FALSE;
		}

		Sys_Printf("COM_WriteFile: %s\n", filename);
		FS_Write(buffer, pos, 1, handle);
		FS_Close(handle);

		Mem_Free(buffer);
	}
	SZ_Clear(&net_message);
	chan->incomingbufs[FRAG_FILE_STREAM] = nullptr;
	chan->incomingready[FRAG_FILE_STREAM] = FALSE;
	msg_readcount = 0;
	return TRUE;
}

NOXREF qboolean Netchan_IsSending(netchan_t *chan)
{
	NOXREFCHECK;
	int i;
	for (i = 0; i < MAX_STREAMS; i++)
	{
		if (chan->fragbufs[i])
			return TRUE;
	}
	return FALSE;
}

NOXREF qboolean Netchan_IsReceiving(netchan_t *chan)
{
	NOXREFCHECK;
	int i;
	for (i = 0; i < MAX_STREAMS; i++)
	{
		if (chan->incomingbufs[i])
			return TRUE;
	}
	return FALSE;
}

qboolean Netchan_IncomingReady(netchan_t *chan)
{
	for (int i = 0; i < MAX_STREAMS; i++)
	{
		if (chan->incomingready[i])
			return TRUE;
	}
	return FALSE;
}

NOXREF void Netchan_UpdateProgress(netchan_t *chan)
{
	NOXREFCHECK;
	fragbuf_t *p;
	int c = 0;
	int total = 0;
	int i;
	float bestpercent = 0.0f;
	float percent;
	char sz[1024];
	char *in;
	char *out;
	int len = 0;

	scr_downloading.value = -1.0f;
	gDownloadFile[0] = 0;

	if (net_drawslider.value != 1.0f)
	{
		if (!chan->incomingbufs[FRAG_FILE_STREAM])
			return;
	}
	for (i = MAX_STREAMS - 1; i >= 0; i--)
	{
		if (chan->incomingbufs[i])
		{
			p = chan->incomingbufs[i];
			total = FRAG_GETCOUNT(p->bufferid);

			while (p)
			{
				c++;
				p = p->next;
			}

			if (total)
			{
				percent = (float)c * 100.0f / (float)total;
				if (percent > bestpercent)
					bestpercent = percent;
			}
			p = chan->incomingbufs[i];
			if (i == FRAG_FILE_STREAM)
			{
				in = (char *)p->frag_message.data;
				out = sz;

				while (*in)
				{
					*out++ = *in++;
					len++;

					if (len > 128)
						break;
				}

				*out = '\0';
				if (Q_strlen(sz) > 0 && (*sz != '!' || !Q_strncmp(sz, "!ModuleC.dll", 11)))
				{
					Q_strncpy(gDownloadFile, sz, 255);
					gDownloadFile[255] = 0;
				}
			}
		}
		else if (chan->fragbufs[i])
		{
			if (chan->fragbufcount[i])
			{
				percent = (float)chan->fragbufs[i]->bufferid * 100.0f / (float)chan->fragbufcount[i];
				if (percent > bestpercent)
					bestpercent = percent;
			}
		}
	}

	scr_downloading.value = bestpercent;
}

/*
===============
Netchan_Init

===============
*/
void Netchan_Init()
{
/*
	int port;

// pick a port value that should be nice and random
#ifdef _WIN32
	port = ((int)(timeGetTime() * 1000) * time(NULL)) & 0xffff;
#else
	port = ((int)(getpid() + getuid() * 1000) * time(NULL)) & 0xffff;
#endif
*/

	Cvar_RegisterVariable(&net_log);
	Cvar_RegisterVariable(&net_showpackets);
	Cvar_RegisterVariable(&net_showdrop);
	Cvar_RegisterVariable(&net_chokeloopback);
	Cvar_RegisterVariable(&net_drawslider);
}

NOXREF qboolean Netchan_CompressPacket(sizebuf_t *chan)
{
	NOXREFCHECK;
	return FALSE;
}

NOXREF qboolean Netchan_DecompressPacket(sizebuf_t *chan)
{
	NOXREFCHECK;
	return FALSE;
}