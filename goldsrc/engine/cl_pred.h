/// @file

#pragma once

#include "common/entity_state.h"
#include "common/usercmd.h"

void CL_PushPMStates();
void CL_PopPMStates();

void CL_RunUsercmd(local_state_t *from, local_state_t *to, usercmd_t *u, qboolean runfuncs, double *pfElapsed, uint random_seed);