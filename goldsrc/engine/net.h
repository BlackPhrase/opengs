/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
/*
*
*    This program is free software; you can redistribute it and/or modify it
*    under the terms of the GNU General Public License as published by the
*    Free Software Foundation; either version 2 of the License, or (at
*    your option) any later version.
*
*    This program is distributed in the hope that it will be useful, but
*    WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software Foundation,
*    Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*    In addition, as a special exception, the author gives permission to
*    link the code of this program with the Half-Life Game Engine ("HL
*    Engine") and Modified Game Libraries ("MODs") developed by Valve,
*    L.L.C ("Valve").  You must obey the GNU General Public License in all
*    respects for all of the code used other than the HL Engine and MODs
*    from Valve.  If you modify this file, you may extend this exception
*    to your version of the file, but you are not obligated to do so.  If
*    you do not wish to do so, delete this exception statement from your
*    version.
*
*/

/// @file
/// @brief engine's interface to the networking layer

#pragma once

//#include <cstdlib>
//#include "tier0/platform.h"
#include "common/netadr.h"
#include "common/enums.h"

//============================================================================

//#define OLD_AVG 0.99 // total = oldtotal*OLD_AVG + new*(1-OLD_AVG)

// Flow control bytes per second limits
const float MAX_RATE		= 100000.0f;
const float MIN_RATE		= 1000.0f;

// Default data rate
const float DEFAULT_RATE	= 9999.0f;

const int PORT_ANY = -1;

// NETWORKING INFO

// This is the packet payload without any header bytes (which are attached for actual sending)
const int NET_MAX_PAYLOAD	= 65536;

// Max size of udp packet payload
const int MAX_UDP_PACKET	= 4010; // 9 bytes SPLITHEADER + 4000 payload?

// UDP has 28 byte headers
const int UDP_HEADER_SIZE = 28; // IP = 20, UDP = 8

const int MAX_RELIABLE_PAYLOAD = 1200;

// Max length of a reliable message
//const int MAX_MSGLEN		= 3990; // 10 reserved for fragheader? // TODO: defined in quakedef

// Max length of unreliable message
//const int MAX_DATAGRAM		= 4000; // TODO: defined in quakedef

/**
*	Maximum size for a fragment buffer.
*/
//TODO: unrelated to actual net constants, remove - Solokiller
const size_t NET_MAX_FRAG_BUFFER = 1400;

// This is the payload plus any header info (excluding UDP header)

// Packet header is:
//  4 bytes of outgoing seq
//  4 bytes of incoming seq
//  and for each stream
// {
//  byte (on/off)
//  int (fragment id)
//  short (startpos)
//  short (length)
// }
#define HEADER_BYTES (8 + MAX_STREAMS * 9)

// Pad this to next higher 16 byte boundary
// This is the largest packet that can come in/out over the wire, before processing the header
//  bytes will be stripped by the networking channel layer
//#define NET_MAX_MESSAGE PAD_NUMBER( ( MAX_MSGLEN + HEADER_BYTES ), 16 )
// This is currently used value in the engine. TODO: define above gives 4016, check it why.
const int NET_MAX_MESSAGE = 4037;

// 0 == regular, 1 == file stream
enum
{
	FRAG_NORMAL_STREAM = 0,
	FRAG_FILE_STREAM,

	MAX_STREAMS
};

enum
{
	FLOW_OUTGOING = 0,
	FLOW_INCOMING,

	MAX_FLOWS ///< in & out
};

//============================================================================

extern netadr_t net_local_adr;
extern netadr_t net_from; // address of who sent the packet

extern sizebuf_t net_message;

extern cvar_t hostname;

extern int net_socket;

//============================================================================

void NET_Init();
void NET_Shutdown();

void NET_Config(qboolean multiplayer);

void NET_Sleep(int msec);

qboolean NET_GetPacket(netsrc_t sock, netadr_t *net_from, sizebuf_t *net_message);
void NET_SendPacket(netsrc_t sock, int length, void *data, netadr_t to);

qboolean NET_IsLocalAddress(netadr_t adr);

qboolean NET_CompareAdr(netadr_t a, netadr_t b);
qboolean NET_CompareBaseAdr(netadr_t a, netadr_t b);

char *NET_AdrToString(netadr_t a);
char *NET_BaseAdrToString(netadr_t a);

qboolean NET_StringToAdr(char *s, netadr_t *a);