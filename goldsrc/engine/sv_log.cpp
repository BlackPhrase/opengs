/*
*
*    This program is free software; you can redistribute it and/or modify it
*    under the terms of the GNU General Public License as published by the
*    Free Software Foundation; either version 2 of the License, or (at
*    your option) any later version.
*
*    This program is distributed in the hope that it will be useful, but
*    WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software Foundation,
*    Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*    In addition, as a special exception, the author gives permission to
*    link the code of this program with the Half-Life Game Engine ("HL
*    Engine") and Modified Game Libraries ("MODs") developed by Valve,
*    L.L.C ("Valve").  You must obey the GNU General Public License in all
*    respects for all of the code used other than the HL Engine and MODs
*    from Valve.  If you modify this file, you may extend this exception
*    to your version of the file, but you are not obligated to do so.  If
*    you do not wish to do so, delete this exception statement from your
*    version.
*
*/

/// @file

#include <cstdarg>
#include <cstdio>
#include <ctime>

//#include "precompiled.h"
#include "quakedef.h"
#include "buildnum.h"
#include "server.h"
#include "sv_log.h"

const int MAX_DAILY_LOG_FILES = 1000;

struct LOGLIST_T
{
	server_log_t log;
	LOGLIST_T *next;
};

static LOGLIST_T *firstLog{nullptr}; // TODO: make non-static?

cvar_t sv_log_onefile = { "sv_log_onefile", "0" };
cvar_t sv_log_singleplayer = { "sv_log_singleplayer", "0" };
cvar_t sv_logsecret = { "sv_logsecret", "0" };

cvar_t mp_logecho = { "mp_logecho", "1" };
cvar_t mp_logfile = { "mp_logfile", "1", FCVAR_SERVER };

void Log_Printf(const char *fmt, ...)
{
	va_list va;

	va_start(va, fmt);

	if(svs.log.net_log || firstLog || svs.log.active)
	{
		static char string[1024];

		time_t ltime;
		time(&ltime);
		tm *pTime = localtime(&ltime);

		snprintf(
		string,
		sizeof(string),
		"L %02i/%02i/%04i - %02i:%02i:%02i: ",
		pTime->tm_mon + 1,
		pTime->tm_mday,
		pTime->tm_year + 1900,
		pTime->tm_hour,
		pTime->tm_min,
		pTime->tm_sec);

		const size_t uiLength = Q_strlen(string);

		vsnprintf(&string[uiLength], sizeof(string) - uiLength, fmt, va);

		if(svs.log.net_log)
			Netchan_OutOfBandPrint(NS_SERVER, svs.log.net_address, "log %s", string);

		for(auto pLog = firstLog; pLog; pLog = pLog->next)
		{
			if(!sv_logsecret.value)
				Netchan_OutOfBandPrint(NS_SERVER, pLog->log.net_address, "log %s", string);
			else
				Netchan_OutOfBandPrint(NS_SERVER, pLog->log.net_address, "%c%s%s", 83, sv_logsecret.string, string);
		}

		if(svs.log.active && (svs.maxclients > 1 || sv_log_singleplayer.value != 0.0))
		{
			if(mp_logecho.value != 0.0)
				Con_Printf("%s", string);

			if(svs.log.file != FILESYSTEM_INVALID_HANDLE)
			{
				if(mp_logfile.value != 0.0)
					FS_FPrintf(svs.log.file, "%s", string);
			}
		}
	}

	va_end(va);
}

void Log_PrintServerVars()
{
	if(!svs.log.active)
		return;
	
	Log_Printf("Server cvars start\n");
	for(cvar_t *var = cvar_vars; var /*!= nullptr*/; var = var->next)
	{
		if(var->flags & FCVAR_SERVER)
			Log_Printf("Server cvar \"%s\" = \"%s\"\n", var->name, var->string);
	}

	Log_Printf("Server cvars end\n");
}

void Log_Close()
{
	if(svs.log.file)
	{
		Log_Printf("Log file closed\n");
		FS_Close(/*(FileHandle_t)*/svs.log.file);
	}
	svs.log.file = FILESYSTEM_INVALID_HANDLE; // nullptr
}

void Log_Open()
{
	if(!svs.log.active && (!sv_log_onefile.value || !svs.log.file))
		return;
	
	if(!mp_logfile.value) // if(mp_logfile.value == 0.0f)
		Con_Printf("Server logging data to console.\n");
	else
	{
		Log_Close();

		time_t ltime;
		time(&ltime);
		auto pTm = localtime(&ltime);
		auto pszLogsDir = Cvar_VariableString("logsdir");

		char szFileBase[MAX_PATH];

		if(!pszLogsDir ||
		   Q_strlen(pszLogsDir) <= 0 ||
		   Q_strstr(pszLogsDir, ":") ||
		   Q_strstr(pszLogsDir, ".."))
		{
			snprintf(szFileBase, ARRAYSIZE(szFileBase), "logs/L%02i%02i", pTm->tm_mon + 1, pTm->tm_mday);
		}
		else
			snprintf(szFileBase, ARRAYSIZE(szFileBase), "%s/L%02i%02i", pszLogsDir, pTm->tm_mon + 1, pTm->tm_mday);

		char szTestFile[MAX_PATH];

		int i;

		for(i = 0; i < MAX_DAILY_LOG_FILES; ++i)
		{
			snprintf(szTestFile, ARRAYSIZE(szTestFile), "%s%03i.log", szFileBase, i);

			COM_FixSlashes(szTestFile);
			COM_CreatePath(szTestFile);

			auto hFile = FS_OpenPathID(szTestFile, "r", "GAMECONFIG");
			if(!hFile)
				break;

			FS_Close(hFile);
		}

		if(i < MAX_DAILY_LOG_FILES)
		{
			COM_CreatePath(szTestFile);

			auto hFile = FS_OpenPathID(szTestFile, "wt", "GAMECONFIG");

			if(hFile)
			{
				Con_Printf("Server logging data to file %s\n", szTestFile);
				svs.log.file = hFile;

				Log_Printf(
				"Log file started (file \"%s\") (game \"%s\") (version \"%i/%s/%d\")\n",
				szTestFile,
				Info_ValueForKey(Info_Serverinfo(), "*gamedir"),
				PROTOCOL_VERSION,
				gpszVersionString,
				build_number());
				return;
			}
		}

		Con_Printf("Unable to open logfiles under %s\nLogging disabled\n", szFileBase);
		svs.log.active = false;
	}
}

void SV_SetLogAddress_f()
{
	const char *s;
	int nPort;
	char szAdr[MAX_PATH];
	netadr_t adr;

	if (Cmd_Argc() != 3)
	{
		Con_Printf("logaddress:  usage\nlogaddress ip port\n");
		if (svs.log.active)
			Con_Printf("current:  %s\n", NET_AdrToString(svs.log.net_address_));
		return;
	}

	nPort = Q_atoi(Cmd_Argv(2));
	if (!nPort)
	{
		Con_Printf("logaddress:  must specify a valid port\n");
		return;
	}

	s = Cmd_Argv(1);
	if (!s || *s == '\0')
	{
		Con_Printf("logaddress:  unparseable address\n");
		return;
	}

	Q_snprintf(szAdr, sizeof(szAdr), "%s:%i", s, nPort);

	if (!NET_StringToAdr(szAdr, &adr))
	{
		Con_Printf("logaddress:  unable to resolve %s\n", szAdr);
		return;
	}

	svs.log.net_log_ = TRUE;
	Q_memcpy(&svs.log.net_address_, &adr, sizeof(netadr_t));
	Con_Printf("logaddress:  %s\n", NET_AdrToString(adr));
}

// REHLDS STUFF BELOW

void SV_AddLogAddress_f()
{
	const char *s;
	int nPort;
	char szAdr[MAX_PATH];
	netadr_t adr;
	LOGLIST_T *list;
	qboolean found = FALSE;
	LOGLIST_T *tmp;

	if (Cmd_Argc() != 3)
	{
		Con_Printf("logaddress_add:  usage\nlogaddress_add ip port\n");
		for (list = firstLog; list != NULL; list = list->next)
			Con_Printf("current:  %s\n", NET_AdrToString(list->log.net_address_));
		return;
	}

	nPort = Q_atoi(Cmd_Argv(2));
	if (!nPort)
	{
		Con_Printf("logaddress_add:  must specify a valid port\n");
		return;
	}

	s = Cmd_Argv(1);
	if (!s || *s == '\0')
	{
		Con_Printf("logaddress_add:  unparseable address\n");
		return;
	}
	Q_snprintf(szAdr, sizeof(szAdr), "%s:%i", s, nPort);

	if (!NET_StringToAdr(szAdr, &adr))
	{
		Con_Printf("logaddress_add:  unable to resolve %s\n", szAdr);
		return;
	}

	if (firstLog)
	{
		for (list = firstLog; list != NULL; list = list->next)
		{
#ifdef REHLDS_FIXES
			//for IPX support
			if (NET_CompareAdr(adr, list->log.net_address_))
#else
			if (Q_memcmp(adr.ip, list->log.net_address_.ip, 4) == 0 && adr.port == list->log.net_address_.port)
#endif // REHLDS_FIXES
			{
				found = TRUE;
				break;
			}
		}
		if (found)
		{
			Con_Printf("logaddress_add:  address already in list\n");
			return;
		}
		tmp = (LOGLIST_T *)Mem_Malloc(sizeof(LOGLIST_T));
		if (!tmp)
		{
			Con_Printf("logaddress_add:  error allocating new node\n");
			return;
		}

		tmp->next = NULL;
		Q_memcpy(&tmp->log.net_address_, &adr, sizeof(netadr_t));

		list = firstLog;

		while (list->next)
			list = list->next;

		list->next = tmp;
	}
	else
	{
		firstLog = (LOGLIST_T *)Mem_Malloc(sizeof(LOGLIST_T));
		if (!firstLog)
		{
			Con_Printf("logaddress_add:  error allocating new node\n");
			return;
		}
		firstLog->next = NULL;
		Q_memcpy(&firstLog->log.net_address_, &adr, sizeof(netadr_t));
	}

	Con_Printf("logaddress_add:  %s\n", NET_AdrToString(adr));
}

void SV_DelLogAddress_f()
{
	const char *s;
	int nPort;
	char szAdr[MAX_PATH];
	netadr_t adr;
	LOGLIST_T *list;
	LOGLIST_T *prev;
	qboolean found = FALSE;

	if (Cmd_Argc() != 3)
	{
		Con_Printf("logaddress_del:  usage\nlogaddress_del ip port\n");
		for (list = firstLog; list != NULL; list = list->next)
			Con_Printf("current:  %s\n", NET_AdrToString(list->log.net_address_));
		return;
	}
	nPort = Q_atoi(Cmd_Argv(2));
	if (!nPort)
	{
		Con_Printf("logaddress_del:  must specify a valid port\n");
		return;
	}

	s = Cmd_Argv(1);
	if (!s || *s == '\0')
	{
		Con_Printf("logaddress_del:  unparseable address\n");
		return;
	}
	Q_snprintf(szAdr, sizeof(szAdr), "%s:%i", s, nPort);
	if (!NET_StringToAdr(szAdr,&adr))
	{
		Con_Printf("logaddress_del:  unable to resolve %s\n", szAdr);
		return;
	}
	if (!firstLog)
	{
		Con_Printf("logaddress_del:  No addresses added yet\n");
		return;
	}
	for(list = firstLog, prev = firstLog; list != NULL; list = list->next)
	{
#ifdef REHLDS_FIXES
		//for IPX
		if (NET_CompareAdr(adr,list->log.net_address_))
#else
		if (Q_memcmp(adr.ip, list->log.net_address_.ip, 4) == 0 && adr.port == list->log.net_address_.port)
#endif // REHLDS_FIXES
		{
			found = TRUE;
			if (list == prev)
			{
				firstLog = prev->next;
				Mem_Free(prev);
			}
			else
			{
				prev->next = list->next;
				Mem_Free(list);
			}
			break;
		}
		prev = list;
	}
	if (!found)
	{
		Con_Printf("logaddress_del:  Couldn't find address in list\n");
		return;
	}
	Con_Printf("deleting:  %s\n", NET_AdrToString(adr));
}

void SV_ServerLog_f()
{
	if (Cmd_Argc() != 2)
	{
		Con_Printf("usage:  log < on | off >\n");

		if (svs.log.active)
			Con_Printf("currently logging\n");

		else Con_Printf("not currently logging\n");
		return;
	}

	const char *s = Cmd_Argv(1);
	if (Q_stricmp(s, "off"))
	{
		if (Q_stricmp(s, "on"))
			Con_Printf("log:  unknown parameter %s, 'on' and 'off' are valid\n", s);
		else
		{
			svs.log.active = TRUE;
			Log_Open();
		}
	}
	else if (svs.log.active)
	{
		Log_Close();
		Con_Printf("Server logging disabled.\n");
		svs.log.active = FALSE;
	}
}