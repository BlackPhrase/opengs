/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/// @file
/// @brief parse a message received from the server

//#include "precompiled.h"
#include "quakedef.h"
//#include "client.h"
//#include "cl_parse.h"

int CL_UPDATE_BACKUP = 1 << 6;
int CL_UPDATE_MASK = CL_UPDATE_BACKUP - 1;

const char *svc_strings[] =
{
  "svc_bad",
  "svc_nop",
  "svc_disconnect",
  "svc_updatestat",
  "svc_version",   // [long] server version
  "svc_setview",   // [short] entity number
  "svc_sound",     // <see code>
  "svc_time",      // [float] server time
  "svc_print",     // [string] null terminated string
  "svc_stufftext", // [string] stuffed into client's console buffer
	               // the string should be \n terminated
  "svc_setangle",  // [vec3] set the view angle to this absolute value

  "svc_serverinfo",   // [long] version
	                  // [string] signon string
	                  // [string]..[0]model cache [string]...[0]sounds cache
	                  // [string]..[0]item cache
  "svc_lightstyle",   // [byte] [string]
  "svc_updatename",   // [byte] [string]
  "svc_updatefrags",  // [byte] [short]
  "svc_clientdata",   // <shortbits + data>
  "svc_stopsound",    // <see code>
  "svc_updatecolors", // [byte] [byte]
  "svc_particle",     // [vec3] <variable>
  "svc_damage",       // [byte] impact [byte] blood [vec3] from

  "svc_spawnstatic",
  "OBSOLETE svc_spawnbinary",
  "svc_spawnbaseline",

  "svc_temp_entity", // <variable>
  "svc_setpause",
  "svc_signonnum",
  "svc_centerprint",
  "svc_killedmonster",
  "svc_foundsecret",
  "svc_spawnstaticsound",
  "svc_intermission",
  "svc_finale",  // [string] music [string] text
  "svc_cdtrack", // [byte] track [byte] looptrack
  "svc_sellscreen",
  "svc_cutscene"
};

//=============================================================================

void CL_ShutDownUsrMessages()
{
	//TODO: implement - Solokiller
}

pfnUserMsgHook HookServerMsg(const char *pszName, pfnUserMsgHook pfn)
{
	//TODO: implement - Solokiller
	return nullptr;
}

void CL_RemoveFromResourceList(resource_t *pResource)
{
	if(!pResource->pPrev || !pResource->pNext)
		Sys_Error("Mislinked resource in CL_RemoveFromResourceList");

	if(pResource == pResource->pNext || pResource->pPrev == pResource)
		Sys_Error("Attempt to free last entry in list.");

	pResource->pPrev->pNext = pResource->pNext;
	pResource->pNext->pPrev = pResource->pPrev;
	pResource->pPrev = nullptr;
	pResource->pNext = nullptr;
}

void CL_ClearResourceLists()
{
	cl.downloadUrl[0] = '\0';

	for(resource_t *pResource = cl.resourcesneeded.pNext, *pNext;
	    pResource != &cl.resourcesneeded;
	    pResource = pNext)
	{
		pNext = pResource->pNext;

		CL_RemoveFromResourceList(pResource);
		Mem_Free(pResource);
	}

	cl.resourcesneeded.pNext = &cl.resourcesneeded;
	cl.resourcesneeded.pPrev = &cl.resourcesneeded;

	for(resource_t *pResource = cl.resourcesonhand.pNext, *pNext;
	    pResource != &cl.resourcesonhand;
	    pResource = pNext)
	{
		pNext = pResource->pNext;

		CL_RemoveFromResourceList(pResource);
		Mem_Free(pResource);
	}

	cl.resourcesonhand.pNext = &cl.resourcesonhand;
	cl.resourcesonhand.pPrev = &cl.resourcesonhand;
}

/*
===============
CL_EntityNum

This error checks and tracks the total number of entities
===============
*/
cl_entity_t *CL_EntityNum(int num)
{
	if(num >= cl.num_entities)
	{
		if(num >= MAX_EDICTS)
			Host_Error("CL_EntityNum: %i is an invalid number", num);

		while(cl.num_entities <= num)
		{
			cl_entities[cl.num_entities].colormap = vid.colormap;
			cl.num_entities++;
		}
	}

	return &cl_entities[num];
}

void CL_HandleDisconnect()
{
	char *sReason = MSG_ReadString();

	if(cls.state == ca_connected)
		Host_EndGame("Server disconnected\n"
		             "Server version may be incompatible");
	else
		Host_EndGame("Server disconnected");
};

/*
	Note: This message can be dropped if the client already has too much content
	in its unreliable buffer.
	Note: Events can be precached using pfnPrecacheEvent routine.
	Note: Events are queued and grouped together every frame, if there's any.
	Note: EventArgs are always inherited from "null" event args.
	Note: Only a max of 31 events can be queued and subsequently sent this way.
	Note: This message has its arguments in bit-packed form.
*/
void CL_ParseEventData(bool bReliable)
{
	if(bReliable)
	{
	};
};

// Seems to be unused
void CL_ParseVersion(){
	// long nServerProtocol = MSG_ReadLong();
	// if(PROTOCOL_VERSION != nServerProtocol)
	// Host_Error("CL_Parse_Version: Server is protocol %i instead of %i\n",
	// nServerProtocol, PROTOCOL_VERSION);
};

void CL_ParseView(){
	//cl.refdef.viewentity = MSG_ReadWord();
};

/*
==================
CL_ParseStartSoundPacket
==================
*/
void CL_ParseStartSoundPacket()
{
	vec3_t pos;

	int flags = MSG_ReadWord();

	int volume = DEFAULT_SOUND_PACKET_VOLUME; // VOL_NORM

	//if(flags & SND_FL_VOLUME)
	//volume = MSG_ReadByte();

	float attenuation = DEFAULT_SOUND_PACKET_ATTENUATION; // ATTN_NONE

	//if(flags & SND_FL_ATTENUATION)
	//attenuation = MSG_ReadByte() / 64.0f;

	int channel = MSG_ReadShort();
	int ent = MSG_ReadByte();
	int sound_num = MSG_ReadByte();

	for(int i = 0; i < 3; i++)
		pos[i] = MSG_ReadCoord();

	int pitch = DEFAULT_SOUND_PACKET_PITCH; // PITCH_NORM

	//if(flags & SND_FL_PITCH)
	//pitch = MSG_ReadByte();

	ent = (channel >> 3) & 1023;
	channel &= 7;

	if(ent > MAX_EDICTS)
		Host_EndGame("CL_ParseStartSoundPacket: ent = %i", ent);

	//S_StartSound(ent, channel, cl.sound_precache[sound_num], pos, volume / 255.0, attenuation);
};

/*
	Notifies clients about the current server time
	Note: This message is sent every frame by the server
*/
void CL_ParseTime()
{
	// shuffle timestamps
	cl.mtime[1] = cl.mtime[0];
	cl.mtime[0] = MSG_ReadFloat();
};

void CL_ParsePrint()
{
	int i = MSG_ReadByte();

	//if(i == PRINT_CHAT)
	//{
	//S_LocalSound("misc/talk.wav");
	//con_ormask = 128;
	//};

	Con_Printf("%s", MSG_ReadString());
	con_ormask = 0;
};

void CL_ParseStuffText()
{
	char *s = MSG_ReadString();
	Con_DPrintf("stufftext: %s\n", s);
	Cbuf_AddText(s);

	//if(cl_trace_stufftext->value)
	//Msg("^3STUFFTEXT:\n^2%s\n^3END^7\n", s);
};

/*
================
CL_ParseSetAngle

set the view angle to this absolute value
================
*/
void CL_ParseSetAngle()
{
	for(int i = 0; i < 3; ++i)
		cl.viewangles[i] = MSG_ReadAngle();

	// cl.viewangles[PITCH] = cl.viewangles[ROLL] = 0;

	/*
  cl.refdef.cl_viewangles[0] = MSG_ReadBitAngle(16 );
  cl.refdef.cl_viewangles[1] = MSG_ReadBitAngle(16 );
  cl.refdef.cl_viewangles[2] = MSG_ReadBitAngle(16 );
  */
};

/*
==============
CL_ServerInfo
==============
*/
void CL_ParseServerInfo()
{
	int slot;
	player_info_t *player;
	char key[MAX_MSGLEN];
	char value[MAX_MSGLEN];

	Q_strncpy(key, MSG_ReadString(), charsmax(key));
	key[sizeof(key) - 1] = 0;
	Q_strncpy(value, MSG_ReadString(), charsmax(value));
	key[sizeof(value) - 1] = 0;

	Con_DPrintf("SERVERINFO: %s=%s\n", key, value);

	//Info_SetValueForKey(cl.serverinfo, key, value, MAX_SERVERINFO_STRING);
};

/*
================
CL_ParseLightStyle
================
*/
void CL_ParseLightStyle()
{
	int nStyle = MSG_ReadByte();

	if(nStyle >= MAX_LIGHTSTYLES)
		Sys_Error("svc_lightstyle > MAX_LIGHTSTYLES");

	//Q_strcpy(cl_lightstyle[nStyle].map, MSG_ReadString());
	//cl_lightstyle[nStyle].length = Q_strlen(cl_lightstyle[nStyle].map);

	/*
	int style = MSG_ReadByte();
	const char *s = MSG_ReadString();
	float f = MSG_ReadFloat();

	CL_SetLightstyle( style, s, f );
	*/
};

/*
================
CL_UpdateUserinfo

collect userinfo from all players
================
*/
void CL_ParseUserinfo()
{
	int slot = 0; //MSG_ReadUBitLong(MAX_CLIENT_BITS);

	if(slot >= MAX_CLIENTS)
		Host_Error("CL_ParseServerMessage: svc_updateuserinfo > MAX_CLIENTS\n");

	player_info_t *player = &cl.players[slot];
	qboolean active = MSG_ReadOneBit() ? true : false;

	if(active)
	{
		Q_strncpy(player->userinfo, MSG_ReadString(), sizeof(player->userinfo));
		Q_strncpy(player->name, Info_ValueForKey(player->userinfo, "name"), sizeof(player->name));
		Q_strncpy(player->model, Info_ValueForKey(player->userinfo, "model"), sizeof(player->model));
		player->topcolor = Q_atoi(Info_ValueForKey(player->userinfo, "topcolor"));
		player->bottomcolor =
		Q_atoi(Info_ValueForKey(player->userinfo, "bottomcolor"));

		//if(slot == cl.playernum)
		//Q_memcpy(&menu.playerinfo, player, sizeof(player_info_t));
	}
	else
		Q_memset(player, 0, sizeof(*player));
};

void CL_ParseDeltaDescription(){};

/*
==================
CL_ParseClientdata

Server information pertaining to this client only, sent every frame
==================
*/
void CL_ParseClientdata()
{
	int i;
	float latency;
	frame_t *frame;

	// calculate simulated time of message
	//oldparsecountmod = parsecountmod;

	i = cls.netchan.incoming_acknowledged;
	cl.parsecount = i;
	//i &= UPDATE_MASK;
	//parsecountmod = i;
	frame = &cl.frames[i];
	//parsecounttime = cl.frames[i].senttime;

	frame->receivedtime = realtime;

	// calculate latency
	//latency = frame->receivedtime - frame->senttime;

	if(latency < 0 || latency > 1.0f)
	{
		//		Con_Printf ("Odd latency: %5.2f\n", latency);
	}
	else
	{
		// drift the average latency towards the observed latency
		//if(latency < cls.latency)
		//cls.latency = latency;
		//else
		//cls.latency += 0.001f; // drift up, so correction are needed
	};
};

void CL_HandleStopSound()
{
	short i = MSG_ReadShort();
	//S_StopSound(i >> 3, i & 7);
};

/*
================
CL_UpdateUserPings

collect pings and packet lossage from clients
================
*/
void CL_ParsePings()
{
	int i = MSG_ReadByte();

	if(i >= MAX_CLIENTS)
		Host_EndGame("CL_ParseServerMessage: svc_updateping > MAX_SCOREBOARD");

	cl.players[i].ping = MSG_ReadShort();
};

void CL_ParseParticle(){};

/*
=====================
CL_ParseStaticEnt

Static entities are non-interactive world objects like torches
=====================
*/
void CL_ParseStaticEnt()
{
	cl_entity_t *ent;
	int i;
	entity_state_t es; // state

	Q_memset(&es, 0, sizeof(es));

	//CL_ParseBaseline(&es);

	i = cl.num_statics;

	//if(i >= MAX_STATIC_ENTITIES)
	//Host_EndGame("Too many static entities");

	/*
	if(i >= MAX_STATIC_ENTITIES)
	{
		MsgDev(D_ERROR, "CL_ParseStaticEntity: static entities limit exceeded!\n");
		return;
	};
	*/

	//ent = &cl_static_entities[i];
	cl.num_statics++;

	/*
	ent->index = 0; // ???
	ent->baseline = state;
	ent->curstate = state;
	ent->prevstate = state;

	// statics may be respawned in game e.g. for demo recording
	if( cls.state == ca_connected )
		ent->trivial_accept = INVALID_HANDLE;

	// setup the new static entity
	CL_UpdateEntityFields( ent );

	if( Mod_GetType( state.modelindex ) == mod_studio )
	{
		CL_UpdateStudioVars( ent, &state, true );

		// animate studio model
		ent->curstate.animtime = cl.time;
		ent->curstate.framerate = 1.0f;
		ent->latched.prevframe = 0.0f;
	}
	*/

	// copy it to the current state
	//ent->model = cl.model_precache[es.modelindex];
	//ent->frame = es.frame;
	//ent->colormap = vid.colormap;
	//ent->skinnum = es.skin;

	VectorCopy(es.origin, ent->origin);
	VectorCopy(es.angles, ent->angles);

	//R_AddEfrags(ent); // add link
};

/*
==================
CL_ParseBaseline
==================
*/
void CL_ParseBaseline(entity_state_t *es)
{
	es->modelindex = MSG_ReadByte(); // MSG_ReadShort();
	//state.sequence = MSG_ReadByte(); // ?
	es->frame = MSG_ReadByte();
	es->colormap = MSG_ReadByte(); // MSG_ReadWord();
	es->skin = MSG_ReadByte();

	for(int i = 0; i < 3; ++i)
	{
		es->origin[i] = MSG_ReadCoord();
		es->angles[i] = MSG_ReadAngle(); // MSG_ReadBitAngle(16);
	};

	es->rendermode = MSG_ReadByte();

	if(es->rendermode != kRenderNormal)
	{
		es->renderamt = MSG_ReadByte();

		es->rendercolor.r = MSG_ReadByte();
		es->rendercolor.g = MSG_ReadByte();
		es->rendercolor.b = MSG_ReadByte();

		es->renderfx = MSG_ReadByte();
	};
};

void CL_HandlePause(){
	//cl.refdef.paused = (MSG_ReadOneBit() != 0);
	//Cvar_SetValue ("paused", !cl_paused->value);

	//cl.paused = MSG_ReadByte();
	//if(cl.paused)
	//CDAudio_Pause();
	//else
	//CDAudio_Resume();
};

void CL_ParseSignOnNum()
{
	// Called just after client_putinserver
	// Signals the client that the server has marked it as "active"

	int nSignOn = MSG_ReadByte();
};

void CL_ParseCenterPrint(){
	//SCR_CenterPrint(MSG_ReadString());
};

void CL_ParseKilledMonster(){
	// Deprecated

	//cl.stats[STAT_MONSTERS]++;
};

void CL_ParseFoundSecret(){
	// Deprecated

	//cl.stats[STAT_SECRETS]++;
};

/*
===================
CL_ParseStaticSound
===================
*/
void CL_ParseStaticSound()
{
	vec3_t org;

	for(int i = 0; i < 3; ++i)
		org[i] = MSG_ReadCoord();

	int sound_num = MSG_ReadByte();
	int vol = MSG_ReadByte();
	int atten = MSG_ReadByte();

	//S_StaticSound(cl.sound_precache[sound_num], org, vol, atten);
};

void CL_ParseIntermission()
{
	cl.intermission = 1;
	//cl.completed_time = realtime;
	//vid.recalc_refdef = true; // go to full screen

	for(int i = 0; i < 3; ++i)
		cl.simorg[i] = MSG_ReadCoord();

	for(int i = 0; i < 3; ++i)
		cl.simangles[i] = MSG_ReadAngle();

	VectorCopy(vec3_origin, cl.simvel);
};

void CL_ParseFinale()
{
	cl.intermission = 2;
	//cl.completed_time = realtime;
	//vid.recalc_refdef = true; // go to full screen
	//SCR_CenterPrint(MSG_ReadString());
};

void CL_ParseCDTrack()
{
	cl.cdtrack = MSG_ReadByte();
	cl.looptrack = MSG_ReadByte();

	CDAudio_Play((byte)cl.cdtrack, true);
};

void CL_ParseRestore(){

};

/*
  Shows the intermission camera view, and writes-out text passed in first
  parameter.

  Note: Intermission mode 3
  Note: This text will keep showing on clients in future intermissions
*/
void CL_ParseCutscene()
{
	char *sText = MSG_ReadString();
};

/*
  Sended only if local client weapon simulation (prediction) is disabled
*/
void CL_ParseWeaponAnim()
{
	int nSequence = MSG_ReadByte();  // sequence number
	int nBodygroup = MSG_ReadByte(); // weapon model bodygroup (i.e. for different hands models)
	                                 //CL_WeaponAnim(param1, param2);
};

/*
  Allows to set, into the client's decals array and at specific position index
  (0->511), a decal name.
  E.g: let's say you send a message to set a decal "{break" at index 200.
  As result, when a message TE_ will be used to show a decal at index 200, we
  will see "{break".

  Note: If there is already an existing decal at the provided index, it will be
  overwritten.
  Note: It appears we can play only with decals from decals.wad.
*/
void CL_ParseDecalName()
{
	int nPosIndex = MSG_ReadByte();

	char *sDecalName = MSG_ReadString();
};

void CL_ParseRoomType()
{
	short nRoomType = MSG_ReadShort();
	//Cvar_SetFloat("room_type", nRoomType);
};

/*
================
CL_ParseAddAngle

add the view angle yaw
================
*/
void CL_ParseAddAngle()
{
	/*
  Note: When pev->fixangle is set to 2, this message is called with
  pev->avelocity[1] as a value.
  Note: The value needs to be scaled by (65536 / 360).
*/
	//float add_angle = MSG_ReadBitAngle(16 );
	//cl.refdef.cl_viewangles[1] += add_angle;

	short nAngle = MSG_ReadShort();
};

/*
================
CL_RegisterUserMessage

register new user message or update existing
================
*/
void CL_ParseNewUserMsg()
{
	/*
  Note: Sent every time a new message is registered on the server, but most
  games do this only once on the map change or server startup.
  Note: Name can be represented as an array of 4 "longs".
*/

	int nIndex = MSG_ReadByte(); // svc_num
	int nSize = MSG_ReadByte();

	char *sName = MSG_ReadString(); // 16 bits or bytes? (4 longs == 16 bytes on x86)
	                                // MSG_ReadLong(*(int *)&pMsg->szName[0]);
	                                // MSG_ReadLong(*(int *)&pMsg->szName[4]);
	                                // MSG_ReadLong(*(int *)&pMsg->szName[8]);
	                                // MSG_ReadLong(*(int *)&pMsg->szName[12]);

	// Important stuff
	//if(nSize == 0xFF)
	//nSize = -1;

	//nIndex = bound(0, nIndex, 255);

	//CL_LinkUserMessage(sName, nIndex, nSize);
};

void CL_ParsePacketEntities(bool bDelta)
{
	if(bDelta)
	{
	};
};

void CL_HandleChoke(){
	// Notify the client that some outgoing datagrams were not transmitted due
	// to exceeding bandwidth rate limits

	// i = MSG_ReadByte ();
	// for (j=0 ; j<i ; j++)
	//	cl.frames[ (cls.netchan.incoming_acknowledged-1-j)&UPDATE_MASK
	//].receivedtime = -2;
};

void CL_ParseResourceList()
{
	//CL_ParseModellist();
	//CL_ParseSoundlist();

	int i = 0;

//Q_memset( &reslist, 0, sizeof( resourcelist_t ));

//reslist.rescount = MSG_ReadWord() - 1;

//for( i = 0; i < reslist.rescount; i++ )
//{
//reslist.restype[i] = MSG_ReadWord();
//Q_strncpy( reslist.resnames[i], MSG_ReadString(), CS_SIZE );
//};

//cls.downloadcount = 0;

//HTTP_ResetProcessState();

//for( i = 0; i < reslist.rescount; i++ )
//{
// skip some types
#if 0
		if( reslist.restype[i] == t_model && !Q_strchr( download_types->latched_string, 'm' ) )
			continue;
		if( reslist.restype[i] == t_sound && !Q_strchr( download_types->latched_string, 's' ) )
			continue;
		if( reslist.restype[i] == t_eventscript && !Q_strchr( download_types->latched_string, 'e' ) )
			continue;
		if( reslist.restype[i] == t_generic && !Q_strchr( download_types->latched_string, 'c' ) )
			continue;
#endif
	//if( reslist.restype[i] == t_sound )
	//CL_CheckingSoundResFile( reslist.resnames[i] );
	//else
	//CL_CheckingResFile( reslist.resnames[i] );
	//};

	//if( !cls.downloadcount )
	//{
	//MSG_WriteByte( &cls.netchan.message, clc_stringcmd );
	//MSG_WriteString( &cls.netchan.message, "continueloading" );
	//};
};

void CL_ParseNewMoveVars(){
	//Delta_InitClient(); // finalize client delta's

	//MSG_ReadDeltaMovevars( msg, &clgame.oldmovevars, &clgame.movevars );

	// update sky if changed
	//if( Q_strcmp( clgame.oldmovevars.skyName, clgame.movevars.skyName ) && cl.video_prepped )
	//R_SetupSky( clgame.movevars.skyName );

	//Q_memcpy( &clgame.oldmovevars, &clgame.movevars, sizeof( movevars_t ));

	// keep features an actual!
	//clgame.oldmovevars.features = clgame.movevars.features = host.features;
};

void CL_ParseResourceRequest(){

};

/*
==================
CL_ParseCustomization
==================
*/
void CL_ParseCustomization(){
	// TODO
};

/*
================
CL_ParseCrosshairAngle

offset crosshair angles
================
*/
void CL_ParseCrosshairAngle(){
	//cl.refdef.crosshairangle[0] = MSG_ReadChar() * 0.2f;
	//cl.refdef.crosshairangle[1] = MSG_ReadChar() * 0.2f;
	//cl.refdef.crosshairangle[2] = 0.0f; // not used for screen space
};

void CL_ParseSoundFade()
{
	float fadePercent = (float)MSG_ReadByte();
	float holdTime = (float)MSG_ReadByte();

	float fadeOutSeconds = (float)MSG_ReadByte();
	float fadeInSeconds = (float)MSG_ReadByte();

	//S_FadeClientVolume( fadePercent, fadeOutSeconds, holdTime, fadeInSeconds );
};

void CL_HandleFileTransferFailed(){};

void CL_ParseHLTVMode()
{
	int nMode = MSG_ReadByte();
};

/*
==============
CL_ParseDirectorMsg

spectator message (hltv)
==============
*/
void CL_ParseDirectorMsg()
{
	byte pbuf[256];
	int iSize = MSG_ReadByte();

	// parse user message into buffer
	//MSG_ReadBytes(msg, pbuf, iSize);
	//gpClientDLL->pfnDirectorMessage(iSize, pbuf);
};

void CL_HandleVoiceInit(){

};

void CL_ParseVoiceData(){

};

void CL_ParseExtraInfo(){

};

void CL_ParseTimeScale()
{
	float fTimeScale = MSG_ReadFloat();
};

void CL_ParseResourceLocation()
{
	// sv_downloadurl
	char *sURL = MSG_ReadString();
};

/*
==============
CL_ParseCvarValue

Find the client cvar value
and sent it back to the server
==============
*/
void CL_ParseCvarValue()
{
	const char *cvarName = MSG_ReadString();
	cvar_t *cvar = Cvar_FindVar(cvarName);

	// build the answer
	//MSG_WriteByte(&cls.netchan.message, clc_requestcvarvalue);
	MSG_WriteString(&cls.netchan.message, cvar ? cvar->string : "Not Found");
};

/*
==============
CL_ParseCvarValue2

Find the client cvar value
and sent it back to the server
==============
*/
void CL_ParseCvarValue2()
{
	int requestID = MSG_ReadLong();
	const char *cvarName = MSG_ReadString();
	cvar_t *cvar = Cvar_FindVar(cvarName);

	// build the answer
	//MSG_WriteByte(&cls.netchan.message, clc_requestcvarvalue2);
	MSG_WriteLong(&cls.netchan.message, requestID);
	MSG_WriteString(&cls.netchan.message, cvarName);
	MSG_WriteString(&cls.netchan.message, cvar ? cvar->string : "Not Found");
};

/*
==================
CL_ParseStartSoundPacket
==================
*/
/*
void CL_ParseStartSoundPacket()
{
    vec3_t  pos;
    int 	channel, ent;
    int 	sound_num;
    int 	volume;
    int 	field_mask;
    float 	attenuation;  
 	int		i;
	           
    field_mask = MSG_ReadByte(); 

    if (field_mask & SND_VOLUME)
		volume = MSG_ReadByte ();
	else
		volume = DEFAULT_SOUND_PACKET_VOLUME;
	
    if (field_mask & SND_ATTENUATION)
		attenuation = MSG_ReadByte () / 64.0;
	else
		attenuation = DEFAULT_SOUND_PACKET_ATTENUATION;
	
	channel = MSG_ReadShort ();
	sound_num = MSG_ReadByte ();

	ent = channel >> 3;
	channel &= 7;

	if (ent > MAX_EDICTS)
		Host_Error ("CL_ParseStartSoundPacket: ent = %i", ent);
	
	for (i=0 ; i<3 ; i++)
		pos[i] = MSG_ReadCoord ();
 
    S_StartSound (ent, channel, cl.sound_precache[sound_num], pos, volume/255.0, attenuation);
}
*/

/*
==================
CL_KeepaliveMessage

When the client is taking a long time to load stuff, send keepalive messages
so the server doesn't disconnect.
==================
*/
/*
void CL_KeepaliveMessage ()
{
	float	time;
	static float lastmsg;
	int		ret;
	sizebuf_t	old;
	byte		olddata[8192];
	
	if (sv.active)
		return;		// no need if server is local
	if (cls.demoplayback)
		return;

// read messages from server, should just be nops
	old = net_message;
	memcpy (olddata, net_message.data, net_message.cursize);
	
	do
	{
		ret = CL_GetMessage ();
		switch (ret)
		{
		default:
			Host_Error ("CL_KeepaliveMessage: CL_GetMessage failed");		
		case 0:
			break;	// nothing waiting
		case 1:
			Host_Error ("CL_KeepaliveMessage: received a message");
			break;
		case 2:
			if (MSG_ReadByte() != svc_nop)
				Host_Error ("CL_KeepaliveMessage: datagram wasn't a nop");
			break;
		}
	} while (ret);

	net_message = old;
	memcpy (net_message.data, olddata, net_message.cursize);

// check time
	time = Sys_FloatTime ();
	if (time - lastmsg < 5)
		return;
	lastmsg = time;

// write out a nop
	Con_Printf ("--> client to server keepalive\n");

	MSG_WriteByte (&cls.netchan.message, clc_nop);
	NET_SendMessage (cls.netcon, &cls.message);
	SZ_Clear (&cls.netchan.message);
}
*/

/*
==================
CL_ParseServerInfo
==================
*/
/*
void CL_ParseServerInfo ()
{
	char	*str;
	int		i;
	int		nummodels, numsounds;
	char	model_precache[MAX_MODELS][MAX_QPATH];
	char	sound_precache[MAX_SOUNDS][MAX_QPATH];
	
	Con_DPrintf ("Serverinfo packet received.\n");
//
// wipe the client_state_t struct
//
	CL_ClearState ();

// parse protocol version number
	i = MSG_ReadLong ();
	
	if (i != PROTOCOL_VERSION)
	{
		Con_Printf ("Server returned version %i, not %i", i, PROTOCOL_VERSION);
		return;
	}

// parse maxclients
	cl.maxclients = MSG_ReadByte ();
	if (cl.maxclients < 1 || cl.maxclients > MAX_SCOREBOARD)
	{
		Con_Printf("Bad maxclients (%u) from server\n", cl.maxclients);
		return;
	}
	cl.scores = Hunk_AllocName (cl.maxclients*sizeof(*cl.scores), "scores");

// parse gametype
	cl.gametype = MSG_ReadByte ();

// parse signon message
	str = MSG_ReadString ();
	strncpy (cl.levelname, str, sizeof(cl.levelname)-1);

// seperate the printfs so the server message can have a color
	Con_Printf("\n\n\35\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\37\n\n");
	Con_Printf ("%c%s\n", 2, str);

//
// first we go through and touch all of the precache data that still
// happens to be in the cache, so precaching something else doesn't
// needlessly purge it
//

// precache models
	memset (cl.model_precache, 0, sizeof(cl.model_precache));
	for (nummodels=1 ; ; nummodels++)
	{
		str = MSG_ReadString ();
		if (!str[0])
			break;
		if (nummodels==MAX_MODELS)
		{
			Con_Printf ("Server sent too many model precaches\n");
			return;
		}
		strcpy (model_precache[nummodels], str);
		Mod_TouchModel (str);
	}

// precache sounds
	memset (cl.sound_precache, 0, sizeof(cl.sound_precache));
	for (numsounds=1 ; ; numsounds++)
	{
		str = MSG_ReadString ();
		if (!str[0])
			break;
		if (numsounds==MAX_SOUNDS)
		{
			Con_Printf ("Server sent too many sound precaches\n");
			return;
		}
		strcpy (sound_precache[numsounds], str);
		S_TouchSound (str);
	}

//
// now we try to load everything else until a cache allocation fails
//

	for (i=1 ; i<nummodels ; i++)
	{
		cl.model_precache[i] = Mod_ForName (model_precache[i], false);
		if (cl.model_precache[i] == NULL)
		{
			Con_Printf("Model %s not found\n", model_precache[i]);
			return;
		}
		CL_KeepaliveMessage ();
	}

	S_BeginPrecaching ();
	for (i=1 ; i<numsounds ; i++)
	{
		cl.sound_precache[i] = S_PrecacheSound (sound_precache[i]);
		CL_KeepaliveMessage ();
	}
	S_EndPrecaching ();


// local state
	cl_entities[0].model = cl.worldmodel = cl.model_precache[1];
	
	R_NewMap ();

	Hunk_Check ();		// make sure nothing is hurt
	
	noclip_anglehack = false;		// noclip is turned off at start	
}
*/

/*
==================
CL_ParseUpdate

Parse an entity update message from the server
If an entities model or origin changes from frame to frame, it must be
relinked.  Other attributes can change without relinking.
==================
*/
/*
int	bitcounts[16];

void CL_ParseUpdate (int bits)
{
	int			i;
	model_t		*model;
	int			modnum;
	qboolean	forcelink;
	cl_entity_t	*ent;
	int			num;
	int			skin;

	if (cls.signon == SIGNONS - 1)
	{	// first update is the final signon stage
		cls.signon = SIGNONS;
		CL_SignonReply ();
	}

	if (bits & U_MOREBITS)
	{
		i = MSG_ReadByte ();
		bits |= (i<<8);
	}

	if (bits & U_LONGENTITY)	
		num = MSG_ReadShort ();
	else
		num = MSG_ReadByte ();

	ent = CL_EntityNum (num);

for (i=0 ; i<16 ; i++)
if (bits&(1<<i))
	bitcounts[i]++;

	if (ent->msgtime != cl.mtime[1])
		forcelink = true;	// no previous frame to lerp from
	else
		forcelink = false;

	ent->msgtime = cl.mtime[0];
	
	if (bits & U_MODEL)
	{
		modnum = MSG_ReadByte ();
		if (modnum >= MAX_MODELS)
			Host_Error ("CL_ParseModel: bad modnum");
	}
	else
		modnum = ent->baseline.modelindex;
		
	model = cl.model_precache[modnum];
	if (model != ent->model)
	{
		ent->model = model;
	// automatic animation (torches, etc) can be either all together
	// or randomized
		if (model)
		{
			if (model->synctype == ST_RAND)
				ent->syncbase = (float)(rand()&0x7fff) / 0x7fff;
			else
				ent->syncbase = 0.0;
		}
		else
			forcelink = true;	// hack to make null model players work
#ifdef GLQUAKE
		if (num > 0 && num <= cl.maxclients)
			R_TranslatePlayerSkin (num - 1);
#endif
	}
	
	if (bits & U_FRAME)
		ent->frame = MSG_ReadByte ();
	else
		ent->frame = ent->baseline.frame;

	if (bits & U_COLORMAP)
		i = MSG_ReadByte();
	else
		i = ent->baseline.colormap;
	if (!i)
		ent->colormap = vid.colormap;
	else
	{
		if (i > cl.maxclients)
			Sys_Error ("i >= cl.maxclients");
		ent->colormap = cl.scores[i-1].translations;
	}

#ifdef GLQUAKE
	if (bits & U_SKIN)
		skin = MSG_ReadByte();
	else
		skin = ent->baseline.skin;
	if (skin != ent->skinnum) {
		ent->skinnum = skin;
		if (num > 0 && num <= cl.maxclients)
			R_TranslatePlayerSkin (num - 1);
	}

#else

	if (bits & U_SKIN)
		ent->skinnum = MSG_ReadByte();
	else
		ent->skinnum = ent->baseline.skin;
#endif

	if (bits & U_EFFECTS)
		ent->effects = MSG_ReadByte();
	else
		ent->effects = ent->baseline.effects;

// shift the known values for interpolation
	VectorCopy (ent->msg_origins[0], ent->msg_origins[1]);
	VectorCopy (ent->msg_angles[0], ent->msg_angles[1]);

	if (bits & U_ORIGIN1)
		ent->msg_origins[0][0] = MSG_ReadCoord ();
	else
		ent->msg_origins[0][0] = ent->baseline.origin[0];
	if (bits & U_ANGLE1)
		ent->msg_angles[0][0] = MSG_ReadAngle();
	else
		ent->msg_angles[0][0] = ent->baseline.angles[0];

	if (bits & U_ORIGIN2)
		ent->msg_origins[0][1] = MSG_ReadCoord ();
	else
		ent->msg_origins[0][1] = ent->baseline.origin[1];
	if (bits & U_ANGLE2)
		ent->msg_angles[0][1] = MSG_ReadAngle();
	else
		ent->msg_angles[0][1] = ent->baseline.angles[1];

	if (bits & U_ORIGIN3)
		ent->msg_origins[0][2] = MSG_ReadCoord ();
	else
		ent->msg_origins[0][2] = ent->baseline.origin[2];
	if (bits & U_ANGLE3)
		ent->msg_angles[0][2] = MSG_ReadAngle();
	else
		ent->msg_angles[0][2] = ent->baseline.angles[2];

	if ( bits & U_NOLERP )
		ent->forcelink = true;

	if ( forcelink )
	{	// didn't have an update last message
		VectorCopy (ent->msg_origins[0], ent->msg_origins[1]);
		VectorCopy (ent->msg_origins[0], ent->origin);
		VectorCopy (ent->msg_angles[0], ent->msg_angles[1]);
		VectorCopy (ent->msg_angles[0], ent->angles);
		ent->forcelink = true;
	}
}
*/

/*
==================
CL_ParseBaseline
==================
*/
/*
void CL_ParseBaseline (cl_entity_t *ent)
{
	int			i;
	
	ent->baseline.modelindex = MSG_ReadByte ();
	ent->baseline.frame = MSG_ReadByte ();
	ent->baseline.colormap = MSG_ReadByte();
	ent->baseline.skin = MSG_ReadByte();
	for (i=0 ; i<3 ; i++)
	{
		ent->baseline.origin[i] = MSG_ReadCoord ();
		ent->baseline.angles[i] = MSG_ReadAngle ();
	}
}
*/

/*
==================
CL_ParseClientdata

Server information pertaining to this client only
==================
*/
/*
void CL_ParseClientdata (int bits)
{
	int		i, j;
	
	if (bits & SU_VIEWHEIGHT)
		cl.viewheight = MSG_ReadChar ();
	else
		cl.viewheight = DEFAULT_VIEWHEIGHT;

	if (bits & SU_IDEALPITCH)
		cl.idealpitch = MSG_ReadChar ();
	else
		cl.idealpitch = 0;
	
	VectorCopy (cl.mvelocity[0], cl.mvelocity[1]);
	for (i=0 ; i<3 ; i++)
	{
		if (bits & (SU_PUNCH1<<i) )
			cl.punchangle[i] = MSG_ReadChar();
		else
			cl.punchangle[i] = 0;
		if (bits & (SU_VELOCITY1<<i) )
			cl.mvelocity[0][i] = MSG_ReadChar()*16;
		else
			cl.mvelocity[0][i] = 0;
	}

// [always sent]	if (bits & SU_ITEMS)
		i = MSG_ReadLong ();

	if (cl.items != i)
	{	// set flash times
		for (j=0 ; j<32 ; j++)
			if ( (i & (1<<j)) && !(cl.items & (1<<j)))
				cl.item_gettime[j] = cl.time;
		cl.items = i;
	}
		
	cl.onground = (bits & SU_ONGROUND) != 0;
	cl.inwater = (bits & SU_INWATER) != 0;

	if (bits & SU_WEAPONFRAME)
		cl.stats[STAT_WEAPONFRAME] = MSG_ReadByte ();
	else
		cl.stats[STAT_WEAPONFRAME] = 0;

	if (bits & SU_ARMOR)
		i = MSG_ReadByte ();
	else
		i = 0;
	if (cl.stats[STAT_ARMOR] != i)
	{
		cl.stats[STAT_ARMOR] = i;
	}

	if (bits & SU_WEAPON)
		i = MSG_ReadByte ();
	else
		i = 0;
	if (cl.stats[STAT_WEAPON] != i)
	{
		cl.stats[STAT_WEAPON] = i;
	}
	
	i = MSG_ReadShort ();
	if (cl.stats[STAT_HEALTH] != i)
	{
		cl.stats[STAT_HEALTH] = i;
	}

	i = MSG_ReadByte ();
	if (cl.stats[STAT_AMMO] != i)
	{
		cl.stats[STAT_AMMO] = i;
	}

	for (i=0 ; i<4 ; i++)
	{
		j = MSG_ReadByte ();
		if (cl.stats[STAT_SHELLS+i] != j)
		{
			cl.stats[STAT_SHELLS+i] = j;
		}
	}

	i = MSG_ReadByte ();

	if (cl.stats[STAT_ACTIVEWEAPON] != i)
	{
		cl.stats[STAT_ACTIVEWEAPON] = i;
	}
}
*/

/*
=====================
CL_NewTranslation
=====================
*/
/*
void CL_NewTranslation (int slot)
{
	int		i, j;
	int		top, bottom;
	byte	*dest, *source;
	
	if (slot > cl.maxclients)
		Sys_Error ("CL_NewTranslation: slot > cl.maxclients");
	dest = cl.scores[slot].translations;
	source = vid.colormap;
	memcpy (dest, vid.colormap, sizeof(cl.scores[slot].translations));
	top = cl.scores[slot].colors & 0xf0;
	bottom = (cl.scores[slot].colors &15)<<4;
#ifdef GLQUAKE
	R_TranslatePlayerSkin (slot);
#endif

	for (i=0 ; i<VID_GRADES ; i++, dest += 256, source+=256)
	{
		if (top < 128)	// the artists made some backwards ranges.  sigh.
			memcpy (dest + TOP_RANGE, source + top, 16);
		else
			for (j=0 ; j<16 ; j++)
				dest[TOP_RANGE+j] = source[top+15-j];
				
		if (bottom < 128)
			memcpy (dest + BOTTOM_RANGE, source + bottom, 16);
		else
			for (j=0 ; j<16 ; j++)
				dest[BOTTOM_RANGE+j] = source[bottom+15-j];		
	}
}
*/

/*
=====================
CL_ParseStatic
=====================
*/
/*
void CL_ParseStatic ()
{
	cl_entity_t *ent;
	int		i;
		
	i = cl.num_statics;
	if (i >= MAX_STATIC_ENTITIES)
		Host_Error ("Too many static entities");
	ent = &cl_static_entities[i];
	cl.num_statics++;
	CL_ParseBaseline (ent);

// copy it to the current state
	ent->model = cl.model_precache[ent->baseline.modelindex];
	ent->frame = ent->baseline.frame;
	ent->colormap = vid.colormap;
	ent->skinnum = ent->baseline.skin;
	ent->effects = ent->baseline.effects;

	VectorCopy (ent->baseline.origin, ent->origin);
	VectorCopy (ent->baseline.angles, ent->angles);	
	R_AddEfrags (ent);
}
*/

/*
===================
CL_ParseStaticSound
===================
*/
/*
void CL_ParseStaticSound ()
{
	vec3_t		org;
	int			sound_num, vol, atten;
	int			i;
	
	for (i=0 ; i<3 ; i++)
		org[i] = MSG_ReadCoord ();
	sound_num = MSG_ReadByte ();
	vol = MSG_ReadByte ();
	atten = MSG_ReadByte ();
	
	S_StaticSound (cl.sound_precache[sound_num], org, vol, atten);
}
*/

void SHOWNET(const char *x)
{
	if(cl_shownet.value == 2)
		Con_Printf("%3i:%s\n", msg_readcount - 1, x);
};

/*
=====================
CL_ParseServerMessage
=====================
*/
void CL_ParseServerMessage()
{
	int cmd;
	int i;

	//
	// if recording demos, copy the message out
	//
	if(cl_shownet.value == 1)
		Con_Printf("%i ", net_message.cursize);
	else if(cl_shownet.value == 2)
		Con_Printf("------------------\n");

	cl.onground = false; // unless the server says otherwise

	//
	// parse the message
	//
	MSG_BeginReading();

	while(1)
	{
		if(msg_badread)
			Host_Error("CL_ParseServerMessage: Bad server message");

		cmd = MSG_ReadByte();

		if(cmd == -1)
		{
			SHOWNET("END OF MESSAGE");
			return; // end of message
		}

		// if the high bit of the command byte is set, it is a fast update
		if(cmd & 128)
		{
			SHOWNET("fast update");
			CL_ParseUpdate(cmd & 127);
			continue;
		}

		SHOWNET(svc_strings[cmd]);

		// other commands
		switch(cmd)
		{
		// GS protocol 48 entries
		default:
			Host_EndGame("CL_ParseServerMessage: Illegible server message");
			break;
		case svc_bad:
			// Host_Error("svc_bad\n");
			break;
		case svc_nop:
			// this does nothing
			// Con_Printf("svc_nop\n");
			break;
		case svc_disconnect:
			CL_HandleDisconnect();
			break;
		case svc_event:
			CL_ParseEventData(false);
			break;
		case svc_version:
			CL_ParseVersion();
			break;
		case svc_setview:
			CL_ParseView();
			break;
		case svc_sound:
			CL_ParseStartSoundPacket();
			break;
		case svc_time:
			CL_ParseTime();
			break;
		case svc_print:
			CL_ParsePrint();
			break;
		case svc_stufftext:
			CL_ParseStuffText();
			break;
		case svc_setangle:
			CL_ParseSetAngle();
			break;
		case svc_serverinfo:
			CL_ParseServerInfo();

			// from svc_serverdata
			// Cbuf_Execute ();		// make sure any stuffed commands are
			// done
			// CL_ParseServerData ();
			// vid.recalc_refdef = true;	// leave full screen intermission
			break;
		case svc_lightstyle:
			CL_ParseLightStyle();
			break;
		case svc_updateuserinfo:
			CL_ParseUserinfo();
			break;
		case svc_deltadescription:
			CL_ParseDeltaDescription();
			break;
		case svc_clientdata:
			//CL_ParseClientData();
			break;
		case svc_stopsound:
			CL_HandleStopSound();
			break;
		case svc_pings:
			CL_ParsePings();
			break;
		case svc_particle:
			CL_ParseParticle();
			break;
		case svc_damage:
			//V_ParseDamage(); // unused
			break;
		case svc_spawnstatic:
			CL_ParseStaticEnt();
			break;
		case svc_event_reliable:
			CL_ParseEventData(true);
			break;
		case svc_spawnbaseline:
			i = MSG_ReadShort();
			//CL_ParseBaseline(&cl_baselines[i]);
			break;
		case svc_temp_entity:
			//CL_ParseTempEntity();
			break;
		case svc_setpause:
			CL_HandlePause();
			break;
		case svc_signonnum:
			CL_ParseSignOnNum();
			break;
		case svc_centerprint:
			CL_ParseCenterPrint();
			break;
		case svc_killedmonster:
			CL_ParseKilledMonster(); // unused
			break;
		case svc_foundsecret:
			CL_ParseFoundSecret(); // unused
			break;
		case svc_spawnstaticsound:
			CL_ParseStaticSound();
			break;
		case svc_intermission:
			CL_ParseIntermission();
			break;
		case svc_finale:
			CL_ParseFinale();
			break;
		case svc_cdtrack:
			CL_ParseCDTrack();
			break;
		case svc_restore:
			CL_ParseRestore();
			break;
		case svc_cutscene:
			CL_ParseCutscene();
			break;
		case svc_weaponanim:
			CL_ParseWeaponAnim();
			break;
		case svc_decalname:
			CL_ParseDecalName();
			break;
		case svc_roomtype:
			CL_ParseRoomType();
			break;
		case svc_addangle:
			CL_ParseAddAngle();
			break;
		case svc_newusermsg:
			CL_ParseNewUserMsg();
			break;
		case svc_packetentities:
			CL_ParsePacketEntities(false);
			break;
		case svc_deltapacketentities:
			CL_ParsePacketEntities(true);
			break;
		case svc_choke: // some preceding packets were choked
			CL_HandleChoke();
			break;
		case svc_resourcelist:
			CL_ParseResourceList();
			break;
		case svc_newmovevars:
			CL_ParseNewMoveVars();
			break;
		case svc_resourcerequest:
			CL_ParseResourceRequest();
			break;
		case svc_customization:
			CL_ParseCustomization();
			break;
		case svc_crosshairangle:
			CL_ParseCrosshairAngle();
			break;
		case svc_soundfade:
			CL_ParseSoundFade();
			break;
		case svc_filetxferfailed:
			CL_HandleFileTransferFailed();
			break;
		case svc_hltv:
			CL_ParseHLTVMode();
			break;
		case svc_director:
			CL_ParseDirectorMsg();
			break;
		case svc_voiceinit:
			CL_HandleVoiceInit();
			break;
		case svc_voicedata:
			CL_ParseVoiceData();
			break;
		case svc_sendextrainfo:
			CL_ParseExtraInfo();
			break;
		case svc_timescale:
			CL_ParseTimeScale();
			break;
		case svc_resourcelocation:
			CL_ParseResourceLocation();
			break;
		case svc_sendcvarvalue:
			CL_ParseCvarValue();
			break;
		case svc_sendcvarvalue2:
			CL_ParseCvarValue2();
			break;

			/*
		default:
			Host_Error ("CL_ParseServerMessage: Illegible server message\n");
			break;
			
		case svc_nop:
//			Con_Printf ("svc_nop\n");
			break;
			
		case svc_time:
			cl.mtime[1] = cl.mtime[0];
			cl.mtime[0] = MSG_ReadFloat ();			
			break;
			
		case svc_clientdata:
			i = MSG_ReadShort ();
			CL_ParseClientdata (i);
			break;
		
		case svc_version:
			i = MSG_ReadLong ();
			if (i != PROTOCOL_VERSION)
				Host_Error ("CL_ParseServerMessage: Server is protocol %i instead of %i\n", i, PROTOCOL_VERSION);
			break;
			
		case svc_disconnect:
			Host_EndGame ("Server disconnected\n");

		case svc_print:
			Con_Printf ("%s", MSG_ReadString ());
			break;
			
		case svc_centerprint:
			SCR_CenterPrint (MSG_ReadString ());
			break;
			
		case svc_stufftext:
			Cbuf_AddText (MSG_ReadString ());
			break;
			
		case svc_damage:
			V_ParseDamage ();
			break;
			
		case svc_serverinfo:
			CL_ParseServerInfo ();
			vid.recalc_refdef = true;	// leave intermission full screen
			break;
			
		case svc_setangle:
			for (i=0 ; i<3 ; i++)
				cl.viewangles[i] = MSG_ReadAngle ();
			break;
			
		case svc_setview:
			cl.viewentity = MSG_ReadShort ();
			break;
					
		case svc_lightstyle:
			i = MSG_ReadByte ();
			if (i >= MAX_LIGHTSTYLES)
				Sys_Error ("svc_lightstyle > MAX_LIGHTSTYLES");
			Q_strcpy (cl_lightstyle[i].map,  MSG_ReadString());
			cl_lightstyle[i].length = Q_strlen(cl_lightstyle[i].map);
			break;
			
		case svc_sound:
			CL_ParseStartSoundPacket();
			break;
			
		case svc_stopsound:
			i = MSG_ReadShort();
			S_StopSound(i>>3, i&7);
			break;
		
		case svc_updatename:
			i = MSG_ReadByte ();
			if (i >= cl.maxclients)
				Host_Error ("CL_ParseServerMessage: svc_updatename > MAX_SCOREBOARD");
			strcpy (cl.scores[i].name, MSG_ReadString ());
			break;
			
		case svc_updatefrags:
			i = MSG_ReadByte ();
			if (i >= cl.maxclients)
				Host_Error ("CL_ParseServerMessage: svc_updatefrags > MAX_SCOREBOARD");
			cl.scores[i].frags = MSG_ReadShort ();
			break;			

		case svc_updatecolors:
			i = MSG_ReadByte ();
			if (i >= cl.maxclients)
				Host_Error ("CL_ParseServerMessage: svc_updatecolors > MAX_SCOREBOARD");
			cl.scores[i].colors = MSG_ReadByte ();
			CL_NewTranslation (i);
			break;
			
		case svc_particle:
			R_ParseParticleEffect ();
			break;

		case svc_spawnbaseline:
			i = MSG_ReadShort ();
			// must use CL_EntityNum() to force cl.num_entities up
			CL_ParseBaseline (CL_EntityNum(i));
			break;
		case svc_spawnstatic:
			CL_ParseStatic ();
			break;			
		case svc_temp_entity:
			CL_ParseTEnt ();
			break;

		case svc_setpause:
			{
				cl.paused = MSG_ReadByte ();

				if (cl.paused)
				{
					CDAudio_Pause ();
#ifdef _WIN32
					VID_HandlePause (true);
#endif
				}
				else
				{
					CDAudio_Resume ();
#ifdef _WIN32
					VID_HandlePause (false);
#endif
				}
			}
			break;
			
		case svc_signonnum:
			i = MSG_ReadByte ();
			if (i <= cls.signon)
				Host_Error ("Received signon %i when at %i", i, cls.signon);
			cls.signon = i;
			CL_SignonReply ();
			break;

		case svc_killedmonster:
			cl.stats[STAT_MONSTERS]++;
			break;

		case svc_foundsecret:
			cl.stats[STAT_SECRETS]++;
			break;

		case svc_updatestat:
			i = MSG_ReadByte ();
			if (i < 0 || i >= MAX_CL_STATS)
				Sys_Error ("svc_updatestat: %i is invalid", i);
			cl.stats[i] = MSG_ReadLong ();;
			break;
			
		case svc_spawnstaticsound:
			CL_ParseStaticSound ();
			break;

		case svc_cdtrack:
			cl.cdtrack = MSG_ReadByte ();
			cl.looptrack = MSG_ReadByte ();
			if ( (cls.demoplayback || cls.demorecording) && (cls.forcetrack != -1) )
				CDAudio_Play ((byte)cls.forcetrack, true);
			else
				CDAudio_Play ((byte)cl.cdtrack, true);
			break;

		case svc_intermission:
			cl.intermission = 1;
			cl.completed_time = cl.time;
			vid.recalc_refdef = true;	// go to full screen
			break;

		case svc_finale:
			cl.intermission = 2;
			cl.completed_time = cl.time;
			vid.recalc_refdef = true;	// go to full screen
			SCR_CenterPrint (MSG_ReadString ());			
			break;

		case svc_cutscene:
			cl.intermission = 3;
			cl.completed_time = cl.time;
			vid.recalc_refdef = true;	// go to full screen
			SCR_CenterPrint (MSG_ReadString ());			
			break;

		case svc_sellscreen:
			Cmd_ExecuteString ("help", src_command);
			break;
		*/
		}
	}
}