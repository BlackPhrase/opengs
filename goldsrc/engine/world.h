/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/// @file

#pragma once

#include "common/const.h"
//#include "maintypes.h"
//#include "model.h"

#define MOVE_NORMAL 0
#define MOVE_NOMONSTERS 1
#define MOVE_MISSILE 2

typedef struct edict_s edict_t;
typedef struct hull_s hull_t;

typedef struct moveclip_s // TODO: Move it to world.cpp someday
{
	vec3_t boxmins;
	vec3_t boxmaxs;
	const float *mins;
	const float *maxs;
	vec3_t mins2;
	vec3_t maxs2;
	const float *start;
	const float *end;
	trace_t trace;
	short int type;
	short int ignoretrans;
	edict_t *passedict;
	qboolean monsterClipBrush;
} moveclip_t;

typedef struct areanode_s
{
	int axis; // -1 = leaf node
	float dist;
	struct areanode_s *children[2];
	link_t trigger_edicts;
	link_t solid_edicts;
} areanode_t;

#define AREA_DEPTH 4
#define AREA_NODES 32

extern areanode_t sv_areanodes[AREA_NODES];
extern int sv_numareanodes;

typedef dclipnode_t box_clipnodes_t[6];
typedef mplane_t box_planes_t[6];
typedef mplane_t beam_planes_t[6];

extern hull_t box_hull;
extern hull_t beam_hull;
extern box_clipnodes_t box_clipnodes;
extern box_planes_t box_planes;
extern beam_planes_t beam_planes;

extern cvar_t sv_force_ent_intersection;

// from common.h
void ClearLink(link_t *l);
void RemoveLink(link_t *l);
void InsertLinkBefore(link_t *l, link_t *before);
/*NOXREF*/ void InsertLinkAfter(link_t *l, link_t *after);
//

void SV_InitBoxHull();

hull_t *SV_HullForBox(const vec_t *mins, const vec_t *maxs);
/*NOXREF*/ hull_t *SV_HullForBeam(const vec_t *start, const vec_t *end, const vec_t *size);
struct hull_s *SV_HullForBsp(edict_t *ent, const vec_t *mins, const vec_t *maxs, vec_t *offset);
hull_t *SV_HullForEntity(edict_t *ent, const vec_t *mins, const vec_t *maxs, vec_t *offset);

areanode_t *SV_CreateAreaNode(int depth, vec_t *mins, vec_t *maxs);

void SV_ClearWorld();
// called after the world model has been loaded, before linking any entities

void SV_UnlinkEdict(edict_t *ent);
// call before removing an entity, and before trying to move one,
// so it doesn't clip against itself
// flags ent->v.modified

void SV_LinkEdict(edict_t *ent, qboolean touch_triggers);
// Needs to be called any time an entity changes origin, mins, maxs, or solid
// flags ent->v.modified
// sets ent->v.absmin and ent->v.absmax
// if touchtriggers, calls prog functions for the intersected triggers

int SV_LinkContents(areanode_t *node, const vec_t *pos);

int SV_HullPointContents(hull_t *hull, int num, const vec_t *p);

int SV_PointContents(const vec3_t p); // const vec_t *p
// returns the CONTENTS_* value from the world at the given point.
// does not check any entities at all
// the non-true version remaps the water current contents to content_water

edict_t *SV_TestEntityPosition(edict_t *ent);

qboolean SV_RecursiveHullCheck(hull_t *hull, int num, float p1f, float p2f, const vec3_t p1, const vec3_t p2, trace_t *trace);

trace_t SV_Move(const vec3_t start, const vec3_t mins, const vec3_t maxs, const vec3_t end, int type, edict_t *passedict, qboolean monsterClipBrush);
// mins and maxs are reletive

trace_t SV_MoveNoEnts(const vec_t *start, vec_t *mins, vec_t *maxs, const vec_t *end, int type, edict_t *passedict);

#ifdef REHLDS_OPT_PEDANTIC
trace_t SV_Move_Point(const vec_t *start, const vec_t *end, int type, edict_t *passedict);
#endif // REHLDS_OPT_PEDANTIC

// if the entire move stays in a solid volume, trace.allsolid will be set

// if the starting point is in a solid, it will be allowed to move out
// to an open area

// nomonsters is used for line of sight or edge testing, where mosnters
// shouldn't be considered solid objects

// passedict is explicitly excluded from clipping checks (normally NULL)

void SV_SingleClipMoveToEntity(edict_t *ent, const vec_t *start, const vec_t *mins, const vec_t *maxs, const vec_t *end, trace_t *trace);
trace_t SV_ClipMoveToEntity(edict_t *ent, const vec_t *start, const vec_t *mins, const vec_t *maxs, const vec_t *end);

void SV_ClipToLinks(areanode_t *node, moveclip_t *clip);
void SV_ClipToWorldbrush(areanode_t *node, moveclip_t *clip);

void SV_MoveBounds(const vec_t *start, const vec_t *mins, const vec_t *maxs, const vec_t *end, vec_t *boxmins, vec_t *boxmaxs);

void SV_TouchLinks(edict_t *ent, areanode_t *node);
void SV_FindTouchedLeafs(edict_t *ent, mnode_t *node, int *topnode);