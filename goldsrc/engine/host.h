/*
*
*    This program is free software; you can redistribute it and/or modify it
*    under the terms of the GNU General Public License as published by the
*    Free Software Foundation; either version 2 of the License, or (at
*    your option) any later version.
*
*    This program is distributed in the hope that it will be useful, but
*    WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software Foundation,
*    Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*    In addition, as a special exception, the author gives permission to
*    link the code of this program with the Half-Life Game Engine ("HL
*    Engine") and Modified Game Libraries ("MODs") developed by Valve,
*    L.L.C ("Valve").  You must obey the GNU General Public License in all
*    respects for all of the code used other than the HL Engine and MODs
*    from Valve.  If you modify this file, you may extend this exception
*    to your version of the file, but you are not obligated to do so.  If
*    you do not wish to do so, delete this exception statement from your
*    version.
*
*/

/// @file

#pragma once

#include <csetjmp>
/*
#include "maintypes.h"
#include "filesystem_internal.h"
#include "sys_dll.h"
*/
#include "tier0/platform.h"
#include "server.h"
//#include "rehlds_api.h"

//=============================================================================

//const int MAX_COMMAND_LINE_PARAMS = 50;

/**
*	the host system specifies the base of the directory tree, the
*	command line parms passed to the program, and the amount of memory
*	available for the program to use
*/
typedef struct // quakeparms_s
{
	/*const*/ char *basedir;
	/*const*/ char *cachedir; ///< for development over ISDN lines
	int argc;
	/*const*/ char **argv;
	void *membase;
	int memsize;
} quakeparms_t;

extern quakeparms_t host_parms;

extern jmp_buf host_abortserver;
extern jmp_buf host_enddemo;

//extern qboolean noclip_anglehack;

extern cvar_t sys_ticrate;
extern cvar_t sys_timescale;
//extern cvar_t sys_nostdout; // TODO: not present

extern cvar_t developer;
//extern cvar_t password;
//extern cvar_t pausable;
//extern cvar_t suitvolume;

//extern cvar_t skill;
//extern cvar_t deathmatch;
//extern cvar_t coop;

//extern cvar_t fps_max;
//extern cvar_t fps_override;

//extern cvar_t sv_stats;

//extern cvar_t host_name;
//extern cvar_t host_speeds;
//extern cvar_t host_profile;
//extern cvar_t host_limitlocal;
//extern cvar_t host_killtime;
//extern cvar_t host_framerate;

//extern qboolean gfNoMasterServer;

extern qboolean host_initialized; ///< true if into command execution
extern double host_frametime;
extern unsigned short *host_basepal;
extern byte *host_colormap;
extern int host_framecount; ///< incremented every frame, never reset
//extern int host_hunklevel;
//extern double rolling_fps;
//extern double oldrealtime;
extern double realtime;     ///< not bounded in any way, changed at
                            /// start of every frame, never reset

extern client_t *host_client;

//

qboolean Host_Init(quakeparms_t *parms); // return int?
void Host_Shutdown();

//void Host_InitLocal();

//void Host_Version();

void /*NORETURN*/ Host_Error(const char *error, ...);

/*NOXREF*/ void Host_EndGame(const char *message, ...);

int Host_Frame(float time, int iState, int *stateInfo);

//void _Host_Frame(float time);

//void Host_UpdateScreen();
//void Host_UpdateSounds();

void Host_ServerFrame();
void Host_InitCommands();

qboolean Host_FilterTime(float time);

qboolean Host_IsServerActive();
qboolean Host_IsSinglePlayerGame();

//void Host_CheckConnectionFailure();

//void Host_ComputeFPS(double frametime);
void Host_GetHostInfo(float *fps, int *nActive, int *unused, int *nMaxPlayers, char *pszMap);
//void Host_Speeds(double *time);

void Host_CheckDyanmicStructures();

void Host_ClearMemory(qboolean bQuiet);
//void Host_ClearClients(qboolean bFramesOnly);

void Host_WriteConfiguration();
//void Host_WriteCustomConfig();

void Host_Quit_f();
void Host_ClientCommands(const char *fmt, ...);
void Host_ShutdownServer(qboolean crash);

/**
*	Loads the server dll if needed.
*/
void Host_InitializeGameDLL();

void Host_ClearSaveDirectory();

//void CheckGore();

//

void SV_DropClient(client_t *cl, qboolean crash, const char *fmt, ...);
//void SV_DropClient_api(IGameClient* cl, bool crash, const char* fmt, ...);
//void SV_DropClient_internal(client_t *cl, qboolean crash, const char *string);
void SV_ClearResourceLists(client_t *cl);
void SV_ClearClientStates();

//void SV_ClientPrintf(const char *fmt, ...);
//void SV_BroadcastPrintf(const char *fmt, ...);

//

//qboolean Master_IsLanGame();
//void Master_Heartbeat_f();

//=============================================================================

//extern qboolean		msg_suppress_1;		///< suppresses resolution and cache size console output
///  an fullscreen DIB focus gain/loss
//extern int			current_skill;		///< skill level for currently loaded level (in case
///  the user changes the cvar while the level is
///  running, this reflects the level actually in use)

//extern qboolean		isDedicated;

//extern int			minimum_memory;