/*
*
*    This program is free software; you can redistribute it and/or modify it
*    under the terms of the GNU General Public License as published by the
*    Free Software Foundation; either version 2 of the License, or (at
*    your option) any later version.
*
*    This program is distributed in the hope that it will be useful, but
*    WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software Foundation,
*    Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*    In addition, as a special exception, the author gives permission to
*    link the code of this program with the Half-Life Game Engine ("HL
*    Engine") and Modified Game Libraries ("MODs") developed by Valve,
*    L.L.C ("Valve").  You must obey the GNU General Public License in all
*    respects for all of the code used other than the HL Engine and MODs
*    from Valve.  If you modify this file, you may extend this exception
*    to your version of the file, but you are not obligated to do so.  If
*    you do not wish to do so, delete this exception statement from your
*    version.
*
*/

/// @file

//#include "precompiled.h"
#include "strtools_local.h"
#include "tier1/UtlVector.h"

#include "sys.h"
#include "traceinit.h"

class CInitTracker
{
public:
	enum
	{
		NUM_LISTS = 4
	};

	struct InitFunc
	{
		const char *initname;
		const char *shutdownname;

		int referencecount;
		int sequence;

		bool warningprinted;
		double inittime;
		double shutdowntime;
	};

public:
	CInitTracker() = default;
	~CInitTracker();

	void Init(const char *init, const char *shutdown, int listnum);

	void Shutdown(const char *shutdown, int listnum);
public:
	int m_nNumFuncs[NUM_LISTS] = {};

	CUtlVector<InitFunc *> m_Funcs[NUM_LISTS];
};

static CInitTracker g_InitTracker; // static?

/*
CInitTracker::CInitTracker()
{
	for (int l = 0; l < NUM_LISTS; l++)
	{
		m_Funcs[l].RemoveAll();
		m_nNumFuncs[l] = 0;
	}
}
*/

CInitTracker::~CInitTracker()
{
	for(int list = 0; list < NUM_LISTS; ++list)
	{
		if(m_nNumFuncs[list] > 0)
		{
			for(int i = 0; i < m_nNumFuncs[list]; ++i)
			{
				auto pFunc = m_Funcs[list][i];

				if(pFunc->referencecount)
					Sys_Printf("Missing shutdown function for %s : %s\n", pFunc->initname, pFunc->shutdownname);

				delete pFunc;
			}
		}

		m_Funcs[list].RemoveAll();
		m_nNumFuncs[list] = 0;
	}
}

void CInitTracker::Init(const char *init, const char *shutdown, int listnum)
{
	auto pFunc = new InitFunc;

	pFunc->initname = init;
	pFunc->shutdownname = shutdown;
	pFunc->inittime = 0.0f;
	pFunc->referencecount = 1;
	pFunc->shutdowntime = 0.0f;
	pFunc->sequence = m_nNumFuncs[listnum];
	pFunc->warningprinted = false;

	m_Funcs[listnum].InsertBefore(m_nNumFuncs[listnum], pFunc); // m_Funcs[listnum].AddToHead(pFunc);

	++m_nNumFuncs[listnum];
}

void CInitTracker::Shutdown(const char *shutdown, int listnum)
{
	const auto iFuncCount = m_nNumFuncs[listnum];

	if(!iFuncCount)
	{
		Sys_Printf("Mismatched shutdown function %s\n", shutdown);
		return;
	}

	//This is unlikely to be the same code that is in the engine, but it is functionally equivalent.

	if(iFuncCount > 0) // TODO: not present in GS?
	{
		InitFunc *pFunc = nullptr;

		for(int i = 0; i < iFuncCount; ++i)
		{
			pFunc = m_Funcs[listnum][i];

			if(pFunc->referencecount)
				break;
		}

		//Shutdown functions should always be called in reverse order of initialization.
		//If we get this, then a shutdown function was called out of order.
		if(pFunc && pFunc->referencecount && Q_stricmp(pFunc->shutdownname, shutdown))
		{
			if(!pFunc->warningprinted)
			{
				pFunc->warningprinted = true;

				//This is what it looks like when reverse engineered, but it's probably the for loop below that causes this.
				//The end of the loop jumps to inside this statement, so this is probably an early out optimization by the compiler. - Solokiller
				/*
				if( iFuncCount <= 0 )
				{
				Sys_Printf( "Shutdown function %s not in list!!!\n", shutdown );
				return;
				}
				*/
			}
		}
	}

	for(int i = 0; i < iFuncCount; ++i)
	{
		auto pFunc = m_Funcs[listnum][i];

		if(!Q_stricmp(pFunc->shutdownname, shutdown))
		{
			--pFunc->referencecount;
			return;
		}
	}

	Sys_Printf("Shutdown function %s not in list!!!\n", shutdown);
}

void TraceInit(const char *i, const char *s, int listnum)
{
	g_InitTracker.Init(i, s, listnum);
}

void TraceShutdown(const char *s, int listnum)
{
	g_InitTracker.Shutdown(s, listnum);
}