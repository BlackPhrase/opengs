#
#Hardware engine (OpenGL)
#

project(OGS-engine)

# Available options
option(OGS_ENGINE_STATIC "Build the engine as a static library" OFF)

# Detect the build type
if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release CACHE STRING
        "Choose the type of build, options are: None(CMAKE_CXX_FLAGS or CMAKE_C_FLAGS used) Debug Release RelWithDebInfo MinSizeRel."
        FORCE
	)
    set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS
		None
		Debug
		Release
		RelWithDebInfo
		MinSizeRel
	)
endif()

# List of sources
file(GLOB PROJECT_SOURCES
	#${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
	
	${CMAKE_CURRENT_SOURCE_DIR}/hdrfix.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/buildnum.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/cdll_exp.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/cdll_int.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/chase.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/cl_cam.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/cl_demo.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/cl_draw.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/cl_ents.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/cl_input.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/cl_main.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/cl_parse.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/cl_parsefn.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/cl_pred.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/cl_spectator.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/cl_tent.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/cmd.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/cmodel.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/com_custom.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/common.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/console.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/crc.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/cvar.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/decals.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/delta.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/DemoPlayerWrapper.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/DetailTexture.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/eventapi.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/filesystem.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/filesystem_internal.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/hashpak.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/host.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/host_cmd.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/info.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/input.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/ipratelimit.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/ipratelimitWrapper.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/keys.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/LoadBlob.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/mathlib.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/md5.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/mem.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/model.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/module.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/net_api.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/net_chan.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/pmove.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/pmovetst.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/pr_cmds.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/pr_edict.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/pr_exec.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/r_efx.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/r_part.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/r_studio.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/r_triangle.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/screen.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/Sequence.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/snd_dma.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/snd_mem.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/snd_mix.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/sv_log.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/sv_main.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/sv_move.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/sv_phys.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/sv_pmove.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/sv_remoteaccess.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/sv_secure.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/sv_steam3.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/sv_upld.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/sv_user.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/sys_dll.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/sys_dll2.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/sys_engine.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/sys_getmodes.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/SystemWrapper.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/textures.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/tmessage.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/traceinit.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/vgui_EngineSurface.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/vgui_EngineSurfaceHW.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/vgui_EngineSurfaceWin32.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/vgui_EngineSurfaceWrap.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/vgui_int.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/vgui_intwrap.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/vgui_intwrap2.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/view.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/voice.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/wad.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/world.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/zone.cpp
)

# Add include directories
include_directories(
	${CMAKE_CURRENT_SOURCE_DIR}
	${CMAKE_CURRENT_SOURCE_DIR}/${OGS_ENGINE_IMPL_TYPE}
)

if(WIN32)
	set(TARGET_PLATFORM win32)
elseif(UNIX)
	set(TARGET_PLATFORM linux)
else()
	set(TARGET_PLATFORM null)
endif()

file(GLOB ENGINE_PLATFORM_SOURCES
	${CMAKE_CURRENT_SOURCE_DIR}/${TARGET_PLATFORM}/*.cpp
)

#Add in the directories
add_sources(
	#${SRC_DIR}/common/BaseSystemModule.cpp
	#${SRC_DIR}/common/FilePaths.cpp
	#${SRC_DIR}/common/ObjectList.cpp
	#${SRC_DIR}/common/TokenLine.cpp
	#${SRC_DIR}/public/interface.cpp
	#${SRC_DIR}/public/registry.cpp
	#${SRC_DIR}/vgui2/vgui_surfacelib/FontAmalgam.cpp
	#${SRC_DIR}/vgui2/vgui_surfacelib/FontManager.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/vgui2/BaseUI_Interface.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/vgui2/BaseUISurface.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/vgui2/FontTextureCache.cpp
	#${CMAKE_CURRENT_SOURCE_DIR}/vgui2/text_draw.cpp
	#${ENGINE_SOURCES}
	#${ENGINE_PLATFORM_SOURCES}
)

if( WIN32 )
	add_sources(
		#${SRC_DIR}/vgui2/vgui_surfacelib/Win32Font.cpp
	)
elseif( UNIX )
	add_sources(
		#${SRC_DIR}/vgui2/vgui_surfacelib/LinuxFont.cpp
	)
endif()

preprocess_sources()

# Detect output lib type (static/dynamic)
if(OGS_ENGINE_STATIC)
	set(LIB_TYPE STATIC)
else()
	set(LIB_TYPE MODULE)
endif()

# Build as static/dynamic library
#add_library( ${PROJECT_NAME} SHARED ${PREP_SRCS} )
add_library(${PROJECT_NAME} ${LIB_TYPE} ${PROJECT_SOURCES} ${PROJECT_HEADERS})

target_include_directories( ${PROJECT_NAME} PRIVATE
	${CMAKE_CURRENT_SOURCE_DIR}
	${SRC_DIR}
	${SRC_DIR}/cl_dll
	${SRC_DIR}/common
	${SRC_DIR}/pm_shared/
	${SRC_DIR}/public
	${SRC_DIR}/public/tier1
	${SRC_DIR}/utils/vgui/include
	${SHARED_INCLUDE_PATHS}
	${EXTERNAL_DIR}/SDL2/include
	${EXTERNAL_DIR}/GLEW/include
	${OPENGL_INCLUDE_DIR}
	${SHARED_EXTERNAL_INCLUDE_PATHS}
)

target_compile_definitions( ${PROJECT_NAME} PRIVATE
	${SHARED_DEFS}
	USE_BREAKPAD_HANDLER
	GLEW_STATIC
)

#Find dependencies
set( PROJECT_LIBS )
if( WIN32 )
	set( PROJECT_LIBS ${PROJECT_LIBS} wsock32 ws2_32 )
endif()

if( UNIX )
	if ( OSX )
		set( VGUI1_NAME "vgui.dylib" )
	else()
		set( VGUI1_NAME "vgui.so" )
	endif()
else()
	set( VGUI1_NAME "vgui.lib" )
endif()

#VGUI1 has issues linking due to its non-standard filename. To work around this, we can instead import it and manually specify the location.
#That sets the correct linker path and filename.
add_library( vgui SHARED IMPORTED )
set_property( TARGET vgui PROPERTY IMPORTED_LOCATION ${ENGINE_BASE_PATH}/${VGUI1_NAME} )
set_property( TARGET vgui PROPERTY IMPORTED_IMPLIB ${SRC_DIR}/utils/vgui/lib/win32_vc6/vgui.lib )

#Link with dependencies
#target_link_libraries( ${PROJECT_NAME} 
#	${SDL2}
#	${GLEW2}
#	${OPENGL_LIBRARIES}
#	vgui2_controls
#	vgui
#	vstdlib
#	tier1
#	tier0
#	${STEAM_API}
#	${PROJECT_LIBS}
#	${SHARED_DEPENDENCIES}
#)

#CMake places libraries in /Debug or /Release on Windows, so explicitly set the paths for both.
#On Linux, it uses LIBRARY_OUTPUT_DIRECTORY
#set_target_properties(${PROJECT_NAME} PROPERTIES
	#LIBRARY_OUTPUT_DIRECTORY "${ENGINE_BASE_PATH}"
	#RUNTIME_OUTPUT_DIRECTORY_DEBUG "${ENGINE_BASE_PATH}"
	#RUNTIME_OUTPUT_DIRECTORY_RELEASE "${ENGINE_BASE_PATH}"
#)

set( PROJECT_LINK_FLAGS )

if( NOT MSVC AND NOT APPLE )
	#Generate a map file that provides information about the linking stage.
	set( PROJECT_LINK_FLAGS
		${PROJECT_LINK_FLAGS} "-Wl,-Map,${PROJECT_NAME}_map.txt "
	)
endif()

set_target_properties( ${PROJECT_NAME} PROPERTIES
	COMPILE_FLAGS "${LINUX_32BIT_FLAG}"
	LINK_FLAGS "${SHARED_ENGINE_LINKER_FLAGS} ${PROJECT_LINK_FLAGS} ${LINUX_32BIT_FLAG}"
)

#No lib prefix
set_target_properties( ${PROJECT_NAME} PROPERTIES PREFIX "" )

#Create filters
create_source_groups( "${CMAKE_SOURCE_DIR}" )

clear_sources()

#
#End library
#


if(OGS_EVOL)
	message(STATUS "Engine: Evol (" ${LIB_TYPE} ")")
else()
	message(STATUS "Engine: Vanilla (" ${LIB_TYPE} ")")
endif()

# Set target properties
set_target_properties(${PROJECT_NAME} PROPERTIES
#	DEBUG_POSTFIX "_d"

if(OGS_EVOL)
	OUTPUT_NAME "engine"
else()
	if(OGS_DEDICATED)
		OUTPUT_NAME "swds"
	else()
		OUTPUT_NAME "hw"
	endif()
endif()
)