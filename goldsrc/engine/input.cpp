/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/// @file

#include "quakedef.h"

/*
===========
IN_ActivateMouse
===========
*/
void IN_ActivateMouse()
{
	ClientDLL_ActivateMouse();
}

/*
===========
IN_DeactivateMouse
===========
*/
void IN_DeactivateMouse()
{
	ClientDLL_DeactivateMouse();
}

/*
===========
IN_MouseEvent
===========
*/
void IN_MouseEvent(int mstate)
{
	ClientDLL_MouseEvent(mstate);
}

/*
===========
IN_Accumulate
===========
*/
void IN_Accumulate() // unused?
{
	ClientDLL_IN_Accumulate();
}

/*
===================
IN_ClearStates
===================
*/
void IN_ClearStates()
{
	ClientDLL_ClearStates();
}