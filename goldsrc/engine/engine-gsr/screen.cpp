#include "quakedef.h"
#include "gl_screen.h"
#include "screen.h"

//SCR_SizeUp
void HudSizeUp()
{
	if(scr_viewsize.value < 120.0)
	{
		float value = scr_viewsize.value + 10.0;
		Cvar_SetValue("viewsize", value);
	}
}

//SCR_SizeDown
void HudSizeDown()
{
	if(scr_viewsize.value > 30.0)
	{
		float value = scr_viewsize.value - 10.0;
		Cvar_SetValue("viewsize", value);
	}
}
