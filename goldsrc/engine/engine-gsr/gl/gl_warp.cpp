



int fgetLittleShort(byte **ppBuffer)
{
	byte b1 = (*ppBuffer)[0];
	byte b2 = (*ppBuffer)[1];

	*ppBuffer += 2;

	return (short)(b1 + b2 * 256);
}

int fgetLittleLong(byte **ppBuffer)
{
	byte b1 = (*ppBuffer)[0];
	byte b2 = (*ppBuffer)[1];
	byte b3 = (*ppBuffer)[2];
	byte b4 = (*ppBuffer)[3];

	*ppBuffer += 4;

	return b1 + (b2 << 8) + (b3 << 16) + (b4 << 24);
}