
#include "qgl.h"
#include "wad.h"

extern cvar_t gl_ansio;

extern GLenum oldtarget;

int Draw_MessageCharacterAdd(int x, int y, int num, int rr, int gg, int bb, unsigned int font);

GLuint GL_GenTexture();

void GL_SelectTexture(GLenum target);