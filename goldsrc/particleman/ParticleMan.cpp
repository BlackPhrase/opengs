/*
 *	This file is part of OGS Engine
 *	Copyright (C) 2016-2017 OGS Dev Team
 *
 *	OGS Engine is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	OGS Engine is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OGS Engine. If not, see <http://www.gnu.org/licenses/>.
 *
 *	In addition, as a special exception, the author gives permission to
 *	link the code of OGS Engine with the Half-Life Game Engine ("GoldSrc/GS
 *	Engine") and Modified Game Libraries ("MODs") developed by Valve,
 *	L.L.C ("Valve"). You must obey the GNU General Public License in all
 *	respects for all of the code used other than the GoldSrc Engine and MODs
 *	from Valve. If you modify this file, you may extend this exception
 *	to your version of the file, but you are not obligated to do so. If
 *	you do not wish to do so, delete this exception statement from your
 *	version.
*/

/// @file

#include "ParticleMan.hpp"

EXPOSE_SINGLE_INTERFACE(CParticleMan, IParticleMan, PARTICLEMAN_INTERFACE_VERSION);

CParticleMan::CParticleMan() = default;
CParticleMan::~CParticleMan() = default;

void CParticleMan::SetUp(cl_enginefunc_t *apEngineFuncs){};

void CParticleMan::Update(){};

void CParticleMan::SetVariables(float afGravity, Vector avViewAngles){};

void CParticleMan::ResetParticles(){};

void CParticleMan::ApplyForce(Vector avOrigin, Vector avDirection, float afRadius, float afStrength, float afDuration){};

void CParticleMan::AddCustomParticleClassSize(unsigned long anSize){};

CBaseParticle *CParticleMan::CreateParticle(Vector avOrg, Vector avNormal, model_s *apSprite, float afSize, float afBrightness, const char *classname)
{
	return nullptr;
};

char *CParticleMan::RequestNewMemBlock(int anSize)
{
	return nullptr;
};

void CParticleMan::CoreInitializeSprite(CCoreTriangleEffect *pParticle, Vector org, Vector normal, model_s *sprite, float size, float brightness){};

void CParticleMan::CoreThink(CCoreTriangleEffect *pParticle, float time){};

void CParticleMan::CoreDraw(CCoreTriangleEffect *pParticle){};

void CParticleMan::CoreAnimate(CCoreTriangleEffect *pParticle, float time){};

void CParticleMan::CoreAnimateAndDie(CCoreTriangleEffect *pParticle, float time){};

void CParticleMan::CoreExpand(CCoreTriangleEffect *pParticle, float time){};

void CParticleMan::CoreContract(CCoreTriangleEffect *pParticle, float time){};

void CParticleMan::CoreFade(CCoreTriangleEffect *pParticle, float time){};

void CParticleMan::CoreSpin(CCoreTriangleEffect *pParticle, float time){};

void CParticleMan::CoreCalculateVelocity(CCoreTriangleEffect *pParticle, float time){};

void CParticleMan::CoreCheckCollision(CCoreTriangleEffect *pParticle, float time){};

void CParticleMan::CoreTouch(CCoreTriangleEffect *pParticle, Vector pos, Vector normal, int index){};

void CParticleMan::CoreDie(CCoreTriangleEffect *pParticle){};

void CParticleMan::CoreForce(CCoreTriangleEffect *pParticle){};

bool CParticleMan::CoreCheckVisibility(CCoreTriangleEffect *pParticle)
{
	return false;
};

void CParticleMan::SetRender(int iRender){};