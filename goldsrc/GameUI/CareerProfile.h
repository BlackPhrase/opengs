/// @file

#pragma once

#include <string>
#include <vector>

struct SavedMap
{
	std::string name{ "" };
	bool defeated{ false };
};

typedef std::vector<std::string> StringVector;
typedef std::vector<SavedMap> SavedMapVector;

class CCareerProfileDifficultyData
{
public:
	bool played{ false };
	int points{ 0 };
	StringVector hired;
	SavedMapVector maps;
	std::string lastMap{ "" };
	int pointsFlashed{ 0 };

	void Reset();
};

class CCareerProfileData
{
public:
	int filetime{ 0 };
	std::string name{ "" };
	int sliderPos{ 0 };

	CareerDifficultyType lastDifficulty;

	bool medals[MAX_CAREER_DIFFICULTY];
	CCareerProfileDifficultyData difficulty[MAX_CAREER_DIFFICULTY]{};

	int tutorData[256]{};

	void Reset();
};