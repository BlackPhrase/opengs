/// @file

#pragma once

struct CvarLatch
{
	CvarLatch(const char *cvarName, const char *defaultValue);
	~CvarLatch();

	char *m_cvarName;
	char *m_oldValue;
};