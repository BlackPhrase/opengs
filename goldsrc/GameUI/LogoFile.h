//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose:
//
// $NoKeywords: $
//
//=============================================================================//

/// @file

#pragma once

void UpdateLogoWAD(void *phdib, int r, int g, int b);

bool UpdateLogoWADFromTGA(const char *tga);