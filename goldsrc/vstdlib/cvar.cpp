/*
 *	This file is part of OGS Engine
 *	Copyright (C) 2016-2017 OGS Dev Team
 *
 *	OGS Engine is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	OGS Engine is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OGS Engine. If not, see <http://www.gnu.org/licenses/>.
 *
 *	In addition, as a special exception, the author gives permission to
 *	link the code of OGS Engine with the Half-Life Game Engine ("GoldSrc/GS
 *	Engine") and Modified Game Libraries ("MODs") developed by Valve,
 *	L.L.C ("Valve"). You must obey the GNU General Public License in all
 *	respects for all of the code used other than the GoldSrc Engine and MODs
 *	from Valve. If you modify this file, you may extend this exception
 *	to your version of the file, but you are not obligated to do so. If
 *	you do not wish to do so, delete this exception statement from your
 *	version.
*/

/// @file

#include "vstdlib/cvar.h"

class CDefaultCvar final : public ICvar
{
public:
	CDefaultCvar() = default;
	~CDefaultCvar() = default;

	void RegisterConCommandBase(ConCommandBase *variable) override;

	const char *GetCommandLineValue(const char *pVariableName) override;

	ConVar *FindVar(const char *var_name) override;
	const ConVar *FindVar(const char *var_name) const override;

	ConCommandBase *GetCommands() override;

private:
	// TODO: cvar list here
};

//static CDefaultCvar gCvar;
EXPOSE_SINGLE_INTERFACE /*_GLOBALVAR*/ (CDefaultCvar, ICvar, VENGINE_CVAR_INTERFACE_VERSION /*, gCvar*/)

CreateInterfaceFn VStdLib_GetICVarFactory()
{
	return Sys_GetFactoryThis();
};

void CDefaultCvar::RegisterConCommandBase(ConCommandBase *variable)
{
	//variable->registered = true;
};

const char *CDefaultCvar::GetCommandLineValue(const char *pVariableName)
{
	return "";
};

ConVar *CDefaultCvar::FindVar(const char *var_name)
{
	//for(auto It : myVarMap)
	//{
	//if(!strcmp(It.first.c_str(), var_name))
	//return It.second;
	//};

	return nullptr;
};

const ConVar *CDefaultCvar::FindVar(const char *var_name) const
{
	return FindVar(var_name);
};

ConCommandBase *CDefaultCvar::GetCommands()
{
	return nullptr; // myVarMap.front().second();
};