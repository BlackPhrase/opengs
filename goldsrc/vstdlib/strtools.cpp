//=========== (C) Copyright 1999 Valve, L.L.C. All rights reserved. ===========
//
// $Header: $
// $NoKeywords: $
//
// Contains string tools somehow missing from CRT
//=============================================================================

/// @file

// These are redefined in the project settings to prevent anyone from using them.
// We in this module are of a higher caste and thus are privileged in their use.
#ifdef strncpy
#undef strncpy
#endif

#ifdef strncat
#undef strncat
#endif

#ifdef _vsnprintf
#ifdef _WIN32
#undef _vsnprintf
#endif
#endif

#ifdef _snprintf
#undef _snprintf
#endif

// NOTE: I have to include stdio + stdarg first so vsnprintf gets compiled in
#include <cstdio>
#include <cstdarg>

#ifdef __linux__
#include <cctype>
#elif _WIN32
#include <direct.h>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

#include "tier0/dbg.h"
#include "vstdlib/strtools.h"
#include <cstring>
#include <cstdlib>
#include "basetypes.h"
//#include "utldict.h"

void _Q_memset(const char *file, int line, void *dest, int fill, int count)
{
	Assert(count >= 0);
	AssertValidWritePtr(dest, count);

	memset(dest, fill, count);
}

void _Q_memcpy(const char *file, int line, void *dest, const void *src, int count)
{
	Assert(count >= 0);
	AssertValidReadPtr(src, count);
	AssertValidWritePtr(dest, count);

	memcpy(dest, src, count);
}

void _Q_memmove(const char *file, int line, void *dest, const void *src, int count)
{
	Assert(count >= 0);
	AssertValidReadPtr(src, count);
	AssertValidWritePtr(dest, count);

	memmove(dest, src, count);
}

//int _Q_memcmp(const char *file, int line, const void *m1, const void *m2, int count)
int _Q_memcmp(const char *file, int line, void *m1, void *m2, int count)
{
	Assert(count >= 0);
	AssertValidReadPtr(m1, count);
	AssertValidReadPtr(m2, count);

	return memcmp(m1, m2, count);
}

int _Q_strlen(const char *file, int line, const char *str)
{
	AssertValidStringPtr(str);
	return strlen(str);
}

void _Q_strcpy(const char *file, int line, char *dest, const char *src) // Not present in GS
{
	AssertValidWritePtr(dest);
	AssertValidStringPtr(src);

	strcpy(dest, src);
}

char *_Q_strrchr(const char *file, int line, const char *s, char c)
{
	AssertValidStringPtr(s);
	int len = Q_strlen(s);
	s += len;
	while(len--)
		if(*--s == c)
			return (char *)s;
	return 0;
}

int _Q_strcmp(const char *file, int line, const char *s1, const char *s2)
{
	AssertValidStringPtr(s1);
	AssertValidStringPtr(s2);

	while(1)
	{
		if(*s1 != *s2)
			return -1; // strings not equal
		if(!*s1)
			return 0; // strings are equal
		s1++;
		s2++;
	}

	return -1;
}

int _Q_stricmp(const char *file, int line, const char *s1, const char *s2)
{
	AssertValidStringPtr(s1);
	AssertValidStringPtr(s2);

	return stricmp(s1, s2);
}

int _Q_wcscmp(const char *file, int line, const wchar_t *s1, const wchar_t *s2)
{
	// TODO
	return 0;
}

char *_Q_strstr(const char *file, int line, const char *s1, const char *search)
{
	AssertValidStringPtr(s1);
	AssertValidStringPtr(search);

	return strstr(s1, search);
}

char *_Q_strupr(const char *file, int line, char *start)
{
	AssertValidStringPtr(start);
	return strupr(start);
}

char *_Q_strlower(const char *file, int line, char *start)
{
	AssertValidStringPtr(start);
	return strlwr(start);
}

int _Q_wcslen(const char *file, int line, const wchar_t *pwch)
{
	return wcslen(pwch);
}

void Q_strcat(char *dest, const char *src)
{
	AssertValidStringPtr(dest);
	AssertValidStringPtr(src);

	dest += Q_strlen(dest);
	Q_strcpy(dest, src);
}

int Q_strncmp(const char *s1, const char *s2, int count)
{
	Assert(count >= 0);
	AssertValidStringPtr(s1, count);
	AssertValidStringPtr(s2, count);

	while(1)
	{
		if(!count--)
			return 0;
		if(*s1 != *s2)
			return -1; // strings not equal
		if(!*s1)
			return 0; // strings are equal
		s1++;
		s2++;
	}

	return -1;
}

char *Q_strnlwr(char *s, size_t count) // Not present in GS
{
	Assert(count >= 0);
	AssertValidStringPtr(s, count);

	char *pRet = s;
	if(!s)
		return s;

	while(--count >= 0)
	{
		if(!*s)
			break;

		*s = tolower(*s);
		++s;
	}

	return pRet;
}

/*
int Q_strncasecmp(const char *s1, const char *s2, int n)
{
	Assert(n >= 0);
	AssertValidStringPtr(s1);
	AssertValidStringPtr(s2);

	int c1, c2;

	while(1)
	{
		c1 = *s1++;
		c2 = *s2++;

		if(!n--)
			return 0; // strings are equal until end point

		if(c1 != c2)
		{
			if(c1 >= 'a' && c1 <= 'z')
				c1 -= ('a' - 'A');
			if(c2 >= 'a' && c2 <= 'z')
				c2 -= ('a' - 'A');
			if(c1 != c2)
				return -1; // strings not equal
		}
		if(!c1)
			return 0; // strings are equal
		              //              s1++;
		              //              s2++;
	}

	return -1;
}

int Q_strcasecmp(const char *s1, const char *s2)
{
	AssertValidStringPtr(s1);
	AssertValidStringPtr(s2);

	return Q_strncasecmp(s1, s2, 99999);
}
*/

int Q_stricmp(const char *str1, const char *str2)
{
	// It is not uncommon to compare a string to itself. See
	// VPanelWrapper::GetPanel which does this a lot. Since stricmp
	// is expensive and pointer comparison is cheap, this simple test
	// can save a lot of cycles, and cache pollution.
	if(str1 == str2)
	{
		return 0;
	}
	const unsigned char *s1 = (const unsigned char *)str1;
	const unsigned char *s2 = (const unsigned char *)str2;
	for(; *s1; ++s1, ++s2)
	{
		if(*s1 != *s2)
		{
			// in ascii char set, lowercase = uppercase | 0x20
			unsigned char c1 = *s1 | 0x20;
			unsigned char c2 = *s2 | 0x20;
			if(c1 != c2 || (unsigned char)(c1 - 'a') > ('z' - 'a'))
			{
				// if non-ascii mismatch, fall back to CRT for locale
				if((c1 | c2) >= 0x80)
					return stricmp((const char *)s1, (const char *)s2);
				// ascii mismatch. only use the | 0x20 value if alphabetic.
				if((unsigned char)(c1 - 'a') > ('z' - 'a'))
					c1 = *s1;
				if((unsigned char)(c2 - 'a') > ('z' - 'a'))
					c2 = *s2;
				return c1 > c2 ? 1 : -1;
			}
		}
	}
	return *s2 ? -1 : 0;
}

int Q_strnicmp(const char *s1, const char *s2, int n)
{
	Assert(n >= 0);
	AssertValidStringPtr(s1);
	AssertValidStringPtr(s2);

	return Q_strncasecmp(s1, s2, n);
}

int Q_atoi(const char *str)
{
	AssertValidStringPtr(str);

	int val;
	int sign;
	int c;

	Assert(str);
	if(*str == '-')
	{
		sign = -1;
		str++;
	}
	else
		sign = 1;

	val = 0;

	//
	// check for hex
	//
	if(str[0] == '0' && (str[1] == 'x' || str[1] == 'X'))
	{
		str += 2;
		while(1)
		{
			c = *str++;
			if(c >= '0' && c <= '9')
				val = (val << 4) + c - '0';
			else if(c >= 'a' && c <= 'f')
				val = (val << 4) + c - 'a' + 10;
			else if(c >= 'A' && c <= 'F')
				val = (val << 4) + c - 'A' + 10;
			else
				return val * sign;
		}
	}

	//
	// check for character
	//
	if(str[0] == '\'')
	{
		return sign * str[1];
	}

	//
	// assume decimal
	//
	while(1)
	{
		c = *str++;
		if(c < '0' || c > '9')
			return val * sign;
		val = val * 10 + c - '0';
	}

	return 0;
}

float Q_atof(const char *str)
{
	AssertValidStringPtr(str);
	double val;
	int sign;
	int c;
	int decimal, total;

	if(*str == '-')
	{
		sign = -1;
		str++;
	}
	else
		sign = 1;

	val = 0;

	//
	// check for hex
	//
	if(str[0] == '0' && (str[1] == 'x' || str[1] == 'X'))
	{
		str += 2;
		while(1)
		{
			c = *str++;
			if(c >= '0' && c <= '9')
				val = (val * 16) + c - '0';
			else if(c >= 'a' && c <= 'f')
				val = (val * 16) + c - 'a' + 10;
			else if(c >= 'A' && c <= 'F')
				val = (val * 16) + c - 'A' + 10;
			else
				return val * sign;
		}
	}

	//
	// check for character
	//
	if(str[0] == '\'')
	{
		return sign * str[1];
	}

	//
	// assume decimal
	//
	decimal = -1;
	total = 0;
	while(1)
	{
		c = *str++;
		if(c == '.')
		{
			decimal = total;
			continue;
		}
		if(c < '0' || c > '9')
			break;
		val = val * 10 + c - '0';
		total++;
	}

	if(decimal == -1)
		return val * sign;
	while(total > decimal)
	{
		val /= 10;
		total--;
	}

	return val * sign;
}

//-----------------------------------------------------------------------------
// Finds a string in another string with a case insensitive test
//-----------------------------------------------------------------------------

char const *Q_stristr(char const *pStr, char const *pSearch)
{
	AssertValidStringPtr(pStr);
	AssertValidStringPtr(pSearch);

	if(!pStr || !pSearch)
		return 0;

	char const *pLetter = pStr;

	// Check the entire string
	while(*pLetter != 0)
	{
		// Skip over non-matches
		if(tolower(*pLetter) == tolower(*pSearch))
		{
			// Check for match
			char const *pMatch = pLetter + 1;
			char const *pTest = pSearch + 1;
			while(*pTest != 0)
			{
				// We've run off the end; don't bother.
				if(*pMatch == 0)
					return 0;

				if(tolower(*pMatch) != tolower(*pTest))
					break;

				++pMatch;
				++pTest;
			}

			// Found a match!
			if(*pTest == 0)
				return pLetter;
		}

		++pLetter;
	}

	return 0;
}

char *Q_stristr(char *pStr, char const *pSearch)
{
	AssertValidStringPtr(pStr);
	AssertValidStringPtr(pSearch);

	return (char *)Q_stristr((char const *)pStr, pSearch);
}

//-----------------------------------------------------------------------------
// Finds a string in another string with a case insensitive test w/ length validation
//-----------------------------------------------------------------------------

static int FastToLower(char c)
{
	int i = (unsigned char)c;
	if(i < 0x80)
	{
		// Brutally fast branchless ASCII tolower():
		i += (((('A' - 1) - i) & (i - ('Z' + 1))) >> 26) & 0x20;
	}
	else
	{
		i += isupper(i) ? 0x20 : 0;
	}
	return i;
}

char const *Q_strnistr(char const *pStr, char const *pSearch, int n)
{
	AssertValidStringPtr(pStr);
	AssertValidStringPtr(pSearch);

	if(!pStr || !pSearch)
		return 0;

	char const *pLetter = pStr;

	// Check the entire string
	while(*pLetter != 0)
	{
		if(n <= 0)
			return 0;

		// Skip over non-matches
		if(FastToLower(*pLetter) == FastToLower(*pSearch))
		{
			int n1 = n - 1;

			// Check for match
			char const *pMatch = pLetter + 1;
			char const *pTest = pSearch + 1;
			while(*pTest != 0)
			{
				if(n1 <= 0)
					return 0;

				// We've run off the end; don't bother.
				if(*pMatch == 0)
					return 0;

				if(FastToLower(*pMatch) != FastToLower(*pTest))
					break;

				++pMatch;
				++pTest;
				--n1;
			}

			// Found a match!
			if(*pTest == 0)
				return pLetter;
		}

		++pLetter;
		--n;
	}

	return 0;
}

const char *Q_strnchr(const char *pStr, char c, int n)
{
	char const *pLetter = pStr;
	char const *pLast = pStr + n;

	// Check the entire string
	while((pLetter < pLast) && (*pLetter != 0))
	{
		if(*pLetter == c)
			return pLetter;
		++pLetter;
	}
	return NULL;
}

void Q_strncpy(char *pDest, char const *pSrc, int maxLen)
{
	Assert(maxLen >= 0); // Assert(maxLen >= sizeof(*pDest));
	AssertValidWritePtr(pDest, maxLen);
	AssertValidStringPtr(pSrc);

	strncpy(pDest, pSrc, maxLen);
	if(maxLen) // if(maxLen > 0)
		pDest[maxLen - 1] = 0; // '\0'
}

// warning C6053: Call to 'wcsncpy' might not zero-terminate string 'pDest'
// warning C6059: Incorrect length parameter in call to 'strncat'. Pass the number of remaining characters, not the buffer size of 'argument 1'
// warning C6386: Buffer overrun: accessing 'argument 1', the writable size is 'destBufferSize' bytes, but '1000' bytes might be written
// These warnings were investigated through code inspection and writing of tests and they are
// believed to all be spurious.
#ifdef _PREFAST_
#pragma warning(push)
#pragma warning(disable : 6053 6059 6386)
#endif

void Q_wcsncpy(wchar_t *pDest, wchar_t const *pSrc, int maxLenInBytes)
{
	Assert(maxLenInBytes >= sizeof(*pDest));
	AssertValidWritePtr(pDest, maxLenInBytes);
	AssertValidReadPtr(pSrc);

	int maxLen = maxLenInBytes / sizeof(wchar_t);

	wcsncpy(pDest, pSrc, maxLen);
	if(maxLen)
	{
		pDest[maxLen - 1] = 0;
	}
}

int Q_snprintf(char *pDest, int maxLen, char const *pFormat, ...)
{
	Assert(maxLen >= 0);
	AssertValidWritePtr(pDest, maxLen);
	AssertValidStringPtr(pFormat);

	va_list marker;

	va_start(marker, pFormat);
	int len = _vsnprintf(pDest, maxLen, pFormat, marker);
	va_end(marker);

	// Len < 0 represents an overflow
	if(len < 0)
	{
		len = maxLen;
		pDest[maxLen - 1] = 0;
	}

	return len;
}

int Q_vsnprintf(char *pDest, int maxLen, char const *pFormat, va_list params)
{
	Assert(maxLen >= 0);
	AssertValidWritePtr(pDest, maxLen);
	AssertValidStringPtr(pFormat);

	int len = _vsnprintf(pDest, maxLen, pFormat, params);

	if(len < 0)
	{
		len = maxLen;
		pDest[maxLen - 1] = 0;
	}

	return len;
}

char *Q_strncat(char *pDest, const char *pSrc, size_t maxLen)
{
	Assert(maxLen >= 0);
	AssertValidStringPtr(pDest);
	AssertValidStringPtr(pSrc);

	int len = strlen(pDest);
	maxLen = (maxLen - 1) - len;

	if(maxLen <= 0)
		return pDest;

	char *pOut = strncat(pDest, pSrc, maxLen);
	pOut[len + maxLen] = 0;
	return pOut;
}

wchar_t *Q_wcsncat(INOUT_Z_CAP(cchDest) wchar_t *pDest, const wchar_t *pSrc, size_t cchDest, int max_chars_to_copy)
//wchar_t *Q_wcsncat(wchar_t*, wchar_t const*, unsigned int, int)
{
	size_t charstocopy = (size_t)0;

	Assert((ptrdiff_t)cchDest >= 0);

	size_t len = wcslen(pDest);
	size_t srclen = wcslen(pSrc);
	if(max_chars_to_copy <= COPY_ALL_CHARACTERS)
	{
		charstocopy = srclen;
	}
	else
	{
		charstocopy = (size_t)std::min(max_chars_to_copy, (int)srclen);
	}

	if(len + charstocopy >= cchDest)
	{
		charstocopy = cchDest - len - 1;
	}

	if((int)charstocopy <= 0)
	{
		return pDest;
	}

	ANALYZE_SUPPRESS(6059); // warning C6059: : Incorrect length parameter in call to 'strncat'. Pass the number of remaining characters, not the buffer size of 'argument 1'
	wchar_t *pOut = wcsncat(pDest, pSrc, charstocopy);
	return pOut;
}

//-----------------------------------------------------------------------------
// Convert multibyte to wchar + back
//-----------------------------------------------------------------------------
//void Q_strtowcs(const char *pString, int nInSize, wchar_t *pWString, int nOutSizeInBytes)
void Q_strtowcs(const char *pString, int nInSize, OUT_Z_BYTECAP(nOutSizeInBytes) wchar_t *pWString, int nOutSizeInBytes)
{
	Assert(nOutSizeInBytes >= sizeof(pWString[0]));
#ifdef _WIN32
	int nOutSizeInChars = nOutSizeInBytes / sizeof(pWString[0]);
	int result = MultiByteToWideChar(CP_UTF8, 0, pString, nInSize, pWString, nOutSizeInChars);
	// If the string completely fails to fit then MultiByteToWideChar will return 0.
	// If the string exactly fits but with no room for a null-terminator then MultiByteToWideChar
	// will happily fill the buffer and omit the null-terminator, returning nOutSizeInChars.
	// Either way we need to return an empty string rather than a bogus and possibly not
	// null-terminated result.
	if(result <= 0 || result >= nOutSizeInChars)
	{
		// If nInSize includes the null-terminator then a result of nOutSizeInChars is
		// legal. We check this by seeing if the last character in the output buffer is
		// a zero.
		if(result == nOutSizeInChars && pWString[nOutSizeInChars - 1] == 0)
		{
			// We're okay! Do nothing.
		}
		else
		{
			// The string completely to fit. Null-terminate the buffer.
			*pWString = L'\0';
		}
	}
	else
	{
		// We have successfully converted our string. Now we need to null-terminate it, because
		// MultiByteToWideChar will only do that if nInSize includes the source null-terminator!
		pWString[result] = 0;
	}
#elif __unix__
	if(mbstowcs(pWString, pString, nOutSizeInBytes / sizeof(pWString[0])) <= 0)
	{
		*pWString = 0;
	}
#endif
};

//void Q_wcstostr(const wchar_t *pWString, int nInSize, char *pString, int nOutSizeInChars)
void Q_wcstostr(const wchar_t *pWString, int nInSize, OUT_Z_CAP(nOutSizeInChars) char *pString, int nOutSizeInChars) // nOutSizeInBytes
{
#ifdef _WIN32
	int result = WideCharToMultiByte(CP_UTF8, 0, pWString, nInSize, pString, nOutSizeInChars, NULL, NULL);
	// If the string completely fails to fit then MultiByteToWideChar will return 0.
	// If the string exactly fits but with no room for a null-terminator then MultiByteToWideChar
	// will happily fill the buffer and omit the null-terminator, returning nOutSizeInChars.
	// Either way we need to return an empty string rather than a bogus and possibly not
	// null-terminated result.
	if(result <= 0 || result >= nOutSizeInChars)
	{
		// If nInSize includes the null-terminator then a result of nOutSizeInChars is
		// legal. We check this by seeing if the last character in the output buffer is
		// a zero.
		if(result == nOutSizeInChars && pWString[nOutSizeInChars - 1] == 0)
		{
			// We're okay! Do nothing.
		}
		else
		{
			*pString = '\0';
		}
	}
	else
	{
		// We have successfully converted our string. Now we need to null-terminate it, because
		// MultiByteToWideChar will only do that if nInSize includes the source null-terminator!
		pString[result] = '\0';
	}
#elif __unix__
	if(wcstombs(pString, pWString, nOutSizeInChars) <= 0)
	{
		*pString = '\0';
	}
#endif
}

int Q_vsnprintfRet(OUT_Z_CAP(maxLenInCharacters) char *pDest, int maxLenInCharacters, PRINTF_FORMAT_STRING const char *pFormat, va_list params, bool *pbTruncated)
{
	// TODO
	return 0;
};

//-----------------------------------------------------------------------------
// Purpose: Converts value into x.xx MB/ x.xx KB, x.xx bytes format, including commas
// Input  : value -
//			2 -
//			false -
// Output : char
//-----------------------------------------------------------------------------
const int NUM_PRETIFYMEM_BUFFERS = 8;

char *Q_pretifymem(float value, int digitsafterdecimal /*= 2*/, bool usebinaryonek /*= false*/)
{
	static char output[NUM_PRETIFYMEM_BUFFERS][32];
	static int current;

	float onekb = usebinaryonek ? 1024.0f : 1000.0f;
	float onemb = onekb * onekb;

	char *out = output[current];
	current = (current + 1) & (NUM_PRETIFYMEM_BUFFERS - 1);

	char suffix[8];

	// First figure out which bin to use
	if(value > onemb)
	{
		value /= onemb;
		sprintf(suffix, " Mb");
	}
	else if(value > onekb)
	{
		value /= onekb;
		sprintf(suffix, " Kb");
	}
	else
	{
		sprintf(suffix, " bytes");
	}

	char val[32];

	// Clamp to >= 0
	digitsafterdecimal = std::max(digitsafterdecimal, 0);

	// If it's basically integral, don't do any decimals
	if(FloatMakePositive(value - (int)value) < 0.00001)
	{
		sprintf(val, "%i%s", (int)value, suffix);
	}
	else
	{
		char fmt[32];

		// Otherwise, create a format string for the decimals
		sprintf(fmt, "%%.%if%s", digitsafterdecimal, suffix);
		sprintf(val, fmt, value);
	}

	// Copy from in to out
	char *i = val;
	char *o = out;

	// Search for decimal or if it was integral, find the space after the raw number
	char *dot = strstr(i, ".");
	if(!dot)
	{
		dot = strstr(i, " ");
	}

	// Compute position of dot
	int pos = dot - i;
	// Don't put a comma if it's <= 3 long
	pos -= 3;

	while(*i)
	{
		// If pos is still valid then insert a comma every third digit, except if we would be
		//  putting one in the first spot
		if(pos >= 0 && !(pos % 3))
		{
			// Never in first spot
			if(o != out)
			{
				*o++ = ',';
			}
		}

		// Count down comma position
		pos--;

		// Copy rest of data as normal
		*o++ = *i++;
	}

	// Terminate
	*o = 0;

	return out;
}

//-----------------------------------------------------------------------------
// Purpose: Returns a string representation of an integer with commas
//			separating the 1000s (ie, 37,426,421)
// Input  : value -		Value to convert
// Output : Pointer to a static buffer containing the output
//-----------------------------------------------------------------------------
#define NUM_PRETIFYNUM_BUFFERS 8 // Must be a power of two
char *Q_pretifynum(int64 inputValue)
{
	static char output[NUM_PRETIFYMEM_BUFFERS][32];
	static int current;

	// Point to the output buffer.
	char *const out = output[current];
	// Track the output buffer end for easy calculation of bytes-remaining.
	const char *const outEnd = out + sizeof(output[current]);

	// Point to the current output location in the output buffer.
	char *pchRender = out;
	// Move to the next output pointer.
	current = (current + 1) & (NUM_PRETIFYMEM_BUFFERS - 1);

	*out = 0;

	// In order to handle the most-negative int64 we need to negate it
	// into a uint64.
	uint64 value;
	// Render the leading minus sign, if necessary
	if(inputValue < 0)
	{
		Q_snprintf(pchRender, 32, "-");
		value = (uint64)-inputValue;
		// Advance our output pointer.
		pchRender += Q_strlen(pchRender);
	}
	else
	{
		value = (uint64)inputValue;
	}

	// Now let's find out how big our number is. The largest number we can fit
	// into 63 bits is about 9.2e18. So, there could potentially be six
	// three-digit groups.

	// We need the initial value of 'divisor' to be big enough to divide our
	// number down to 1-999 range.
	uint64 divisor = 1;
	// Loop more than six times to avoid integer overflow.
	for(int i = 0; i < 6; ++i)
	{
		// If our divisor is already big enough then stop.
		if(value < divisor * 1000)
			break;

		divisor *= 1000;
	}

	// Print the leading batch of one to three digits.
	int toPrint = value / divisor;
	Q_snprintf(pchRender, outEnd - pchRender, "%d", toPrint);

	for(;;)
	{
		// Advance our output pointer.
		pchRender += Q_strlen(pchRender);
		// Adjust our value to be printed and our divisor.
		value -= toPrint * divisor;
		divisor /= 1000;
		if(!divisor)
			break;

		// The remaining blocks of digits always include a comma and three digits.
		toPrint = value / divisor;
		Q_snprintf(pchRender, outEnd - pchRender, ",%03d", toPrint);
	}

	return out;
}

//-----------------------------------------------------------------------------
// Purpose: returns true if a wide character is a "mean" space; that is,
//			if it is technically a space or punctuation, but causes disruptive
//			behavior when used in names, web pages, chat windows, etc.
//
//			characters in this set are removed from the beginning and/or end of strings
//			by Q_AggressiveStripPrecedingAndTrailingWhitespaceW()
//-----------------------------------------------------------------------------
bool Q_IsMeanSpaceW(wchar_t wch)
{
	bool bIsMean = false;

	switch(wch)
	{
	case L'\x0082': // BREAK PERMITTED HERE
	case L'\x0083': // NO BREAK PERMITTED HERE
	case L'\x00A0': // NO-BREAK SPACE
	case L'\x034F': // COMBINING GRAPHEME JOINER
	case L'\x2000': // EN QUAD
	case L'\x2001': // EM QUAD
	case L'\x2002': // EN SPACE
	case L'\x2003': // EM SPACE
	case L'\x2004': // THICK SPACE
	case L'\x2005': // MID SPACE
	case L'\x2006': // SIX SPACE
	case L'\x2007': // figure space
	case L'\x2008': // PUNCTUATION SPACE
	case L'\x2009': // THIN SPACE
	case L'\x200A': // HAIR SPACE
	case L'\x200B': // ZERO-WIDTH SPACE
	case L'\x200C': // ZERO-WIDTH NON-JOINER
	case L'\x200D': // ZERO WIDTH JOINER
	case L'\x200E': // LEFT-TO-RIGHT MARK
	case L'\x2028': // LINE SEPARATOR
	case L'\x2029': // PARAGRAPH SEPARATOR
	case L'\x202F': // NARROW NO-BREAK SPACE
	case L'\x2060': // word joiner
	case L'\xFEFF': // ZERO-WIDTH NO BREAK SPACE
	case L'\xFFFC': // OBJECT REPLACEMENT CHARACTER
		bIsMean = true;
		break;
	}

	return bIsMean;
}

//-----------------------------------------------------------------------------
// Purpose: strips trailing whitespace; returns pointer inside string just past
// any leading whitespace.
//
// bAggresive = true causes this function to also check for "mean" spaces,
// which we don't want in persona names or chat strings as they're disruptive
// to the user experience.
//-----------------------------------------------------------------------------
static wchar_t *StripWhitespaceWorker(int cchLength, wchar_t *pwch, bool *pbStrippedWhitespace, bool bAggressive)
{
	// walk backwards from the end of the string, killing any whitespace
	*pbStrippedWhitespace = false;

	wchar_t *pwchEnd = pwch + cchLength;
	while(--pwchEnd >= pwch)
	{
		if(!iswspace(*pwchEnd) && (!bAggressive || !Q_IsMeanSpaceW(*pwchEnd)))
			break;

		*pwchEnd = 0;
		*pbStrippedWhitespace = true;
	}

	// walk forward in the string
	while(pwch < pwchEnd)
	{
		if(!iswspace(*pwch))
			break;

		*pbStrippedWhitespace = true;
		pwch++;
	}

	return pwch;
}

//-----------------------------------------------------------------------------
// Purpose: strips leading and trailing whitespace
//-----------------------------------------------------------------------------
bool Q_StripPrecedingAndTrailingWhitespace(char *pch)
{
	int cch = Q_strlen(pch);

	// Early out and don't convert if we don't have any chars or leading/trailing ws.
	if((cch < 1) || (!isspace((unsigned char)pch[0]) && !isspace((unsigned char)pch[cch - 1])))
		return false;

	// convert to unicode
	int cubDest = (cch + 1) * sizeof(wchar_t);
	wchar_t *pwch = (wchar_t *)stackalloc(cubDest);
	int cwch = 0; //Q_UTF8ToUnicode(pch, pwch, cubDest) / sizeof(wchar_t); // TODO

	bool bStrippedWhitespace = false;
	pwch = StripWhitespaceWorker(cwch - 1, pwch, &bStrippedWhitespace, false /* not aggressive */);

	// copy back, if necessary
	if(bStrippedWhitespace)
	{
		//Q_UnicodeToUTF8(pwch, pch, cch); // TODO
	}

	return bStrippedWhitespace;
}

// Even though \ on Posix (Linux&Mac) isn't techincally a path separator we are
// now counting it as one even Posix since so many times our filepaths aren't actual
// paths but rather text strings passed in from data files, treating \ as a pathseparator
// covers the full range of cases
bool PATHSEPARATOR(char c)
{
	return c == '\\' || c == '/';
}

//-----------------------------------------------------------------------------
// Purpose: Extracts the base name of a file (no path, no extension, assumes '/' or '\' as path separator)
// Input  : *in -
//			*out -
//			maxlen -
//-----------------------------------------------------------------------------
void Q_FileBase(const char *in, char *out, int maxlen)
{
	Assert(maxlen >= 1);
	Assert(in);
	Assert(out);

	if(!in || !in[0])
	{
		*out = 0;
		return;
	}

	int len, start, end;

	len = Q_strlen(in);

	// scan backward for '.'
	end = len - 1;
	while(end && in[end] != '.' && !PATHSEPARATOR(in[end]))
	{
		end--;
	}

	if(in[end] != '.') // no '.', copy to end
	{
		end = len - 1;
	}
	else
	{
		end--; // Found ',', copy to left of '.'
	}

	// Scan backward for '/'
	start = len - 1;
	while(start >= 0 && !PATHSEPARATOR(in[start]))
	{
		start--;
	}

	if(start < 0 || !PATHSEPARATOR(in[start]))
	{
		start = 0;
	}
	else
	{
		start++;
	}

	// Length of new sting
	len = end - start + 1;

	int maxcopy = std::min(len + 1, maxlen);

	// Copy partial string
	Q_strncpy(out, &in[start], maxcopy);
}

//-----------------------------------------------------------------------------
// Purpose:
// Input  : *ppath -
//-----------------------------------------------------------------------------
void Q_StripTrailingSlash(char *ppath)
{
	Assert(ppath);

	int len = Q_strlen(ppath);
	if(len > 0)
	{
		if(PATHSEPARATOR(ppath[len - 1]))
		{
			ppath[len - 1] = 0;
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose:
// Input  : *in -
//			*out -
//			outSize -
//-----------------------------------------------------------------------------
void Q_StripExtension(const char *in, char *out, int outSize)
{
	// Find the last dot. If it's followed by a dot or a slash, then it's part of a
	// directory specifier like ../../somedir/./blah.

	// scan backward for '.'
	int end = Q_strlen(in) - 1;
	while(end > 0 && in[end] != '.' && !PATHSEPARATOR(in[end]))
	{
		--end;
	}

	if(end > 0 && !PATHSEPARATOR(in[end]) && end < outSize)
	{
		int nChars = std::min(end, outSize - 1);
		if(out != in)
		{
			memcpy(out, in, nChars);
		}
		out[nChars] = 0;
	}
	else
	{
		// nothing found
		if(out != in)
		{
			Q_strncpy(out, in, outSize);
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose:
// Input  : *path -
//			*extension -
//			pathStringLength -
//-----------------------------------------------------------------------------
void Q_DefaultExtension(char *path, const char *extension, int pathStringLength)
{
	Assert(path);
	Assert(pathStringLength >= 1);
	Assert(extension);
	Assert(extension[0] == '.');

	char *src;

	// if path doesn't have a .EXT, append extension
	// (extension should include the .)
	src = path + Q_strlen(path) - 1;

	while(!PATHSEPARATOR(*src) && (src > path))
	{
		if(*src == '.')
		{
			// it has an extension
			return;
		}
		src--;
	}

	// Concatenate the desired extension
	//Q_strncat(path, extension, pathStringLength, COPY_ALL_CHARACTERS); // TODO
}

//-----------------------------------------------------------------------------
// Purpose: Force extension...
// Input  : *path -
//			*extension -
//			pathStringLength -
//-----------------------------------------------------------------------------
void Q_SetExtension(char *path, const char *extension, int pathStringLength)
{
	Q_StripExtension(path, path, pathStringLength);

	// We either had an extension and stripped it, or didn't have an extension
	// at all. Either way, we need to concatenate our extension now.

	// extension is not required to start with '.', so if it's not there,
	// then append that first.
	if(extension[0] != '.')
	{
		//Q_strncat(path, ".", pathStringLength, COPY_ALL_CHARACTERS); // TODO
	}

	//Q_strncat(path, extension, pathStringLength, COPY_ALL_CHARACTERS); // TODO
}

//-----------------------------------------------------------------------------
// Purpose: Remove final filename from string
// Input  : *path -
// Output : void  Q_StripFilename
//-----------------------------------------------------------------------------
void Q_StripFilename(char *path)
{
	int length;

	length = Q_strlen(path) - 1;
	if(length <= 0)
		return;

	while(length > 0 &&
	      !PATHSEPARATOR(path[length]))
	{
		length--;
	}

	path[length] = 0;
}

#ifdef _WIN32
#define CORRECT_PATH_SEPARATOR '\\'
#define INCORRECT_PATH_SEPARATOR '/'
#elif __unix__
#define CORRECT_PATH_SEPARATOR '/'
#define INCORRECT_PATH_SEPARATOR '\\'
#endif

//-----------------------------------------------------------------------------
// Purpose: Changes all '/' or '\' characters into separator
// Input  : *pname -
//			separator -
//-----------------------------------------------------------------------------
void Q_FixSlashes(char *pname, char separator /* = CORRECT_PATH_SEPARATOR */)
{
	while(*pname)
	{
		if(*pname == INCORRECT_PATH_SEPARATOR || *pname == CORRECT_PATH_SEPARATOR)
		{
			*pname = separator;
		}
		pname++;
	}
}

//-----------------------------------------------------------------------------
// Purpose: Strip off the last directory from dirName
// Input  : *dirName -
//			maxlen -
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool Q_StripLastDir(char *dirName, int maxlen)
{
	if(dirName[0] == 0 ||
	   !Q_stricmp(dirName, "./") ||
	   !Q_stricmp(dirName, ".\\"))
		return false;

	int len = Q_strlen(dirName);

	Assert(len < maxlen);

	// skip trailing slash
	if(PATHSEPARATOR(dirName[len - 1]))
	{
		len--;
	}

	while(len > 0)
	{
		if(PATHSEPARATOR(dirName[len - 1]))
		{
			dirName[len] = 0;
			Q_FixSlashes(dirName, CORRECT_PATH_SEPARATOR);
			return true;
		}
		len--;
	}

	// Allow it to return an empty string and true. This can happen if something like "tf2/" is passed in.
	// The correct behavior is to strip off the last directory ("tf2") and return true.
	if(len == 0)
	{
		Q_snprintf(dirName, maxlen, ".%c", CORRECT_PATH_SEPARATOR);
		return true;
	}

	return true;
}

//-----------------------------------------------------------------------------
// Purpose: Returns a pointer to the beginning of the unqualified file name
//			(no path information)
// Input:	in - file name (may be unqualified, relative or absolute path)
// Output:	pointer to unqualified file name
//-----------------------------------------------------------------------------
const char *Q_UnqualifiedFileName(const char *in)
{
	// back up until the character after the first path separator we find,
	// or the beginning of the string
	const char *out = in + strlen(in) - 1;
	while((out > in) && (!PATHSEPARATOR(*(out - 1))))
		out--;
	return out;
}

//-----------------------------------------------------------------------------
// Purpose: Composes a path and filename together, inserting a path separator
//			if need be
// Input:	path - path to use
//			filename - filename to use
//			dest - buffer to compose result in
//			destSize - size of destination buffer
//-----------------------------------------------------------------------------
void Q_ComposeFileName(const char *path, const char *filename, char *dest, int destSize)
{
	Q_strncpy(dest, path, destSize);
	Q_FixSlashes(dest);
	Q_AppendSlash(dest, destSize);
	//Q_strncat(dest, filename, destSize, COPY_ALL_CHARACTERS); // TODO
	Q_FixSlashes(dest);
}

//-----------------------------------------------------------------------------
// Purpose:
// Input  : *path -
//			*dest -
//			destSize -
// Output : void Q_ExtractFilePath
//-----------------------------------------------------------------------------
bool Q_ExtractFilePath(const char *path, char *dest, int destSize)
{
	Assert(destSize >= 1);
	if(destSize < 1)
	{
		return false;
	}

	// Last char
	int len = Q_strlen(path);
	const char *src = path + (len ? len - 1 : 0);

	// back up until a \ or the start
	while(src != path && !PATHSEPARATOR(*(src - 1)))
	{
		src--;
	}

	int copysize = std::min(src - path, destSize - 1);
	memcpy(dest, path, copysize);
	dest[copysize] = 0;

	return copysize != 0 ? true : false;
}

//-----------------------------------------------------------------------------
// Purpose:
// Input  : *path -
//			*dest -
//			destSize -
// Output : void Q_ExtractFileExtension
//-----------------------------------------------------------------------------
void Q_ExtractFileExtension(const char *path, char *dest, int destSize)
{
	*dest = '\0';
	const char *extension = Q_GetFileExtension(path);
	if(NULL != extension)
		Q_strncpy(dest, extension, destSize);
}

//-----------------------------------------------------------------------------
// Purpose: Returns a pointer to the file extension within a file name string
// Input:	in - file name
// Output:	pointer to beginning of extension (after the "."), or NULL
//				if there is no extension
//-----------------------------------------------------------------------------
const char *Q_GetFileExtension(const char *path)
{
	const char *src;

	src = path + strlen(path) - 1;

	//
	// back up until a . or the start
	//
	while(src != path && *(src - 1) != '.')
		src--;

	// check to see if the '.' is part of a pathname
	if(src == path || PATHSEPARATOR(*src))
	{
		return NULL; // no extension
	}

	return src;
}

bool Q_RemoveDotSlashes(char *pFilename, char separator, bool bRemoveDoubleSlashes /* = true */)
{
	char *pIn = pFilename;
	char *pOut = pFilename;
	bool bRetVal = true;

	bool bBoundary = true;
	while(*pIn)
	{
		if(bBoundary && pIn[0] == '.' && pIn[1] == '.' && (PATHSEPARATOR(pIn[2]) || !pIn[2]))
		{
			// Get rid of /../ or trailing /.. by backing pOut up to previous separator

			// Eat the last separator (or repeated separators) we wrote out
			while(pOut != pFilename && pOut[-1] == separator)
			{
				--pOut;
			}

			while(true)
			{
				if(pOut == pFilename)
				{
					bRetVal = false; // backwards compat. return value, even though we continue handling
					break;
				}
				--pOut;
				if(*pOut == separator)
				{
					break;
				}
			}

			// Skip the '..' but not the slash, next loop iteration will handle separator
			pIn += 2;
			bBoundary = (pOut == pFilename);
		}
		else if(bBoundary && pIn[0] == '.' && (PATHSEPARATOR(pIn[1]) || !pIn[1]))
		{
			// Handle "./" by simply skipping this sequence. bBoundary is unchanged.
			if(PATHSEPARATOR(pIn[1]))
			{
				pIn += 2;
			}
			else
			{
				// Special case: if trailing "." is preceded by separator, eg "path/.",
				// then the final separator should also be stripped. bBoundary may then
				// be in an incorrect state, but we are at the end of processing anyway
				// so we don't really care (the processing loop is about to terminate).
				if(pOut != pFilename && pOut[-1] == separator)
				{
					--pOut;
				}
				pIn += 1;
			}
		}
		else if(PATHSEPARATOR(pIn[0]))
		{
			*pOut = separator;
			pOut += 1 - (bBoundary & bRemoveDoubleSlashes & (pOut != pFilename));
			pIn += 1;
			bBoundary = true;
		}
		else
		{
			if(pOut != pIn)
			{
				*pOut = *pIn;
			}
			pOut += 1;
			pIn += 1;
			bBoundary = false;
		}
	}
	*pOut = 0;

	return bRetVal;
}

void Q_AppendSlash(char *pStr, int strSize)
{
	int len = Q_strlen(pStr);
	if(len > 0 && !PATHSEPARATOR(pStr[len - 1]))
	{
		if(len + 1 >= strSize)
			Error("Q_AppendSlash: ran out of space on %s.", pStr);

		pStr[len] = CORRECT_PATH_SEPARATOR;
		pStr[len + 1] = 0;
	}
}

void Q_MakeAbsolutePath(char *pOut, int outLen, const char *pPath, const char *pStartingDir)
{
	if(Q_IsAbsolutePath(pPath))
	{
		// pPath is not relative.. just copy it.
		Q_strncpy(pOut, pPath, outLen);
	}
	else
	{
		// Make sure the starting directory is absolute..
		if(pStartingDir && Q_IsAbsolutePath(pStartingDir))
		{
			Q_strncpy(pOut, pStartingDir, outLen);
		}
		else
		{
			if(!_getcwd(pOut, outLen))
				Error("Q_MakeAbsolutePath: _getcwd failed.");

			if(pStartingDir)
			{
				Q_AppendSlash(pOut, outLen);
				//Q_strncat(pOut, pStartingDir, outLen, COPY_ALL_CHARACTERS); // TODO
			}
		}

		// Concatenate the paths.
		Q_AppendSlash(pOut, outLen);
		//Q_strncat(pOut, pPath, outLen, COPY_ALL_CHARACTERS); // TODO
	}

	if(!Q_RemoveDotSlashes(pOut))
		Error("Q_MakeAbsolutePath: tried to \"..\" past the root.");

	//Q_FixSlashes( pOut ); - handled by Q_RemoveDotSlashes
}

//-----------------------------------------------------------------------------
// small helper function shared by lots of modules
//-----------------------------------------------------------------------------
bool Q_IsAbsolutePath(const char *pStr)
{
	bool bIsAbsolute = (pStr[0] && pStr[1] == ':') || pStr[0] == '/' || pStr[0] == '\\';
	if(IsX360() && !bIsAbsolute)
	{
		bIsAbsolute = (Q_stristr(pStr, ":") != NULL);
	}
	return bIsAbsolute;
}

// Copies at most nCharsToCopy bytes from pIn into pOut.
// Returns false if it would have overflowed pOut's buffer.
static bool CopyToMaxChars(char *pOut, int outSize, const char *pIn, int nCharsToCopy)
{
	if(outSize == 0)
		return false;

	int iOut = 0;
	while(*pIn && nCharsToCopy > 0)
	{
		if(iOut == (outSize - 1))
		{
			pOut[iOut] = 0;
			return false;
		}
		pOut[iOut] = *pIn;
		++iOut;
		++pIn;
		--nCharsToCopy;
	}

	pOut[iOut] = 0;
	return true;
}

// Returns true if it completed successfully.
// If it would overflow pOut, it fills as much as it can and returns false.
bool Q_StrSubst(
const char *pIn,
const char *pMatch,
const char *pReplaceWith,
char *pOut,
int outLen,
bool bCaseSensitive)
{
	int replaceFromLen = strlen(pMatch);
	int replaceToLen = strlen(pReplaceWith);

	const char *pInStart = pIn;
	char *pOutPos = pOut;
	pOutPos[0] = 0;

	while(1)
	{
		int nRemainingOut = outLen - (pOutPos - pOut);

		const char *pTestPos = (bCaseSensitive ? strstr(pInStart, pMatch) : Q_stristr(pInStart, pMatch));
		if(pTestPos)
		{
			// Found an occurence of pMatch. First, copy whatever leads up to the string.
			int copyLen = pTestPos - pInStart;
			if(!CopyToMaxChars(pOutPos, nRemainingOut, pInStart, copyLen))
				return false;

			// Did we hit the end of the output string?
			if(copyLen > nRemainingOut - 1)
				return false;

			pOutPos += strlen(pOutPos);
			nRemainingOut = outLen - (pOutPos - pOut);

			// Now add the replacement string.
			if(!CopyToMaxChars(pOutPos, nRemainingOut, pReplaceWith, replaceToLen))
				return false;

			pInStart += copyLen + replaceFromLen;
			pOutPos += replaceToLen;
		}
		else
		{
			// We're at the end of pIn. Copy whatever remains and get out.
			int copyLen = strlen(pInStart);
			Q_strncpy(pOutPos, pInStart, nRemainingOut);
			return (copyLen <= nRemainingOut - 1);
		}
	}
}

char *AllocString(const char *pStr, int nMaxChars)
{
	int allocLen;
	if(nMaxChars == -1)
		allocLen = strlen(pStr) + 1;
	else
		allocLen = std::min((int)strlen(pStr), nMaxChars) + 1;

	char *pOut = new char[allocLen];
	Q_strncpy(pOut, pStr, allocLen);
	return pOut;
}

void Q_SplitString2(const char *pString, const char **pSeparators, int nSeparators, CUtlVector<char *> &outStrings)
{
/*
	outStrings.Purge();
	const char *pCurPos = pString;
	while(1)
	{
		int iFirstSeparator = -1;
		const char *pFirstSeparator = 0;
		for(int i = 0; i < nSeparators; i++)
		{
			const char *pTest = Q_stristr(pCurPos, pSeparators[i]);
			if(pTest && (!pFirstSeparator || pTest < pFirstSeparator))
			{
				iFirstSeparator = i;
				pFirstSeparator = pTest;
			}
		}

		if(pFirstSeparator)
		{
			// Split on this separator and continue on.
			int separatorLen = strlen(pSeparators[iFirstSeparator]);
			if(pFirstSeparator > pCurPos)
			{
				outStrings.AddToTail(AllocString(pCurPos, pFirstSeparator - pCurPos));
			}

			pCurPos = pFirstSeparator + separatorLen;
		}
		else
		{
			// Copy the rest of the string
			if(strlen(pCurPos))
			{
				outStrings.AddToTail(AllocString(pCurPos, -1));
			}
			return;
		}
	}
*/
}

void Q_SplitString(const char *pString, const char *pSeparator, CUtlVector<char *> &outStrings)
{
	Q_SplitString2(pString, &pSeparator, 1, outStrings);
}

// This function takes a slice out of pStr and stores it in pOut.
// It follows the Python slice convention:
// Negative numbers wrap around the string (-1 references the last character).
// Numbers are clamped to the end of the string.
void Q_StrSlice(const char *pStr, int firstChar, int lastCharNonInclusive, char *pOut, int outSize)
{
	if(outSize == 0)
		return;

	int length = strlen(pStr);

	// Fixup the string indices.
	if(firstChar < 0)
	{
		firstChar = length - (-firstChar % length);
	}
	else if(firstChar >= length)
	{
		pOut[0] = 0;
		return;
	}

	if(lastCharNonInclusive < 0)
	{
		lastCharNonInclusive = length - (-lastCharNonInclusive % length);
	}
	else if(lastCharNonInclusive > length)
	{
		lastCharNonInclusive %= length;
	}

	if(lastCharNonInclusive <= firstChar)
	{
		pOut[0] = 0;
		return;
	}

	int copyLen = lastCharNonInclusive - firstChar;
	if(copyLen <= (outSize - 1))
	{
		memcpy(pOut, &pStr[firstChar], copyLen);
		pOut[copyLen] = 0;
	}
	else
	{
		memcpy(pOut, &pStr[firstChar], outSize - 1);
		pOut[outSize - 1] = 0;
	}
}

void Q_StrLeft(const char *pStr, int nChars, char *pOut, int outSize)
{
	if(nChars == 0)
	{
		if(outSize != 0)
			pOut[0] = 0;

		return;
	}

	Q_StrSlice(pStr, 0, nChars, pOut, outSize);
}

void Q_StrRight(const char *pStr, int nChars, char *pOut, int outSize)
{
	int len = strlen(pStr);
	if(nChars >= len)
	{
		Q_strncpy(pOut, pStr, outSize);
	}
	else
	{
		Q_StrSlice(pStr, -nChars, strlen(pStr), pOut, outSize);
	}
}