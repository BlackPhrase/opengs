/*
 *	This file is part of OGS Engine
 *	Copyright (C) 2016-2017 OGS Dev Team
 *
 *	OGS Engine is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	OGS Engine is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OGS Engine. If not, see <http://www.gnu.org/licenses/>.
 *
 *	In addition, as a special exception, the author gives permission to
 *	link the code of OGS Engine with the Half-Life Game Engine ("GoldSrc/GS
 *	Engine") and Modified Game Libraries ("MODs") developed by Valve,
 *	L.L.C ("Valve"). You must obey the GNU General Public License in all
 *	respects for all of the code used other than the GoldSrc Engine and MODs
 *	from Valve. If you modify this file, you may extend this exception
 *	to your version of the file, but you are not obligated to do so. If
 *	you do not wish to do so, delete this exception statement from your
 *	version.
*/

/// @file

#include "vstdlib/osversion.h"

/*

// TODO

enum eOSMap
{
	EOSType type{k_eOSUnknown};
	const char *name{""};
};

eOSMap gOSMap
{
	{k_eOSUnknown, "Unknown"},
	{k_eMacOS104, "Mac OS X 10.4"},
	{NULL, NULL}
};
*/

const char *GetNameFromOSType(EOSType eOSType)
{
	switch(eOSType)
	{
	//#ifdef __APPLE__ // vstdlib.dll contains only win entries so I guess this should be here
	// Mac OS X
	case k_eMacOSUnknown:
		return "Mac OS";
		break;
	case k_eMacOS104:
		return "Mac OS X 10.4";
		break;
	case k_eMacOS105:
		return "Mac OS X 10.5";
		break;
	case k_eMacOS1058:
		return "Mac OS X 10.5.8";
		break;
	case k_eMacOS106:
		return "Mac OS X 10.6";
		break;
	case k_eMacOS1063:
		return "Mac OS X 10.6.3";
		break;
	case k_eMacOS107:
		return "Mac OS X 10.7";
		break;
	case k_eMacOS108:
		return "Mac OS X 10.8";
		break;
	case k_eMacOS109:
		return "Mac OS X 10.9";
		break;
	case k_eMacOS1010:
		return "Mac OS X 10.10";
		break;
	case k_eMacOS1011:
		return "Mac OS X 10.11";
		break;
	case k_eMacOS1012:
		return "Mac OS X 10.12";
		break;
	case k_eMacOS1013:
		return "Mac OS X 10.13";
		break;
	//#elif __linux__
	// Linux
	case k_eLinuxUnknown:
		return "Linux";
		break;
	case k_eLinux22:
		return "Linux 2.2";
		break;
	case k_eLinux24:
		return "Linux 2.4";
		break;
	case k_eLinux26:
		return "Linux 2.6";
		break;
	case k_eLinux32:
		return "Linux 3.2";
		break;
	case k_eLinux310:
		return "Linux 3.10";
		break;
	case k_eLinux316:
		return "Linux 3.16";
		break;
	case k_eLinux318:
		return "Linux 3.18";
		break;
	case k_eLinux41:
		return "Linux 4.1";
		break;
	case k_eLinux44:
		return "Linux 4.4";
		break;
	case k_eLinux49:
		return "Linux 4.9";
		break;
	case k_eLinux413:
		return "Linux 4.13";
		break;
	//#elif _WIN32
	// Windows
	case k_eWinUnknown:
		return "Windows";
		break;
	case k_eWin311:
		return "Windows 3.11";
		break;
	case k_eWin95:
		return "Windows 95";
		break;
	case k_eWin98:
		return "Windows 98";
		break;
	case k_eWinME:
		return "Windows ME";
		break;
	case k_eWinNT:
		return "Windows NT";
		break;
	case k_eWin2000:
		return "Windows 2000";
		break;
	case k_eWinXP:
		return "Windows XP";
		break;
	case k_eWin2003:
		return "Windows 2003";
		break;
	case k_eWinVista:
		return "Windows Vista";
		break;
	case k_eWindows7:
		return "Windows 7";
		break;
	case k_eWin2008:
		return "Windows 2008";
		break;
	case k_eWindows8:
		return "Windows 8";
		break;
	case k_eWindows81:
		return "Windows 8.1";
		break;
	case k_eWindows10:
		return "Windows 10";
		break;
	//#endif

	// Others/unknown
	case k_eOSUnknown:
	default:
		return "Unknown";
		break;
	};
};

/*
const char *GetOSDetailString(char *pchOutBuf, int cchOutBuf) // Not present
{
	return "";
};
*/

EOSType GetOSType()
{
#ifdef _WIN32
	//return k_eWin311;
	//return k_eWin95;
	//return k_eWin98;
	//return k_eWinME;
	//return k_eWinNT;
	//return k_eWin2000;
	//return k_eWinXP;
	//return k_eWin2003;
	//return k_eWinVista;
	//return k_eWindows7;
	//return k_eWin2008;

	return k_eWinUnknown;
#elif __linux__
	//return k_eLinux22;
	//return k_eLinux24;
	//return k_eLinux26;

	return k_eLinuxUnknown;
#elif __APPLE__
	//return k_eMacOS104;
	//return k_eMacOS105;
	//return k_eMacOS1058;
	//return k_eMacOS106;
	//return k_eMacOS1063;
	//return k_eMacOS107;

	return k_eMacOSUnknown;
#else
	return k_eOSUnknown;
#endif
};

EOSType GetOSTypeFromString(const char *str)
{
	return k_eOSUnknown;
};

/*
bool OSTypesAreCompatible(EOSType eOSTypeDetected, EOSType eOSTypeRequired) // Not present
{
	bool bCompatible{false};
	
	switch(eOSTypeRequired)
	{
	};
	
	return false;
};

const char *GetPlatformName(bool *pbIs64Bit) // Not present
{
	if(pbIs64Bit)
	{
		*pbIs64Bit = false;
		
		if(Is64Bit())
			*pbIs64Bit = true;
	};
	
#ifdef __APPLE__
#elif __linux__
#elif _WIN32
	return "win311";
	return "win95";
	return "win98";
	return "winME";
	return "winNT";
	return "win2000";
	return "winXP";
	return "win2003";
	return "winVista"; // end of GS entries
	return "win7";
	return "win2008";
	return "win8";
	return "win81";
	return "win10";
#endif

	return "unknown";
};
*/