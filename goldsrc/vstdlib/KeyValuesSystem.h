//========= Copyright © 1996-2003, Valve LLC, All rights reserved. ============
//
// Purpose:
//
// $NoKeywords: $
//=============================================================================

/// @file

#pragma once

#include "vstdlib/IKeyValuesSystem.h"
//#include "UtlSymbol.h"

class CMemoryPool;

/*
//-----------------------------------------------------------------------------
// Purpose: Central storage point for KeyValues memory and symbols
//-----------------------------------------------------------------------------
class CKeyValuesSystem final : public IKeyValuesSystem
{
public:
	CKeyValuesSystem();
	~CKeyValuesSystem();

	void RegisterSizeofKeyValues(int size) override;

	void *AllocKeyValuesMemory(int size) override;
	void FreeKeyValuesMemory(void *pMem) override;

	HKeySymbol GetSymbolForString(const char *name) override;
	const char *GetStringForSymbol(HKeySymbol symbol) override;

	HLocalized GetLocalizedFromANSI(const char *string) override;
	const char *GetANSIFromLocalized(HLocalized l) override;

	void AddKeyValuesToMemoryLeakList(void *pMem, HKeySymbol name) override;
	void RemoveKeyValuesFromMemoryLeakList(void *pMem) override;

private:
	CMemoryPool *m_pMemPool{nullptr};
	CUtlSymbolTable m_SymbolTable;

	int m_iMaxKeyValuesSize{0};
};
*/