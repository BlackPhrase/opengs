#pragma once

/**
*	@file
*
*	File paths used in multiple projects.
*/

#ifdef _WIN32
#define DEFAULT_SO_EXT ".dll"
#elif defined( __linux__ )
#define DEFAULT_SO_EXT ".so"
#elif defined( __APPLE__ )
#define DEFAULT_SO_EXT ".dylib"
#else
#error "Unsupported platform!"
#endif

namespace filepath
{
extern const char FILESYSTEM_STDIO[];
}