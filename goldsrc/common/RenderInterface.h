/*
 *	This file is part of OGS Engine
 *	Copyright (C) 1997-2001 Id Software, Inc.
 *	Copyright (C) 2017 OGS Dev Team
 *
 *	OGS Engine is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	OGS Engine is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OGS Engine. If not, see <http://www.gnu.org/licenses/>.
 *
 *	In addition, as a special exception, the author gives permission to
 *	link the code of OGS Engine with the Half-Life Game Engine ("GoldSrc/GS
 *	Engine") and Modified Game Libraries ("MODs") developed by Valve,
 *	L.L.C ("Valve"). You must obey the GNU General Public License in all
 *	respects for all of the code used other than the GoldSrc Engine and MODs
 *	from Valve. If you modify this file, you may extend this exception
 *	to your version of the file, but you are not obligated to do so. If
 *	you do not wish to do so, delete this exception statement from your
 *	version.
*/

/// @file
/// @brief render module interface

#pragma once

#include "public/interface.h"
#include "common/const.h"
#include "common/cvardef.h"
#include "common/cl_entity.h"
#include "common/dlight.h"
//#include "common/lightstyle.h"
#include "common/particledef.h"

const int MAX_DLIGHTS = 32;
const int MAX_ENTITIES = 128;
const int MAX_PARTICLES = 4096;
//const int MAX_LIGHTSTYLES = 256; // defined in qlimits

#define SHELL_RED_COLOR 0xF2
#define SHELL_GREEN_COLOR 0xD0
#define SHELL_BLUE_COLOR 0xF3

#define SHELL_RG_COLOR 0xDC
//#define SHELL_RB_COLOR		0x86
#define SHELL_RB_COLOR 0x68
#define SHELL_BG_COLOR 0x78

//ROGUE
#define SHELL_DOUBLE_COLOR 0xDF // 223
#define SHELL_HALF_DAM_COLOR 0x90
#define SHELL_CYAN_COLOR 0x72
//ROGUE

#define SHELL_WHITE_COLOR 0xD7

/*
typedef struct cl_entity_s
{
	struct model_s		*model;			// opaque type outside render
	float				angles[3];

	// most recent data
	float				origin[3];		// also used as RF_BEAM's "from"
	int					frame;			// also used as RF_BEAM's diameter

	// previous data for lerping
	float				oldorigin[3];	// also used as RF_BEAM's "to"
	int					oldframe;

	// misc
	float	backlerp;				// 0.0 = current, 1.0 = old
	int		skinnum;				// also used as RF_BEAM's palette index

	int		lightstyle;				// for flashing entities
	float	alpha;					// ignore if RF_TRANSLUCENT isn't set

	struct image_s	*skin;			// NULL for inline skin
	int		flags;

} cl_entity_t;
*/

const int ENTITY_FLAGS = 68;

typedef struct
{
	float		rgb[3];			// 0.0 - 2.0
	float		white;			// highest of rgb
} lightstyle_t;

struct refdef_t
{
	int x{0}, y{0}, width{0}, height{0}; ///< in virtual screen coordinates
	
	float fov_x{0.0f}, fov_y{0.0f};

	vec3_t vieworg{};
	vec3_t viewangles{};

	float blend[4]{}; ///< rgba 0-1 full screen blend
	
	float time{0.0f}; ///< time is used to auto animate
	
	int rdflags{0}; ///< RDF_UNDERWATER, etc

	byte *areabits{nullptr}; ///< if not nullptr, only areas with set bits will be drawn

	lightstyle_t *lightstyles{nullptr}; // [MAX_LIGHTSTYLES]

	int num_entities{0};
	cl_entity_t *entities{nullptr};

	int num_dlights{0};
	dlight_t *dlights{nullptr};

	int num_particles{0};
	particle_t *particles{nullptr};
};

/// If API version is different, the dll cannot be used
const char OGS_RENDER_INTERFACE_VERSION[] = "OGSRender001";

//
// This is the interface exposed by the render module
//
struct IRender : public IBaseInterface
{
	/// Called when the library is loaded
	virtual bool Init(CreateInterfaceFn afnEngineFactory, void *hinstance, void *wndproc) = 0;

	/// Called before the library is unloaded
	virtual void Shutdown() = 0;

	// All data that will be used in a level should be
	// registered before rendering any frames to prevent disk hits,
	// but they can still be registered at a later time
	// if necessary.
	//
	// EndRegistration will free any remaining data that wasn't registered.
	// Any model_s or skin_s pointers from before the BeginRegistration
	// are no longer valid after EndRegistration.
	//
	// Skins and images need to be differentiated, because skins
	// are flood filled to eliminate mip map edge errors, and pics have
	// an implicit "pics/" prepended to the name. (a pic name that starts with a
	// slash will not use the "pics/" prefix or the ".pcx" postfix)
	virtual void BeginRegistration(const char *map) = 0;
	virtual struct model_s *RegisterModel(const char *name) = 0;
	//virtual struct image_s *RegisterSkin(const char *name) = 0;
	virtual struct image_s *RegisterPic(const char *name) = 0;
	virtual void SetSky(const char *name, float rotate, vec3_t axis) = 0;
	virtual void EndRegistration() = 0;

	virtual void RenderFrame(refdef_t *fd) = 0;

	virtual void DrawGetPicSize(int *w, int *h, const char *name) = 0; // will return 0 0 if not found
	virtual void DrawPic(int x, int y, const char *name) = 0;
	virtual void DrawStretchPic(int x, int y, int w, int h, const char *name) = 0;
	virtual void DrawChar(int x, int y, int c) = 0;
	virtual void DrawTileClear(int x, int y, int w, int h, const char *name) = 0;
	virtual void DrawFill(int x, int y, int w, int h, int c) = 0;
	virtual void DrawFadeScreen() = 0;

	/// Draw images for cinematic rendering (which can have a different palette). Note that calls
	virtual void DrawStretchRaw(int x, int y, int w, int h, int cols, int rows, byte *data) = 0;

	/*
	** video mode and render state management entry points
	*/
	virtual void CinematicSetPalette(const unsigned short *palette) = 0; // NULL = game palette

	virtual void BeginFrame(float camera_separation) = 0;
	virtual void EndFrame() = 0;

	virtual void AppActivate(bool activate) = 0;
};

//temp
#define	ERR_FATAL	0		// exit the entire game with a popup window
#define	ERR_DROP	1		// print to console and disconnect from game
#define	ERR_QUIT	2		// not an error, just a normal exit

#define	PRINT_ALL		0
#define PRINT_DEVELOPER	1	// only print when "developer 1"

//
// these are the functions imported by the render module
//
struct refimport_t
{
	void (*Sys_Error)(int err_level, const char *str, ...);

	void (*Cmd_AddCommand)(const char *name, void (*cmd)());
	void (*Cmd_RemoveCommand)(const char *name);
	
	int (*Cmd_Argc)();
	char *(*Cmd_Argv)(int i);
	
	void (*Cmd_ExecuteText)(int exec_when, const char *text);

	void (*Con_Printf)(int print_level, const char *str, ...);

	// files will be memory mapped read only
	// the returned buffer may be part of a larger pak file,
	// or a discrete file from anywhere in the quake search path
	// a -1 return means the file does not exist
	// NULL can be passed for buf to just determine existance
	int (*FS_LoadFile)(const char *name, void **buf);
	void (*FS_FreeFile)(void *buf);

	// gamedir will be the current directory that generated
	// files should be stored to, ie: "f:\quake\id1"
	char *(*FS_Gamedir)();

	cvar_t *(*Cvar_Register)(const char *name, const char *value, int flags);
	cvar_t *(*Cvar_Set)(const char *name, const char *value);
	void (*Cvar_SetValue)(const char *name, float value);

	bool (*Vid_GetModeInfo)(int *width, int *height, int mode);
	void (*Vid_MenuInit)();
	void (*Vid_NewWindow)(int width, int height);
};