/// @file

#include "tier0/threadtools.h"

bool ThreadInterlockedAssignIf(long volatile *pDest, long value, long comperand)
{
	return true;
};

bool ThreadInterlockedAssignIf64(volatile int64 *pDest, int64 value, int64 comperand)
{
	return true;
};

bool CThread::IsThreadRunning()
{
	return true;
};

void CThreadFastMutex::Lock(const uint32 threadId, unsigned nSpinSleepTime) volatile
{
};

void CThreadRWLock::WaitForRead()
{
};

void CThreadSpinRWLock::SpinLockForWrite(const uint32 threadId)
{
};

CThreadMutex::CThreadMutex()
{
};

CThreadMutex::~CThreadMutex()
{
};