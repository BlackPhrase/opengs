/*
 *	This file is part of OGS Engine
 *	Copyright (C) 1997-2001 Id Software, Inc.
 *	Copyright (C) 2016-2017 OGS Dev Team
 *
 *	OGS Engine is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	OGS Engine is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with OGS Engine. If not, see <http://www.gnu.org/licenses/>.
 *
 *	In addition, as a special exception, the author gives permission to
 *	link the code of OGS Engine with the Half-Life Game Engine ("GoldSrc/GS
 *	Engine") and Modified Game Libraries ("MODs") developed by Valve,
 *	L.L.C ("Valve"). You must obey the GNU General Public License in all
 *	respects for all of the code used other than the GoldSrc Engine and MODs
 *	from Valve. If you modify this file, you may extend this exception
 *	to your version of the file, but you are not obligated to do so. If
 *	you do not wish to do so, delete this exception statement from your
 *	version.
*/

/// @file

#pragma once

//#include "common/com_model.h"

/*

d*_t structures are on-disk representations
m*_t structures are in-memory

*/

/*
==============================================================================

BRUSH MODELS

==============================================================================
*/

// TEMP DUPLICATE
// plane_t structure
typedef struct mplane_s
{
	vec3_t normal; ///< surface normal
	float dist;    ///< closest appoach to origin
	byte type;     ///< for texture axis selection and fast side tests
	byte signbits; ///< signx + signy<<1 + signz<<1
	byte pad[2];
} mplane_t;

//
// in memory representation
//
// !!! if this is changed, it must be changed in asm_draw.h too !!!
typedef struct
{
	vec3_t position;
} mvertex_t;

typedef struct
{
	vec3_t mins, maxs;
	vec3_t origin; // for sounds or lights
	float radius;
	int headnode;
	int visleafs; // not including the solid leaf 0
	int firstface, numfaces;
} mmodel_t;

#define SIDE_FRONT 0
#define SIDE_BACK 1
#define SIDE_ON 2

#define SURF_PLANEBACK 2
#define SURF_DRAWSKY 4
#define SURF_DRAWTURB 0x10
#define SURF_DRAWBACKGROUND 0x40
#define SURF_UNDERWATER 0x80

// !!! if this is changed, it must be changed in asm_draw.h too !!!
typedef struct
{
	unsigned short v[2];
	unsigned int cachededgeoffset;
} medge_t;

typedef struct mtexinfo_s
{
	float vecs[2][4];
	int flags;
	int numframes;
	struct mtexinfo_s *next; // animation chain
	image_t *image;
} mtexinfo_t;

#define VERTEXSIZE 7

typedef struct glpoly_s
{
	struct glpoly_s *next;
	struct glpoly_s *chain;
	int numverts;
	int flags;                  // for SURF_UNDERWATER (not needed anymore?)
	float verts[4][VERTEXSIZE]; // variable sized (xyz s1t1 s2t2)
} glpoly_t;

// TEMP
const int MAXLIGHTMAPS = 4;

typedef struct msurface_s
{
	int visframe; // should be drawn when node is crossed

	mplane_t *plane;
	int flags;

	int firstedge; // look up in model->surfedges[], negative numbers
	int numedges;  // are backwards edges

	short texturemins[2];
	short extents[2];

	int light_s, light_t;   // gl lightmap coordinates
	int dlight_s, dlight_t; // gl lightmap coordinates for dynamic lightmaps

	glpoly_t *polys; // multiple if warped
	struct msurface_s *texturechain;
	struct msurface_s *lightmapchain;

	mtexinfo_t *texinfo;

	// lighting info
	int dlightframe;
	int dlightbits;

	int lightmaptexturenum;
	byte styles[MAXLIGHTMAPS];
	float cached_light[MAXLIGHTMAPS]; // values currently used in lightmap
	byte *samples;                    // [numstyles*surfsize]
} msurface_t;

typedef struct mnode_s
{
	// common with leaf
	int contents; // -1, to differentiate from leafs
	int visframe; // node needs to be traversed if current

	float minmaxs[6]; // for bounding box culling

	struct mnode_s *parent;

	// node specific
	mplane_t *plane;
	struct mnode_s *children[2];

	unsigned short firstsurface;
	unsigned short numsurfaces;
} mnode_t;

typedef struct mleaf_s
{
	// common with node
	int contents; // wil be a negative contents number
	int visframe; // node needs to be traversed if current

	float minmaxs[6]; // for bounding box culling

	struct mnode_s *parent;

	// leaf specific
	int cluster;
	int area;

	msurface_t **firstmarksurface;
	int nummarksurfaces;
} mleaf_t;

//===================================================================

//
// Whole model
//

typedef enum { mod_bad,
	           mod_brush,
	           mod_sprite,
	           mod_alias } modtype_t;

typedef struct model_s
{
	char name[MAX_QPATH];

	int registration_sequence;

	modtype_t type;
	int numframes;

	int flags;

	//
	// volume occupied by the model graphics
	//
	vec3_t mins, maxs;
	float radius;

	//
	// solid volume for clipping
	//
	qboolean clipbox;
	vec3_t clipmins, clipmaxs;

	//
	// brush model
	//
	int firstmodelsurface, nummodelsurfaces;
	int lightmap; // only for submodels

	int numsubmodels;
	mmodel_t *submodels;

	int numplanes;
	mplane_t *planes;

	int numleafs; // number of visible leafs, not counting 0
	mleaf_t *leafs;

	int numvertexes;
	mvertex_t *vertexes;

	int numedges;
	medge_t *edges;

	int numnodes;
	int firstnode;
	mnode_t *nodes;

	int numtexinfo;
	mtexinfo_t *texinfo;

	int numsurfaces;
	msurface_t *surfaces;

	int numsurfedges;
	int *surfedges;

	int nummarksurfaces;
	msurface_t **marksurfaces;

	byte *visdata;

	byte *lightdata;

	int extradatasize;
	void *extradata;
} model_t;

#define	MIPLEVELS	4
typedef struct miptex_s
{
	char		name[16];
	unsigned	width, height;
	unsigned	offsets[MIPLEVELS];		// four mip maps stored
} miptex_t;

//============================================================================

void Mod_Init();
void Mod_ClearAll();
model_t *Mod_ForName(char *name, qboolean crash);
mleaf_t *Mod_PointInLeaf(float *p, model_t *model);
byte *Mod_ClusterPVS(int cluster, model_t *model);

void Mod_Modellist_f();

void *Hunk_Begin(int maxsize);
void *Hunk_Alloc(int size);
int Hunk_End();
void Hunk_Free(void *base);

void Mod_FreeAll();
void Mod_Free(model_t *mod);