/// @file

#pragma once

void _LogFunctionTrace(const char *pFunctionName, const char *param);
double _StartFunctionTimer();
void _LogFunctionTraceMaxTime(const char *pFunctionName, double startTime, double maxTime);
void ClearErrorLogs();
void Error(const char *pMsg, ...);